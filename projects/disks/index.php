<?
define("ROOT","../../");
require_once("../../funcs.php");
show_header("�����",PROJECTS);
show_menu("inc/main.menu"); ?>
<td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/projects/">���������� �������</a> :: ������������ ����� </div>
            <p>����������� ����� ������ ����������� ����
<?php lj_user('scrackan'); ?> �������� ��� ������� ���������, ���������� ��&nbsp;��&nbsp;����, �&nbsp;����� �������������� ��� ���� ��&nbsp;��������. ��� �������� ����� �&nbsp;2004&nbsp;���� (&laquo;Planescape: Sigil&raquo;) � ������ �������� ���� �� �� ��������� �������� ���� ��� ����������� ���. </p>
            <p>���������� ������� ��� ������� ��&nbsp;������ ������������ ���������� (�����������&nbsp;&mdash; �&nbsp;�������� ��������) �&nbsp;�����. �� ���������, ��� �������, ����� ������� � ������ �����. ����� ���������������� ������ ��&nbsp;50&nbsp;������, ��&nbsp;���������� �&nbsp;���������� �������� ��&nbsp;���� ��������.</p>
<h4>������������ ����� (�&nbsp;�������� ��������������� �������) </h4>
<p><table border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top"><img src="2008-alaska_small.jpg" width="150" height="150" /></td>
<td valign="top"><strong><a name="alaska" id="alaska"></a><a href="http://bastilia.ru/alaska/final/">��������: ������� ���������<br />
</a></strong>(23-25&nbsp;������� 2008&nbsp;�.)</td>
<td valign="top"><div align="left">
  <ul>
       <li>460 ���������� � �������� �������� (���������:       ����, ��������, ����, ������, �����, ���� �������)</li>
       <li>������� ����� �� ������� (MPG, 16 �����)</li>
       <li>���������������       ���������� ����</li>
       <li>����       � ����������� ���������� �� ��������-2007</li>
       <li>�������       ���������� (������, �������, ����� � �.�.)</li>
       <li>����������       ������</li>
       <li>����������       � ���� (� �.�. ���������� �����)</li>
       <li>������������       ������� � ������ ������</li>
       <li>�������       ��������� ��������</li>
       <li>������������       ����� ������� (� ��� ����� � MP3)</li>
       <li>�����,       ����� � ���� ���� �������</li>
       <li>������       �� ������������ ������</li>
       <li>���-�����       ������</li>
       <li>�����       ���� ������� (��� ���, ��� ��� � �� ���������)</li>
       </ul>
</div></td>
</tr>
<tr>
<td valign="top"><img src="2007-lynch_small.jpg" width="150" height="150" /></td>
<td valign="top"><strong><a href="http://bastilia.ru/lynch/final/">��������<br />
</a></strong>(17-19&nbsp;������� 2007&nbsp;�.)</td>
<td valign="top"><div align="left">
  <ul>
    <li>���������� (����, ��������, ����, ���)</li>
    <li>&laquo;����� �����&raquo; �&nbsp;����� &laquo;������&raquo; (5.01.07): ��������, ����������, ������</li>
    <li>������� �������</li>
    <li>������ ��&nbsp;������� ������ ����� �&nbsp;��&nbsp;������</li>
    <li>������������ ������� �&nbsp;������ ������</li>
    <li>���������� �&nbsp;���� (�&nbsp;�.�. ���������� �����)</li>
    <li>������� ���������� (������, ������ �&nbsp;��.)</li>
  </ul>
</div></td>
</tr>
<tr>
<td valign="top"><img src="2007-hellas_small.jpg" width="150" height="150" /></td>
<td valign="top"><strong><a href="http://bastilia.ru/hellas/final/">���� ���������<br />
</a></strong>(19-22&nbsp;���� 2007&nbsp;�.) </td>
<td valign="top">  <div align="left">
  <ul>
    <li> 1600&nbsp;���������� (���, ������, ���, ����, �����, �������, ����� ��������, ���������, ��������, ������� ��������, ����, ���������), ����������� ��&nbsp;�������&nbsp;&mdash; �&nbsp;�������� �������� </li>
    <li>����� ����� (4&nbsp;����)</li>
    <li>������������ ������� �&nbsp;������ ������ (44&nbsp;��)</li>
    <li>���������� �&nbsp;���� (�&nbsp;�.�. ���������� �����)</li>
    <li>���� (&laquo;���.���&nbsp;&mdash; �������� ������&raquo;)</li>
    <li>������� ���������� (������, ������, �����)</li>
  </ul>
</div></td>
</tr>
<tr>
<td valign="top"><img src="2006-150x210.jpg" width="150" height="210" /></td>
<td valign="top"><div align="left"><strong>2 in 1:<a href="http://bastilia.ru/akunin/post/"> <br />
  <br />
  ��������� ������</a></strong><br />
  (10-13 ������� 2006 �.)<br />
  <br />
  <strong><a href="http://bastilia.ru/neron/">Quo Vadis</a></strong><br />
(6-9 ���� 2006 �.) </div></td>
<td align="left" valign="top"><ul type="disc"><li>800 ���������� (������: ��������, ��������,       ��������, ��������, ������, ���� ����, ����, ������, ���, ��������).</li>
    <li>���������� ��������       �������, ��� �����-������� �� ������ ��������� �� ������ ����.</li>
    <li>������������ ������� �� ��.</li>
  <li>����������� ���������� ����������, ������ ���       ����.</li>
  <li>���������� (������, �������, ������).</li>
  <li>�������� �������� ����������� �����.</li>
  <li>���������� � ����� � �������-���� � �����       �������.</li>
  <li>�������� ������. </li>
  </ul>
  <ul type="disc">
    <li>600 ���������� (������: ����, ��������, ��������,       ������, ������, ������).</li>
    <li>2 ����������� �� �����.</li>
    <li>3 ���� ��������� �����.</li>
    <li>������� � ������������ ����������.</li>
    <li>����������.</li>
    <li>��������� � �����</li>
  </ul>  </td>
</tr>

<tr>
<td valign="top"><img src="2005-pulp_small.jpg" width="150" height="150" /></td>
<td valign="top"><div align="left"><strong><a href="http://bastilia.ru/pulp/">���������� �����</a></strong><br />
  (8-10 ���� 2005 �.) </div></td>
<td align="left" valign="top">  <div align="left">
  <ul>
    <li>
      ���������� � �������� �������� (����� 1000 ��)</li>
    <li>����� � �������������</li>
    <li>����� ���� (36 �����, MPEG-1)</li>
    <li>����� ���� (52 ������, MPEG-2)</li>
    <li>����������� ����� �� W-bros. (��������� QT�)</li>
    <li>������� (� �.�. ��������� �<strong>�</strong>�������� ����������)</li>
    <li> ������������ �������</li>
    <li>���������� ����������� ����� (MP3)</li>
    <li>������� (������, �������, ������ � ��.) </li>
  </ul>
</div></td>
</tr>
<tr>
<td valign="top"><img src="2004-sigil_small.jpg" width="150" height="150" /></td>
<td valign="top"><div align="left"><strong><a href="http://bastilia.ru/sigil/after/">Planescape: Sigil</a><br />
  </strong>(19-22 ������� 2004 �.)
</div></td>
<td valign="top">  <div align="left">
  <ul>
    <li>
      ���������� � �������� ��������</li>
    <li>�������� ����������</li>
    <li>������ �������</li>
    <li>���� � ����</li>
    <li>������� (� �.�. ��������� �<strong>�</strong>�������� ����������)</li>
    <li> ���������������� ������</li>
    <li>���������� ������</li>
    <li>������� (������, ������)</li>
    <li>�������� ���� Planescape (PDF, ��������, ������) </li>
  </ul>
</div></td>
</tr>
</table>
</td>


<td width="150" class="side">

<? show_menu("inc/projects.menu");?>
	     </tr>
<? show_footer(); ?>

