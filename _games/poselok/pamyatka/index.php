<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("�������: �� �� ������� :: ������� ��� �������",POSELOK);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://poselok.bastilia.ru/">�������: �� �� �������</a> :: ������� ��� �������</div>


<h3>����������������� ����</h3>

<p>���� ���� ���������� �&nbsp;������� 1&nbsp;����� �&nbsp;15.00&nbsp;&mdash; ����� ����� �������� ����������� ������� �����.

<p>���������&nbsp;&mdash; �&nbsp;���� �&nbsp;������� ��&nbsp;�����������. ����� �������, ���� ��������� 1,5&nbsp;�����, �&nbsp;��� ��������� 2&nbsp;�������.

<p>����������� ����� ��������� ���������� �����������, ������ �&nbsp;�������.

<h3>�������� ����� �&nbsp;������� ���� �����!</h3>

<p>�������� ���� ��&nbsp;������� ����������� ��&nbsp;����� ��&nbsp;�������, ������� ������ ��&nbsp;����� ����, ����� ��� ��������� ��������, ��&nbsp;������� ��� ���� ����� ��&nbsp;�������, �&nbsp;������������� ������������ ��&nbsp;�����, ������� �&nbsp;������� ����� ��� �������� ����� ��� ����������, ������� ������� ��&nbsp;������������ ������� �&nbsp;6.55 (��&nbsp;�������) �&nbsp;8.25 (��&nbsp;�����������).

<p>���������� �&nbsp;����������� ������ ������������! ��� ��&nbsp;������� ��������, �&nbsp;������� ������ ������ ���!

<p>������ �&nbsp;����, ��� ����� ��������� &laquo;��������&raquo; ����������� �&nbsp;9.30 �&nbsp;������ &laquo;�������&raquo;, �&nbsp;������� ��&nbsp;�������� ��&nbsp;����� ��� ������ �&nbsp;��������, ������� �������.

<p>��� �������, ����� ������� ��&nbsp;������� �������� (�&nbsp;������� ������� ����� ������).

<h3>������ ��&nbsp;�������</h3>

<p>��&nbsp;�/�-������� ����������� ��� ��������� ������ 2&nbsp;�� ��&nbsp;����������� ������, ����� 2&nbsp;�� ��&nbsp;������, ����������� �����������. ������ ������ ������� �&nbsp;��������� ������-�����������. ������ �������� �������� �����.

<p>����� ��������� ������ 1,5&nbsp;�� ��&nbsp;������ ��&nbsp;������� �������. ������, ��&nbsp;�������� �&nbsp;�����, ������� �&nbsp;����� ������� ��� ��������� �������.

<h3>���� ��&nbsp;�������� �&nbsp;������ ����</h3>

<p>���� ��&nbsp;�������� �&nbsp;������ ����� ������ ����, ��&nbsp;�&nbsp;�&nbsp;������� ��&nbsp;������� ��� ��&nbsp;����.

<p>��&nbsp;&mdash; ������ �������, ������� �������� ������� ��&nbsp;�����-�� �����������. ��&nbsp;�������� ���������, ��� ��&nbsp;����, ��� ������ �&nbsp;��� �����.

<p>��������� ��� �&nbsp;������� ���� ����� �����, ��&nbsp;������&nbsp;�� ����������� ������ ������� ������������ ��� ��&nbsp;�����, ���� ��&nbsp;���, �������, �����. ���� ��&nbsp;����� ��� ��&nbsp;������� ����������&nbsp;&mdash; ��������� ��� ��������.

<p>�&nbsp;������ �&nbsp;����� ������ ��������� ���������� ��� �&nbsp;��� ��������...

<h3>�����������</h3>

<p>����� ��&nbsp;�������� ��&nbsp;������� �������� ���������� ������. �.�. ��&nbsp;����� ������������� ��&nbsp;���� �&nbsp;������ �������, ��&nbsp;��&nbsp;������ ������ ���� ���� �&nbsp;������ ���������.

<p>��&nbsp;�������� ��&nbsp;�������� ������� ����� �&nbsp;������� ��������� ��������� �&nbsp;�������� ��&nbsp;&laquo;��������&raquo; (��������&nbsp;&mdash; ��� ��&nbsp;�������� &laquo;�������������&raquo; �&nbsp;���� ��&nbsp;��������������, ����&nbsp;&mdash; ����). ���������� &laquo;�������&raquo; ��&nbsp;��&nbsp;����&nbsp;&mdash; ��� �������� ��� ��� ������ �����.

<h3>������</h3>

<p>��� ������&nbsp;&mdash; ��&nbsp;������� ������ �&nbsp;&laquo;�������&raquo;. ������&nbsp;&mdash; ��� �����, ������&nbsp;&mdash; ��� ��������.

<p>�&nbsp;������ ������� ������ ����� �&nbsp;���� (����� ������ ��������� ������). ��� �����-�� ������� �&nbsp;������� ��������, ��������, �������, ������ (���� ������). �������������� &laquo;��������&raquo; �������� �����&nbsp;&mdash; �������, ��������.

<p>��� ������ ��������� ����������� �&nbsp;������.

<p>����� ����������� ������, ���������, ������, ������ �&nbsp;������ �������� ����� �����&nbsp;&mdash; ��&nbsp;��������������!

<h3>��� ������������ �������</h3>

<p>���� ��� ����� ��&nbsp;����, ��&nbsp;������������ �������. �&nbsp;������ �������� ���� ��&nbsp;����.

<p>���� ��� ����� �����������/����������� �������&nbsp;&mdash; ��������, ��&nbsp;������������ ��&nbsp;��� ������������ ��� ������ �������.

<p>�&nbsp;���� ���� �����-���� &laquo;����������&raquo; ��������, ��� ����������� ��&nbsp;���� �&nbsp;���������. �������� ��&nbsp;���� �������� ���� ���������, ��� ��� �����.

<h3>���������� �������� ����</h3>

<p>������ (�����, �������, ���� �&nbsp;��.) ����� ���� ��� �������� �&nbsp;������������. ������ ��&nbsp;��&nbsp;��&nbsp;�����.

<p>����������� �������� ���� ������� ���������� ���������� �����&nbsp;&mdash; �������, &laquo;�����������&raquo; ������ ��� ����������� ������� &laquo;��� ���������&raquo;. �&nbsp;������� �������� �������� ��� ������������� ������ ������ �������.

<p>������ �&nbsp;������ ������ ���� ���������� ��� ���� �����������.

<p>�&nbsp;�����, ���������� ���������� �������� ����.

<h3>������</h3>

<p>����������� ������ �������&nbsp;&mdash; 2&nbsp;����������� ������-��������. ���� ��&nbsp;������� ����� ������������ ������-���������. ����� ����� ���������.

<p>��&nbsp;�������� ��� �� ����� ��������� �&nbsp;����������� ��������, ������������ ���������� ��&nbsp;2&nbsp;�������������� ��������. ��� ������� ��� �&nbsp;��&nbsp;����������, �&nbsp;������&nbsp;&mdash; <a href="http://poselok.bastilia.ru/survival/">��� ������ �&nbsp;������ ����</a>&nbsp;&mdash; ������� �&nbsp;<a href="http://poselok.bastilia.ru/survival/">��������������� ������</a>.


  <? right_block('poselok'); ?>
  </tr>
  <? show_footer(); ?>
