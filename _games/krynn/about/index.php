<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� :: �� ����",KRYNN);
show_menu("inc/main.menu"); ?>

	    <td class="box">
 <div class="boxheader"><a href="/">�����: ����� �����</a> :: �� ����</div>
<h2>����� � ����� ��������</h2>
<p>�����, ���������� ����� ���������� � �� ������ ����� ������� ���� (������ ���������). 
</p>

<h2>� ��� �������� �����?</h2>
<p>����� ���� ������� �������. �������� ���� ���������� � ����, ��� ����� �����, �������� ���������� �� �������� ����� �� ������ � �������� � ��������� � ������, ����� ����������� � �������� �����, ��� ����� ����� ����� � ��������� ������ ���������� ������� ������������ �&nbsp;����� � ����� ����������.</p>
<h2>��� ����� �������?</h2><p>������ ����� �� ���� ��������� �������� �������� (3-5 �������). ��� ����� ���� �����, ������ ��� ������ ��������� ��������� (�� �������� ����� ������ ���������). � ������ ����� ������� �&nbsp;���� �������, ���� ����, ���� �����, ������� ������� � �������� ��������� ������ � ����. ��� ����� ���� ������������ ������������ ������ (������� � ����, � ������� ����� ����� ������ ���� � ��������� �� ����) � ��� ������, ������ ����� �� ������ (������� � �������� ���������) � ������������ � �����������, ����� ����� �����, ������������ �������� � �������, ���� ������ �� ����� ������� � ��� �������������, ������ ����� � ������� ��������� ����� � �.�.</p>
<h2>��� ������� ������������ �������� ������������ ����������?</h2>
<p>��������� ����� ������ �������� ���� � ������, ��������������� ��������, ���������, ���������, ������� �� ������ � ���������, ������� �������� ���������, � ����������� �� ���, �������� ������� ��������� (���������� ���/��������/����� ��� �������), ���������� � ������� �����������, �������� ������ ����� �������� (�����, ���� ��������, �������������� ������ �������), �������� ����� �����.</p>
<h2>��� ������ ����?</h2><p>������� ������������ ����������� ���� � ������ �������� ��� ���������� ��� ������� ���������� �� ������� ������.<br>
��� �������� ���� ��� ���� � ���� ������:
<ul> 
<li><strong>������</strong> (��������, ��������, ��������). ������ ����� ���������� � ������� ���������� ��� ���������� ������. ������ ��� ��������, ����� ��� ��� ���������, � ������ ����� ����������� ������ � ���� ���� �����, ��������� ���� ������� ��� ������ ��������� �����. </li>
<li><strong>����������</strong>. ������ ������ �������, �� ������ ����� ���������. ���������� ���������� ������ ��������, �� ����� ������� ���-��, ��� ������� ���. � ������ ���������� ���� ���� ������, ����������� ��� � ������ ����, � ������ ������� ����� ����� ���� ����������� ��� ����� �� ����.</li><li><strong>����������� ������</strong>. ������ ������� �����, ��������� �� �������� ������� ������� ����� ��� � ����������������, ��������� �� ������� �����, � ��� ��� �����������. ����� ������ �� ��������� �� ���������� � �� ������� �������� ������. </li>
</ul>
</p>
<h2>�������� ������� ����</h2>
<p>������������ ��������� ���� ����� ��� ��������� �������� �� ������ � ��������, ������ ����� 30 ���������� �  �����, ����� ���� � ������. � ��� ����� ����������� �������� ��������.</p>
<h2>����� ���� ��� �������?</h2>
<p>�������� ������ ���� ������ � ������ ���������� (20-25 ����������) �������� ������ � ����������, ��� � ������ ������ �������� ��������� ���������. ���� ������ �� ���� � ������� �� ��� ����.</p><p>����� ��������� ������� �������� � ������ ����������, ���� ��� ��������� ���������: �������� � ������ (���-��), ������ ������, ������ ���������� (� ���� ����������� ������� ��� ������ ����������).</p><p>� ����������� ������ ����-����� ����� ��������� ��������� ����� �������� ������.</p><p>���-�� ���������� ���� ���������� ����� ������� ����������.</p><p>����� �� ���� ���� ��������������� ����� ��������.</p>
<h2>����� �� ������� ����������������� ��������/��������/����������?</h2><p>������� ������ ������� ����� ���������� ������� ������� ��� �������, ������������ �� ��������� ������ ����� � ����������� ���� ������ ����������. ��������, ���� ���������� ����� ����� �� ���� �� ����, � ��������� ������ ��������� ������� � ������� ����� � ������� ��������, �� ��������� � ����������� � ���������. �� ������������ ��������������� ����� �������.</p>
</td>
<? right_block('krynn'); ?>
	     </tr>
<? show_footer(); ?>

