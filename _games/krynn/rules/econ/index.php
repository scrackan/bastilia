<?
define("ROOT","../../../../");
require_once(ROOT . 'funcs.php');
show_header("����� :: ������� :: ���������",KRYNN);
show_menu("inc/main.menu"); ?>

	    <td class="box">
 <div class="boxheader"><a href="/">�����: ����� �����</a> :: <a href="/rules/">�������</a> :: ���������</div>
 <div style="margin:1em">
<p>��&nbsp;���� ���� �������, ��&nbsp;������ �&nbsp;�����������, �������� �&nbsp;������������� ������. ��&nbsp;���� ���: �������� ��������� �&nbsp;������ �����, �������� ��&nbsp;������� ��������.</p>
<h2>���</h2>
<p>��&nbsp;����� ���� ��������� ���� ��� �������� ������� ���������. ������� ��� ���:
<ol>
<li>����� (����������);</li>
<li>����� (������);</li>
<li>���������� (�����);</li>
<li>������� ����/������������ (����� ����);</li>
<li>��������� (�����);</li>
<li>���������/��������� (����� ����);</li>
<li>������� (������);</li>
<li>���������� ������� (���-��).</li>
</ol>
<p>
����������� ���������� ��� ��&nbsp;����� ������ ���������� �������� ��� ������� ��&nbsp;����.
��������, ������� ��������� ���������� ��� ��� ������� ����� ��������� ��&nbsp;����� ������,&nbsp;&mdash; ��&nbsp;���� �������� ������, �������� �&nbsp;������������ ����������. �&nbsp;��������, ������� ��&nbsp;��� ��������� ��&nbsp;����� ������, ��������� ���������� ����� �&nbsp;�����������, ��� ��������.
��� ��������� ��� ���������� �������� ��������������, ������������, ������� ����������� ��&nbsp;�����.</p>
<p>��&nbsp;���� ����� ������, �&nbsp;������� ����� ����� ������, ���������� �&nbsp;���������� �&nbsp;������� �����������. ����������� �������� ������ ��� ��������������� �&nbsp;������ ��&nbsp;���� �����������. ������ ������� ���������� ������������� ������������� ����� (50 ��. �.) ��� ���� � ���� (12:00, 20:00), ��� ���������� ������� �� ������� ������� ���������.</p>
<h2>������</h2>
<p>��&nbsp;���� ���� ��� �������� �������&nbsp;&mdash; �������� �&nbsp;������� ������.<br>
<b>��������</b> ������, ������� ������� ������ �����, �������� �������� �������� ��������, ����������� �����. <br>
<b>�������</b> ������ ���� ��&nbsp;����� �&nbsp;����, �������������� ��&nbsp;����������. ��&nbsp;���� ������� ������ ����� ���� �&nbsp;����������, ������� ��������� ������� �&nbsp;��� ������, ���� ����� ����. ��������� ���� �����&nbsp;&mdash; 1&nbsp;�������� ��&nbsp;5&nbsp;�������.<br>
��������� ������&nbsp;&mdash; ������ ��&nbsp;�����. <a href="#mm">���������� ��������</a> ��������� �&nbsp;�����, �&nbsp;������. </p>
<h2>������� �&nbsp;��&nbsp;������</h2>
<p>�&nbsp;�������� ���������:</p>
<ol>
<li>���������� ����� (�������� �&nbsp;�������);</li>
<li>�����;</li>
<li>������� ��� ����������� ����/������� (�������);</li>
<li>������� ���;</li>
<li>���� (��������� ��������� ��� �����).</li>
</ol>
<p>������� (������� ������� ���) ������ ��������� ��&nbsp;���� ��������������, ����� ������ �������� ��� �������� ��&nbsp;����.</p>
<p>����� ������ ��������&nbsp;&mdash; ��������������. &laquo;�������������&raquo; ���� ����������� ����� ���������, ��&nbsp;��&nbsp;����������� ��������� �&nbsp;��� ������ ���������.
������������� �������� ����� ���������� �������� (�&nbsp;������� ������ ���������).
��������� ���� ��&nbsp;��� ������� ��&nbsp;������ ���� ����� ���� ���������, ��&nbsp;����� �������� ��&nbsp;���� ��� ������.</p>
<h2>��������</h2>
<p>�������� ������ ��������� ��������.<br>
��������� �&nbsp;����������� ��&nbsp;������� ��������� ������ (����� ����� ����� ��������, �������� ��� ������). ���������� ��� ����������:
<ol>
<li>����� �������� ������, ������������ �&nbsp;��������� ������� ��&nbsp;����� �&nbsp;��������� ������ ��&nbsp;����� ���� ������;</li>
<li>������������� ��������� ����� �������� ��������� �&nbsp;��� ������ ��� ������.</li>
</ol>
<p>������������� �&nbsp;������ ��������� ����� �����. ������������ �������� �����:
<ol>
<li>�������;</li>
<li>��������;</li>
<li>����� �����;</li>
<li>���������� �����.</li>
</ol>
<p>���������� ����� ������ ����������� ��� ������� ��&nbsp;����� �&nbsp;����� ����� (���, ��� ������, ��&nbsp;������� ������). ����� �&nbsp;����� ����� ���������� ��������.
����� ����� ������������ �&nbsp;������ �������� ��&nbsp;�����.</p>
<p id="mm">��&nbsp;���� ���������� ��� ��������������� �������� (��&nbsp;������ ������ ��������, ��� ���������), ������������ ������� ��������, ������ �� ������, ������ �� �������. ��� ������� �&nbsp;�������� �������. ���� �&nbsp;��������� ������, ���������� ����������� ���������� ��&nbsp;����. �������� �������� �� ���������� � ���� ��� �� ����.</p>
<h2>�������������</h2>
<p>������������ ������� ��������� ����� ����� �&nbsp;��������� �&nbsp;����������� ��&nbsp;������� �&nbsp;������������ �������, ������� ����������� ��&nbsp;�����, ����������� ���� ��������&nbsp;&mdash; <b>�������</b>.
������� ����� ���� ��������� ����������� ����� ��&nbsp;������������� ������, ��&nbsp;������ �&nbsp;���� ���������. 
������ ������ ����� ��������� �&nbsp;�������� �&nbsp;�&nbsp;�������� ������� (����� �����, ��� ������������� �������� ������� ��&nbsp;�����). ����� ������ ������� ������� ��������� ��� ���������� �������. ����� ����, ��� ������� ��������� �&nbsp;����������� ��&nbsp;�����, ������ ��������� �����������. ������� ������������� ��������� �������� ������, ���� ������� ����� ������ ������� ����������. ��&nbsp;��������� ����������� ������ ������� ������ ������� ��&nbsp;���� ��������� ���������� ����������� ��������.
���������:</p>
<table class="roletable" border="2" cellpadding="1" cellspacing="1" align="center" id="building">
<tr><th>��������</th><th>������</th><th>�������</th><th>��.�.</th><th>�� ������ ����</th></tr>
<tr><td><b>������, ������������� ������������</b> ������������ ��������</td><td>�&nbsp;��������� ����� ������ ������ �������� ������������� ����. ����� ��������� ������ ����� ���������&nbsp;&mdash; ������� ��&nbsp;���������</td><td>20</td><td>10</td><td>����� �� ���������</td></tr>
<tr><td><b>�����</b></td><td>��������� ������� ��� ������� ��������, ��&nbsp;�������� ���������. ����� ����� ������ �������� �����.</td><td>20</td><td>8</td><td>���� � ����� �����</td></tr>
<tr><td><b>����������</b></td><td>��������� ��������� ������� ������� ���������; ������ ��������� ��&nbsp;���������� ����� �����, �&nbsp;�������&nbsp;&mdash; ������ �������� �����.</td><td>10</td><td>8</td><td>���� � �����</td></tr>
<tr><td><b>����</b></td><td>������� �&nbsp;�������� ���� ������-�� ����; �������� �������� �������� ����� ��������� ��������������� ����� ����. �&nbsp;��������� ����� ���� ��������� ����� ������, ���� ����������, �������.</td><td>10+<sup>1</sup></td><td>10</td><td>����� �� ���������</td></tr>
<tr><td><b>���������� �����</b></td><td>���������� ��������</td><td>20</td><td>10</td><td>����� �� ���������</td></tr>
<tr><td><b>���������</b></td><td>�������������� ������ �� ����������, �������� �� ������ � ������������ ������� � ��������.</td><td>30</td><td>12</td><td>����� �� ���������</td></tr>
<tr><td><b>�������</b></td><td>��������� �������� �&nbsp;�������� ��������� ��� ������������ ������ (������) � ������� �����</td><td>10</td><td>8</td><td>���� � ������</td></tr>
<tr><td><b>�����</b></td><td>��������� ������������ ��������. ����� ��������� ��� ������ (������� ����� ���������)</td><td>0</td><td>12</td><td>���� � ������, ����� ����� � �����</td></tr>
</table>
<p><sup>1</sup> ��� ������ �������� ��������� �� ����, ��� �� ������.</p>
<p>����� ������ ����� ��������� ��&nbsp;����� ��� ���� ����������� �������, ���� ������� �&nbsp;��&nbsp;�����. �&nbsp;���� ������ ������ ������������ ���������. ������� ���������� ������������, �������� �� ������ � ������� ����� ��&nbsp;��&nbsp;1/2 ���� � �������� �������. </p>
<h2>����������� �&nbsp;������������� ����</h2>
<p>����������:
<ol>
<li>������;</li>
<li>�������;</li>
<li>������� �����;</li>
<li>������� ��������� (��������, ������, �����...);</li>
<li>���������� ��������, ������� ���������� ������ �&nbsp;�������;</li>
<li>���������;</li>
<li>����� �&nbsp;������, ������� ����������;</li>
<li>�����;</li>
<li>���������� ����� &laquo;������� �&nbsp;����&raquo;.</li>
</ol>
<p>��� ��������� ������������, ������� ������ �&nbsp;�������. �&nbsp;������ ����� ������ �&nbsp;������� �������� �&nbsp;������������ �&nbsp;���� ��� ��������. ����� ������ ��&nbsp;����� ������� ����� �������.</p>
<h2>��������� ����</h2>
<p>������ ���� ���� ��������� ��� ��������� �������� �� ������ ���� � ����� ���������� �� ������� ����������. ������� �������� � 1 �������� ������,
��� ��������� ������ ����, ����� ����-�� ��������, ������� ��� ��� �����-������ ������.</p>
<table class="roletable" border="2" cellpadding="1" cellspacing="1" align="center" id="prices">
<tr><th>���</th><th>����</th></tr>
<tr><td colspan="2"><b>��������� ��������</b></td></tr>
<tr><td>������</td><td>2 ��.�.</td></tr>
<tr><td>�������� �������� ��������</td><td>5 ��.�.</td></tr>
<tr><td>�����</td><td>8 ��.�.</td></tr>
<tr><td>����� ������� (��������, ���)</td><td>12 ��.�.</td></tr>
<tr><td colspan="2"><b>�������</b></td></tr>
<tr><td>�����</td><td>1 ��. �. �� ������ (���� �����)</td></tr>
<tr><td>�����</td><td>1 ��. �. �� ������</td></tr>
<tr><td>����������</td><td>1 ��. �. �� ������</td></tr>
<tr><td>������� ����/������������</td><td>1 ��. �. �� ������</td></tr>
<tr><td>���������</td><td>1 ��.�. �� 4 ��.</td></tr>
<tr><td>���������/���������</td><td>1 ��.�. �� 1 ��.</td></tr>
<tr><td>�������</td><td>2 ��.�. �� ������</td></tr>
<tr><td>���������� �������</td><td>1 ��.�. �� ������� ������  (� ���� �����)</td></tr>
<tr><td>����</td><td>4 ��. �. �� ����</td></tr>
<tr><td>������� ��������</td><td>5 ��.�. �� 0,2 �����</td></tr>
<tr><td>�����</td><td>1 ��.�. �� ���������</td></tr>
<tr><td>������</td><td>2 ��.�. �� 0,2 �����</td></tr>
<tr><td>����� ��� �����</td><td>0,5�1,5 ��. �. �� �������</td></tr>
<tr><td colspan="2"><b>���� � �������</b></td></tr>
<tr><td>������ ����� ��� �������</td><td>0,5 ��.�.</td></tr>
<tr><td>������ ����</td><td>1 ��.�.</td></tr>
<tr><td>����� ��������</td><td>1 ��.�.</td></tr>
<tr><td>��������� �������</td><td>0,5 ��.�.</td></tr>
<tr><td>������� ������� ���</td><td>1 ��.�.</td></tr>
<tr><td colspan="2"><b>������ ������</b></td></tr>
<tr><td>������� �����</td><td>1,5�3 ��.�.</td></tr>
<tr><td>���������� ������� ��� ������</td><td>�� 3 ��.�.</td></tr>
<tr><td>������� ������</td><td>1 ��. �.</td></tr>
<tr><td>��������� �� ������������ ������� (���������� � �������� ��� ������� �������)</td><td>2 ��.�.</td></tr>
<tr><td>����� (���������� � �������� ��� ������� �������)</td><td>20 ��.�.</td></tr>
<tr><td>����� ��������������</td><td>1 ��.�.</td></tr>
<tr><td>������������� � �����</td><td>�� 1 ��.�.</td></tr>
<tr><td colspan="2"><b>������</b></td></tr>
<tr><td>��������</td><td>1 ��.�. �� ������</td></tr>
<tr><td>��������� ����� ������� �����</td><td>�� 1 ��.�.</td></tr>
<tr><td>������� � ������</td><td>2�4 ��.�.</td></tr>
</table>
<p>����������: ��� ������������� �������� 0,5 �������� ������, ������ ����� ��������� ������� �������.</p>
</div></td>
<? right_block('krynn'); ?>
	     </tr>
<? show_footer(); ?>

