<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� :: �������",KRYNN);
show_menu("inc/main.menu"); ?>
<style type="text/css">
<!--
.�����3 {color: #000000}
-->
</style>


	    <td class="box">
 <div class="boxheader"><a href="/">�����: ����� �����</a> :: ������� </div>
 
<p>
<table border="1" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
<tr>
<td><h2 align="center"><a href="krynn-dobiralovo.doc">������� ������ ��� ������</a></h2></td>
</tr>
</table>
<p class="�����3">���� ������� ��&nbsp;������ ����� ��������� ����������� �&nbsp;�������� (����������� �����). ��� ���������� ��&nbsp;���� &laquo;�����&raquo;, ��� ��������� ���� ���� <a href="http://bsg.bastilia.ru/">&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</a>.</p>
<p><strong>��&nbsp;����������� �������� ��&nbsp;������� ��&nbsp;������� 19:00&nbsp;��������! ����� ��&nbsp;������� ���������� ��&nbsp;����������� ������ �&nbsp;�������� �&nbsp;����. ��� �������������� �&nbsp;���������, ���� ������������ ������.</strong></p>
<h3>��� ����� �������</h3>
<p>��&nbsp;������������ ����������:</p>
<ol>
<li><a href="#1">���������� ��&nbsp;������� + ������� (��� �����) + 3&nbsp;(��� <nobr>0-1,5)</nobr> ��&nbsp;������</a></li>
<li><a href="#2">������� ��&nbsp;��������� ��&nbsp;�������� + 3&nbsp;�� ������</a></li>
<li><a href="#3">������� ��&nbsp;������� ��&nbsp;������������ + 6&nbsp;�� ������ (��� ��&nbsp;�����)</a></li>
</ol>
<p>��&nbsp;����������:</p>
<ol>
<li><a href="#auto1">����� ����������� �����, �������� ��&nbsp;��������/��������/�����������</a></li>
<li><a href="#auto2">����� &laquo;�����������&raquo;, �������� ��&nbsp;������������, ����� ��&nbsp;����������/�����������</a></li>
</ol>


<a name="map" id="map"></a><p align="center"><a href="map-krynn.gif"><img src="map-krynn_mini.gif" width="770" height="481" border="0" /></a>
<p>������� �����&nbsp;&mdash; ������ ��&nbsp;�������� (�&nbsp;���� ����� �������� ������� �&nbsp;����� 7� &nbsp;�&nbsp;3�). ����� �����&nbsp;&mdash; ��������� ������.
<p>������� ��������&nbsp;&mdash; 1,4&times;1,4&nbsp;��. ��������������� ������������ �������:<br />
<strong>�</strong>&nbsp;&mdash; ���������� ������<br />
<strong>�</strong>&nbsp;&mdash; ���������� �������� (��&nbsp;���� ������ ��������)<br />
<strong>�</strong>&nbsp;&mdash; ����� ����<br />
<strong>�</strong>&nbsp;&mdash; �����<br />
<strong>�</strong>&nbsp;&mdash; ������<br />
<strong>�</strong>&nbsp;&mdash; ����-������<br />
<strong>�</strong>&nbsp;&mdash; ����-�����<br />
<strong>�</strong>&nbsp;&mdash; ���������� ���<br />
<strong>�</strong>&nbsp;&mdash; ����� �����<br />
<strong>�</strong>&nbsp;&mdash; ������� ���-�� 
<p>��������� ����������� �������� ����.<h3>�� ������������ ����������</h3>
<ol>
<li><a href="#1">��&nbsp;���������� ��&nbsp;�������</a>, �&nbsp;������:
<ol type="a">
<li><a href="#1a">��&nbsp;�������� ��&nbsp;�������� (�&nbsp;����� 3&nbsp;�� ������ ��&nbsp;��������)</a></li>
<li><a href="#1b">��&nbsp;����� ��&nbsp;�������� �&nbsp;��� (1,5&nbsp;�� ������) ��� ��������������� ��&nbsp;�������� </a></li>
</ol>
</li>
<li><a href="#2">��&nbsp;�������� ��&nbsp;��������� ��&nbsp;�������� (�&nbsp;����� 3&nbsp;�� ������ ��&nbsp;��������)</a></li>
<li><a href="#3">��&nbsp;�������� ��&nbsp;������� ��&nbsp;������������ (�&nbsp;����� 6&nbsp;�� ������ ��� ��&nbsp;����� ��&nbsp;��������)</a></li>
</ol>
<h4><a name="1" id="1"></a>1. ��&nbsp;���������� ��&nbsp;�������, �&nbsp;����� ��&nbsp;�������� ��� �����</h4>
<p>��� ����� ������� �������, �.�. ���������� ��&nbsp;����� �&nbsp;�������. ���� ������&nbsp;&mdash; <strong>146,3</strong>&nbsp;�. (��&nbsp;���������) ��� <strong>186,4</strong>&nbsp;�. (��&nbsp;������������ �������); ��������&nbsp;&mdash; ������. </p>
<p>���������� ���������� �&nbsp;������ ��� (<a href="http://tutu.ru/spb/rasp.php?st1=46405&amp;st2=47605">�&nbsp;�������� �������� ����</a>) �&nbsp;�������� �&nbsp;��������:</p>
<p><a name="rasp" id="rasp"></a>
<table border="2" align="center" cellpadding="2" cellspacing="2">
<tr>
<td><strong>����. ������ </strong></td>
<td><strong>���������</strong></td>
<td><strong>�������</strong></td>
<td><strong>����� ��������</strong></td>
<td><strong>������� ����������</strong></td>
<td><strong>��������� �������</strong></td>
</tr>
<tr>
<td align="right"><strong>6:10</strong></td>
<td align="right"><strong>6:41</strong></td>
<td align="right"><strong>7:58</strong></td>
<td><strong>���������</strong></td>
<td><strong>�������</strong></td>
<td><strong>8:10 (&#8470;&nbsp;646)</strong></td>
</tr>
<tr>
<td></td>
<td align="right">7:30</td>
<td align="right">8:47</td>
<td>���������</td>
<td>�������</td>
<td>9:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">7:47</td>
<td align="right">8:18</td>
<td align="right">9:38</td>
<td>���������</td>
<td>���������</td>
<td>9:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">8:08</td>
<td align="right">8:40</td>
<td align="right">9:57</td>
<td>����� �����������</td>
<td>�������</td>
<td>10:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">8:25</td>
<td align="right">8:56</td>
<td align="right">10:14</td>
<td>���������</td>
<td>���������</td>
<td>10:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td></td>
<td align="right">9:15</td>
<td align="right">10:33</td>
<td>����� �����������</td>
<td>�������</td>
<td>10:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">9:20</td>
<td align="right">9:51</td>
<td align="right">11:08</td>
<td>��&nbsp;��������</td>
<td>�������</td>
<td>12:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">9:56</td>
<td align="right">10:27</td>
<td align="right">11:45</td>
<td>���������</td>
<td>�������</td>
<td>12:50 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">10:45</td>
<td align="right">11:16</td>
<td align="right">12:33</td>
<td>���������</td>
<td>�������</td>
<td>12:50 (&#8470;&nbsp;897)* ��� 14:10 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td align="right"><strong>14:06</strong></td>
<td align="right"><strong>14:36</strong></td>
<td align="right"><strong>15:54</strong></td>
<td><strong>���������</strong></td>
<td><strong>���������</strong></td>
<td><strong>16:00 (&#8470;&nbsp;646)</strong></td>
</tr>
<tr>
<td align="right">14:40</td>
<td align="right">15:11</td>
<td align="right">16:28</td>
<td>����� ������</td>
<td>�������</td>
<td>17:15 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td align="right"><strong>15:20</strong></td>
<td align="right"><strong>15:51</strong></td>
<td align="right"><strong>17:09</strong></td>
<td><strong>��&nbsp;�������</strong></td>
<td><strong>�������</strong></td>
<td><strong>17:15 (&#8470;&nbsp;646)</strong></td>
</tr>
<tr>
<td align="right">16:16</td>
<td align="right">16:47</td>
<td align="right">18:04</td>
<td>���������</td>
<td>���������</td>
<td>18:20 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">16:32</td>
<td align="right">17:03</td>
<td align="right">18:20</td>
<td>���������</td>
<td>�������</td>
<td>18:20 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">17:25</td>
<td align="right">17:46</td>
<td align="right">18:36</td>
<td>��������� (��������) </td>
<td>���������</td>
<td>20:20 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">17:36</td>
<td align="right">18:06</td>
<td align="right">19:26</td>
<td>���������</td>
<td>�������</td>
<td>20:20 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td align="right">18:45</td>
<td align="right">19:18</td>
<td align="right">20:36</td>
<td>���������</td>
<td>���������</td>
<td>�����</td>
</tr>
<tr>
<td></td>
<td align="right">19:38</td>
<td align="right">20:57</td>
<td>��&nbsp;�������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td align="right">19:36</td>
<td align="right">20:07</td>
<td align="right">21:25</td>
<td>���������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td align="right">20:00</td>
<td align="right">20:29</td>
<td align="right">21:47</td>
<td>��&nbsp;��������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td></td>
<td align="right">20:29</td>
<td align="right">21:47</td>
<td>����� ������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td align="right">20:25</td>
<td align="right">20:55</td>
<td align="right">22:12</td>
<td>����� ������</td>
<td>���������</td>
<td>�����</td>
</tr>
<tr>
<td align="right">20:43</td>
<td align="right">21:06</td>
<td align="right">22:12</td>
<td>��&nbsp;��������</td>
<td>���������</td>
<td>�����</td>
</tr>
<tr>
<td></td>
<td align="right">21:52</td>
<td align="right">23:09</td>
<td>���������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td align="right">22:10</td>
<td align="right">22:41</td>
<td align="right">23:59</td>
<td>����� ������</td>
<td>�������</td>
<td>�����</td>
</tr>
</table>
<p>* ���� ������� ����� �&nbsp;��������, �.�. ��&nbsp;���� ��&nbsp;�����-����������.</p>
<p><em>���� ��������� ��&nbsp;�������, ���������� ������ (���� ��&nbsp;������, ��� ��&nbsp;�������� �������� ����������), ��&nbsp;�������� ����� <nobr>40-60</nobr> ����� ����� ���� ������ ��&nbsp;�������� &#8470;&nbsp;897 (��&nbsp;���������). ��� ���������� ����� ��&nbsp;���������. ��� ������� ��&nbsp;�����, ���� ��&nbsp;������ �����.</em></p>
<p>����������� ��� ������� �&nbsp;����� �������� ����.</p>
<p><strong><a name="1a" id="1a"></a>�. ��&nbsp;�������� ��&nbsp;�������� </strong></p>
<p>���������� ��������� ��������� ����� �/�-������� �������. ��� �������� ����� ��������, ������ ��&nbsp;������������ ��� ������������� (����������&nbsp;<a href="#rasp">��. ����</a>).</p>
<p>�������� <strong>�&nbsp;�������� </strong> &mdash; ��� �������� ��&nbsp;3&nbsp;�� ��&nbsp;��������. ����� ��������� �����, ���� �������� �&nbsp;��������, ���� �������� ��&nbsp;������������ ������. ��������� ����� &laquo;39&nbsp;��&raquo;&nbsp;&mdash; ����. ��. ����� &laquo;<strong>1�</strong>&raquo; <a href="#map">��&nbsp;����� ����</a>.</p>
<p>����� ����� 1,3&nbsp;�� ��&nbsp;������. ��������� ����� ��������� <strong>(1�)</strong>: ���� &laquo;����� ����������� ������ ��������&raquo;, ������������ ����� &laquo;40&nbsp;��&raquo; <strong>(2�)</strong>. ��&nbsp;����� ���� ��&nbsp;��� ����� ��������� ��������� �����. ��������� ������� ������� ����� ����&nbsp;&mdash; ������ <strong>(3�)</strong>. �������� �&nbsp;��� �&nbsp;������� <a href="#1c">�������� ������ ��������</a>. ������� �&nbsp;���� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������. </p>
<p><strong><a name="1b" id="1b"></a>b. ��&nbsp;����� ��&nbsp;�������� �&nbsp;��� </strong></p>
<p>��������������� ��������� �������� <nobr class="phone">(<strong>+7(921)368-28-70</strong>,</nobr> ������), �������, ��� ��� ����� �&nbsp;��������, �&nbsp;�������� ����� �������� ����������. ����&nbsp;&mdash; <strong>300&nbsp;�.</strong> ��������������������&nbsp;&mdash; 4&nbsp;��������. ���� ��� ��������� ������, �������� ��&nbsp;���� ��������&nbsp;&mdash; ��&nbsp;������� ��������.</p>
<p>��� ����� �������� ���� ������ ��������. ���������: ���� &laquo;����� ����������� ������ ��������&raquo;, ������������ ����� &laquo;40&nbsp;��&raquo; <strong>(2�)</strong>. ��&nbsp;����� ���� ��&nbsp;��� ����� ��������� ��������� �����. ��������� ������� ������� ����� ����&nbsp;&mdash; ������ <strong>(3�)</strong>.</p>
<p>������� �&nbsp;���� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������. ��������, ��&nbsp;������������ �&nbsp;��������� ��&nbsp;��������� ����� �������� �&nbsp;��&nbsp;����. �&nbsp;�����, ������� <a href="#1c">�������� ������ ��������</a>.</p>
<p><strong><a name="1c" id="1c"></a>c. ������ (��� ��&nbsp;����) ��&nbsp;�������� �&nbsp;��� ��&nbsp;��������</strong></p>
<p>�������� �&nbsp;��� <strong>(3�)</strong> �&nbsp;�������������� ��&nbsp;������� �����. ������ ��&nbsp;��������� ����� ��&nbsp;������� <strong>(4�)</strong>&nbsp;&mdash; ������, �� ���������. ����� ��&nbsp;������ ���� ����� ��������� �������. ��&nbsp;��������� �������� <strong>(5�)</strong> ������������ �������. ������ ��������� ������� <strong>(6�)</strong>, ��&nbsp;��&nbsp;������� ��� ��&nbsp;����, ����� �����. ��������� �������� ����� ��������� <strong>(�)</strong>&nbsp;&mdash; ������� ��&nbsp;�����. ��������� �����. ��� ������� ��&nbsp;��������&nbsp;&mdash; �����&nbsp;�� �������� �&nbsp;����� ��������. ��&nbsp;��� �������������� �&nbsp;�������, ���� ������������ ������. </p>
<h4><a name="2" id="2"></a>2. ��&nbsp;�������� ��&nbsp;���������</h4>
<p>��������� ��&nbsp;��.�. &laquo;���������&raquo; �&nbsp;���� ��&nbsp;���������� &laquo;��������&raquo;. ��� ����� ������� &#8470;&nbsp;897 (��&nbsp;�������������). ����������: <strong>8:40, 9:20, 11:20, 14:50, 16:50, 18:50</strong> (����������� �������������� �������� ��&nbsp;�������� <nobr class="phone">635-81-58,</nobr> �.�. ������ ��������� ����� ����������). ����� ����� ��<strong>&nbsp;��������</strong> (<strong>140&nbsp;�.</strong>), ����� �&nbsp;����&nbsp;&mdash; <strong>2&nbsp;����</strong> (��� �������&nbsp;&mdash; ������).</p>
<p><em>���� ��&nbsp;������, �������� �&nbsp;��������, ��&nbsp;������� ��&nbsp;��������� ��������, ����� ���� �������, �&nbsp;���������. ���������� ��&nbsp;���� ��������&nbsp;&mdash; ��&nbsp;����� ����� ������������. </em></p>
<p>�������� <strong>�&nbsp;�������� </strong> &mdash; ��� �������� ����� 17&nbsp;�� ����� ��������� �&nbsp;������� (��&nbsp;������ ��&nbsp;��������) �&nbsp;��&nbsp;3&nbsp;�� ��&nbsp;��������. ����� ��������� �����, ���� �������� �&nbsp;��������, ���� �������� ��&nbsp;������������ ������. ��������� ����� &laquo;39&nbsp;��&raquo;&nbsp;&mdash; ����. ��. ����� &laquo;<strong>1�</strong>&raquo; <a href="#map">��&nbsp;����� ����</a>.</p>
<p>����� ����� 1,3&nbsp;�� ��&nbsp;������. ��������� ����� ��������� <strong>(1�)</strong>: ���� &laquo;����� ����������� ������ ��������&raquo;, ������������ ����� &laquo;40&nbsp;��&raquo; <strong>(2�)</strong>. ��&nbsp;����� ���� ��&nbsp;��� ����� ��������� ��������� �����. ��������� ������� ������� ����� ����&nbsp;&mdash; ������ <strong>(3�)</strong>. �������� �&nbsp;��� �&nbsp;������� <a href="#1c">�������� ������ �������� ���� </a>. ������� �&nbsp;���� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������.</p>
<h4><a name="3" id="3"></a>3. ��&nbsp;�������� ��&nbsp;�������</h4>
<p><strong>������� ���������� ����� ��������&nbsp;&mdash; ���� ������ ������ ����� 6&nbsp;��. ��&nbsp;���� ��� ��� ��&nbsp;�������&nbsp;&mdash; ��������. </strong></p>
<p>
��������� ��&nbsp;��.�. &laquo;������&raquo;. (����������� ������� ���� ������ ��&nbsp;��.�. &laquo;�������� �����������&raquo;, ���� ���� ��������� ��&nbsp;<nobr>13-��������</nobr> �������� �������� ��&nbsp;&laquo;�������&raquo;&nbsp;&mdash; ���������� ��� �����, ����� ��&nbsp;�������� ��&nbsp;�������).<p>
<a href="http://maps.yandex.ru/-/CNqQIhd">���������� ���������</a> ��������� ����� �������� ������ ��&nbsp;����� &laquo;������&raquo;. ��� ����� <strong>������� &#8470;&nbsp;678</strong> (��&nbsp;�����������).
<p>
���������� ��������: <strong>9:00, 11:00, 13:00, 15:00, 17:00, 19:00, 20:00, 21:00</strong> (����������� �������������� �������� ��&nbsp;�������� <nobr class="phone"><span class="phone">+7(921)344-10-38</span>,</nobr> �.�. ������ ��������� ����� ����������).
<p>
���� ������&nbsp;&mdash; <strong>180&nbsp;�.</strong> ����� �&nbsp;����&nbsp;&mdash; <strong>2&nbsp;����</strong> (��� �������&nbsp;&mdash; ������). ����������� ��������� �������, ����� ������ ������� �����.
<p>
������� &#8470;&nbsp;678 ����� ��������� ���� ��.�. &laquo;������&raquo;. ����� ������ ��&nbsp;����� ������������� �&nbsp;����������� �����, ��&nbsp;�������� �����&nbsp;&mdash; <a href="http://maps.yandex.ru/-/CNqQIhd">��� ����� ���������</a>. ������� ��������� ��&nbsp;���-�� ����� <nobr>10-15</nobr> ����� ����� ������ �&nbsp;�������.<p><em>
�������� �&nbsp;������� ��&nbsp;����������� ������ ���� �&nbsp;��� ���� �������, ������� ���� ��&nbsp;������� �&nbsp;����� ��� �����&nbsp;&mdash; ����� ���� ���� ����� ��� ������ ������! </em>
<p>�&nbsp;����������� ����� ����������� ������ ����� ��&nbsp;��������&nbsp;&mdash; �������������� ��&nbsp;�����. �&nbsp;������� ���������� ��������:

<p>
��&nbsp;��������� �&nbsp;<a href="http://maps.yandex.ru/-/CNqUY1D">�����������</a> (��. ����� &laquo;<strong>1�</strong>&raquo; <a href="#map">��&nbsp;����� ����</a>) ����� ��&nbsp;������ �&nbsp;������� ��������/�������. �������� ����� 2&nbsp;�� ��&nbsp;������� ��������� &laquo;�����&raquo; (��. ����� &laquo;<strong>2�</strong>&raquo; <a href="#map">��&nbsp;�����</a>). ����� ��� 1&nbsp;�� ��&nbsp;���������. ����� ����� ������� ��&nbsp;&laquo;�����&raquo; <strong>(3�)</strong>. ��� ���� ��&nbsp;����. ������ ����� ��������� ������� ��&nbsp;&laquo;��� �����������&raquo; <strong>(4�)</strong>. ���� ���� ��&nbsp;����. ��������� ��������&nbsp;&mdash; ����� �&nbsp;���������� ������� <strong>(5�)</strong>. ��&nbsp;�������� ����� ������ <strong>(6�)</strong> ��������� �����. ��������� �������, �&nbsp;��������, ���� �����, ��&nbsp;��� ������ ��&nbsp;����� ����, �&nbsp;��&nbsp;����, ��� ��� ������� ����������. �&nbsp;�����, ��� ���� ������� ��&nbsp;��������� �������� <strong>(7�)</strong>. ������� �&nbsp;����� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������.
<p>���� ��&nbsp;����� ���� ��&nbsp;��� ��������� ����������� �����&nbsp;&mdash; ������, �� ���������. ����� ������ ��&nbsp;����������� <strong>(8�)</strong>. ������������� ������ ��&nbsp;�������. ������ ���-�� ����� ������� <strong>(�)</strong>, ��&nbsp;��������� ��&nbsp;������� ����������� <strong>(�)</strong>. ��� ������ ����������. ����� ����� ����-������. ��������� �������, �&nbsp;������ ������� �������� <strong>(�)</strong>. �����&nbsp;�� �������� �&nbsp;����� ��������! ��&nbsp;��� �������������� �&nbsp;�������, ���� ������������ ������. 
<h3>��&nbsp;����������</h3>
<h4><a name="auto1" id="auto1"></a>1. ��&nbsp;������������ �����</h4>
<p>������� ��&nbsp;������������ ����� ��&nbsp;�������, ���������� ��� � ����� 61 �� (����-���� �� ������� 62 ��) ������������� ������ ��&nbsp;�����������. �������� ����� 10&nbsp;�� ����� �������� ��&nbsp;��������� �&nbsp;������� ��������. ��� ����� �������� ���� ������ ��������. ���������: ���� &laquo;����� ����������� ������ ��������&raquo;, ������������ ����� &laquo;40&nbsp;��&raquo; <strong>(2�)</strong>. ��&nbsp;����� ���� ��&nbsp;��� ����� ��������� ��������� �����. ��������� ������� ������� ����� ����&nbsp;&mdash; ������ <strong>(3�)</strong>.</p>
<p>������� �&nbsp;���� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������. ����� ������� ��&nbsp;<a href="#1c">�������� ������ �������� ����</a>.</p>
<h4><a name="auto2" id="auto2"></a>2. ��&nbsp;������ &laquo;�����������&raquo;</h4>
<p>��&nbsp;&laquo;�����������&raquo; ��� ����� ��������� ������� ��&nbsp;������������. �����&nbsp;&mdash; �&nbsp;������� ���������� �&nbsp;������������. ����� ������������ ���������� �������� 10&nbsp;�� ��&nbsp;��������. ��&nbsp;����������� ������� ��&nbsp;�������� <strong>�&nbsp;��� ������������</strong>, ����� ����� ��&nbsp;���������� ������ ������� �&nbsp;���.</p>
<p>��� ����� ������� ��&nbsp;�������� (�&nbsp;������� ������������). ���������: ���� &laquo;����� ����������� ������ ��������&raquo;, ������������ ����� &laquo;40&nbsp;��&raquo; <strong>(2�)</strong>. ��&nbsp;����� ���� ��&nbsp;��� ����� ��������� ��������� �����. ��������� ������� ������� ����� ����&nbsp;&mdash; ������ <strong>(3�)</strong>.
<p>������� �&nbsp;���� ����� ���������� ����� �������� ������� ������� ��&nbsp;��������. ����� ������� ��&nbsp;<a href="#1c">�������� ������ �������� ����</a>.</p>
<h3><a name="gps" id="gps"></a>������������� GPS-�����������</h3>
<p><strong>OziExplorer</strong>: <a href="gps/050k--p35-132-1_2.gif">�����</a> �&nbsp;<a href="gps/050k--p35-132-1_2.map">���� ��������</a>.</p>
<p><strong><a href="http://www.oruxmaps.com/index_en.html">OruxMaps</a></strong> (���� ��� Android, �����������!): <a href="gps/krynn-oruxmaps.rar">�������� �����</a> �&nbsp;���������� ����� &laquo;�����-�������&raquo; ��&nbsp;���������� �&nbsp;<em>sdcard/oruxmaps/mapfiles</em></p>
<p><strong>������� � ��. </strong>: ����� �&nbsp;������ �&nbsp;��� (����� &laquo;<strong>3�</strong>&raquo; ��&nbsp;�����)&nbsp;&mdash; <strong>N60.59219&deg;, E29.98057&deg;</strong></p>
<h3>�������� ������</h3>
<h4>������� &#8470;&nbsp;646 ��&nbsp;�������� ��&nbsp;�������</h4>
<p>���������� ��&nbsp;����������� ������ �����. </p>
<h4>������� &#8470;&nbsp;897 ��&nbsp;�������� ��&nbsp;��������� </h4>
<p>��&nbsp;����� ������ ����� ������ ����� �������� ��&nbsp;�������������� (35&nbsp;�� ��&nbsp;��������): 7:40, 12:00, 15:20. ��&nbsp;������� ������ ��� �&nbsp;��������&nbsp;&mdash; ���������� �����.</p>
<h4>������� &#8470;&nbsp;678 ��&nbsp;������������ ��&nbsp;������� </h4>
<p class="�����3">
����������: 6:10, 7:10, 11:40, 13:40, 15:40, 17:40, 19:40 
<h4>���������� ��&nbsp;������� </h4>
<p>
���������� ��&nbsp;21&nbsp;�������:
<table border="2" align="center" cellpadding="2" cellspacing="2">
<tr>
<td><strong>�������</strong></td>
<td><strong>���������</strong></td>
<td><strong>����. ������</strong></td>
<td><strong>����� ��������</strong></td>
<td><strong>������� �����������</strong></td>
</tr>
<tr>
<td align="right">4:47</td>
<td align="right">6:16</td>
<td align="right">6:46</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">5:20</td>
<td align="right">6:39</td>
<td align="right">7:09</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">6:14</td>
<td align="right">7:32</td>
<td align="right">8:03</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">7:55</td>
<td align="right">8:49</td>
<td align="right">9:12</td>
<td>��������� (��������) </td>
<td>���������</td>
</tr>
<tr>
<td align="right">8:01</td>
<td align="right">9:19</td>
<td align="right">9:49</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">9:10</td>
<td align="right">10:29</td>
<td align="right">10:59</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">9:34</td>
<td align="right">10:53</td>
<td align="right">11:23</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">11:00</td>
<td align="right">12:18</td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">13:03</td>
<td align="right">14:23</td>
<td align="right">14:53</td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">13:37</td>
<td align="right">14:58</td>
<td align="right">15:27</td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">14:12</td>
<td align="right">15:31</td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">14:45</td>
<td align="right">16:05</td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">15:23</td>
<td align="right">16:15</td>
<td align="right">16:37</td>
<td><p>��&nbsp;�������� (��������) </p></td>
<td>���������</td>
</tr>
<tr>
<td align="right">15:51</td>
<td align="right">17:11</td>
<td align="right">17:41</td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">16:26</td>
<td align="right">17:46</td>
<td align="right">18:16</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">17:01</td>
<td align="right">18:20</td>
<td align="right">18:50</td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">17:27</td>
<td align="right">18:46</td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">17:53</td>
<td align="right">19:13</td>
<td align="right">19:43</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">18:14</td>
<td align="right">19:33</td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">18:34</td>
<td align="right">19:53</td>
<td align="right">20:23</td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">19:02</td>
<td align="right">20:23</td>
<td align="right">20:53</td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">19:20</td>
<td align="right">20:44</td>
<td align="right">21:15</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">19:46</td>
<td align="right">21:05</td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">20:11</td>
<td align="right">21:30</td>
<td align="right">22:00</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">20:45</td>
<td align="right">22:05</td>
<td align="right">22:35</td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td align="right">21:43</td>
<td align="right">23:01</td>
<td align="right">23:34</td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td align="right">22:03</td>
<td align="right">23:19</td>
<td align="right">23:47</td>
<td>��&nbsp;������������</td>
<td>�������</td>
</tr>
</table></td>
<? right_block('krynn'); ?>
	     </tr>
<? show_footer(); ?>

