<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ������ ��������",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ������ ��������</div>
<p><strong>����� 1</strong>
<p>�������, �&nbsp;��� ��&nbsp;����� �&nbsp;����� �&nbsp;������� ���������� ������������� �����������, ��� ����� ����� ���� ����� ������������ ������ ��&nbsp;����������. �&nbsp;��������� ������� �&nbsp;����� �&nbsp;�����: ��������, ������ ������ ����� �&nbsp;��������� �������, ������ �&nbsp;�����. �&nbsp;��������, ������� ������������ �&nbsp;����������, ������ ��������� �&nbsp;5&nbsp;�� ��&nbsp;��&nbsp;�����. �&nbsp;��������� ����������, ��� ��������� �&nbsp;�����������. ��&nbsp;�������� �&nbsp;��������� �����, ��&nbsp;���� ����� �������������� ��������. �&nbsp;�������� ����� ��������� ����, �&nbsp;�&nbsp;��&nbsp;����� ������ ����� ��� ����� ������ ��&nbsp;����� �����. ��&nbsp;���� �&nbsp;�������, ���, ������ ����, ����� ����� ��� ����� ����������� ����.
<p>���� �������� �����������, ��� ��� ��������. </p>
<p><strong>����� 2</strong></p>
<p>��&nbsp;����� �������� �����, ��&nbsp;��&nbsp;���������, ����� ����� �&nbsp;��&nbsp;��������. �&nbsp;�����-�� ������ ��&nbsp;���� �����, ��� ���� ���� ��&nbsp;������ ��� ������, �������, ��&nbsp;�������, ��&nbsp;�����. </p>
<p>����������� ����� ����� ��&nbsp;���-�� ������. ���� ����� ���������, ��� ��� ��������. ��� ��������� �������? ��&nbsp;�������� ���� ����, ���������� ������� �&nbsp;��������� ������� ������: ���������� �����, ����, ������. ��&nbsp;������ ��&nbsp;������ ����� ����� �������, ����� �������� ���������������� ����� ��&nbsp;����� ����� �&nbsp;�������� ��&nbsp;�&nbsp;�����. ��� ����������� ��������� �&nbsp;��������� ��&nbsp;������ ������������ �����. �&nbsp;��&nbsp;����� �����, ��� ����� ����� ����� �������� ��������� �������� ����. <br />
<br />
����� ������ ����� ��&nbsp;������������. �&nbsp;��&nbsp;������ ������������ ���� ��������� �������������. </p>
<p>������� ���� ��������� ������, ��� �&nbsp;��� ����������: </p>
<p>&laquo;���� ������&nbsp;&mdash; ��� ������ ������ �&nbsp;�������. ��� ������ ���� �����. <br />
�&nbsp;������� ������ ������, ������ ��� ��� ��&nbsp;����, �&nbsp;���� ���� ��� ����� ��������� ������, ������ ��� ���� ������ �������� (�������� �����). �&nbsp;����&nbsp;�� ���� ���������� ������. </p>
<p>��&nbsp;����� ��� ������������ �&nbsp;�������, ������� ��������� �������. ������� �������� �������� ��������� ���������, �������&nbsp;&mdash; ��������&raquo;. </p>
<p>�&nbsp;����� ����, ��� ����� �������� ���� ��� �����������... ��� �������� �������� ������, ��&nbsp;������������ ����� �������� ���������&nbsp;&mdash; ������ ���� ��� ��������� ������� ��&nbsp;�����. ����� ����� ������������� ���� ��� ����������� �������� ����� ���� �&nbsp;����� ������ �������. &laquo;����� ���� ��������� ���� ���, ��&nbsp;������� ����� ����� ������� ��� ������.&raquo; <br />
<br />
<strong>����� 2</strong></p>
<p>��&nbsp;�������, ��� ��&nbsp;��� ��&nbsp;�����, �&nbsp;������, ��� �&nbsp;�������� ����� �������. �&nbsp;����&nbsp;&mdash; ���� ��&nbsp;����. ��� ��� ���������: &laquo;��������, ����������!&raquo;, �&nbsp;��&nbsp;�������� ������� �����. �������������&nbsp;�� ��&nbsp;��&nbsp;����� ������? </p>
<p><strong>����� 3</strong></p>
<p>��&nbsp;����� ��&nbsp;�&nbsp;����, �������, �������, �&nbsp;�����&nbsp;�� ��������� �������������� �������.<br />
<br />
��&nbsp;����� ��&nbsp;��������� ������ �&nbsp;�����, ��� ��� ���������. ���, ��&nbsp;����� ����, ��� ��������� ��� ������ ����� ��������, �����, ��&nbsp;����� ��������&nbsp;&mdash; ������-����� ����, ����� �������� ������� �����&nbsp;�� ������� ������. ��&nbsp;��&nbsp;������ ��� �������, ��&nbsp;������� �������� ��� ����. ��� ��� ����� ����� �&nbsp;������ ����� ��&nbsp;����� �������. �&nbsp;��� ��������� ���������� ��&nbsp;��������... </p>
<p>���&nbsp;��, ��� ���� �����, ��, ���� ��&nbsp;����� ��� ��� �����. ��&nbsp;��������� �&nbsp;�������������� ��&nbsp;��&nbsp;�����</p>
<p><strong>����� 4</strong></p>
<p>��� �������. �������, ��� ������ ���� �������&nbsp;&mdash; �&nbsp;���������, �&nbsp;����� ������ ������. �&nbsp;���������, ��� ����, ����� �����.</p>
<p>���� ����� ������� �&nbsp;��������, �������������� ������� �&nbsp;���. ����� ��.</p>
<p>������ ����. �������&nbsp;&mdash; ���, ���� ������� ����� �������� �����&nbsp;&mdash; ������� ������ ������ ��&nbsp;������� �������. ���� ���. ����� ���������� ��&nbsp;����. ��&nbsp;��&nbsp;��������� �&nbsp;�������� ���������� ���� ������. ����&nbsp;&mdash; ������ ����� ������. </p>
<p>����� ����� ������ �&nbsp;��� ��&nbsp;������ ����. ��&nbsp;��� ������ ��� ������, ��� ��� ��� ������, ��&nbsp;������ ��� ��� ���� ������. ��&nbsp;��&nbsp;������ ������� ������.</p>
<p>����� ����������� ��������.</p>
<p><strong>����� 5</strong></p>
<p>�&nbsp;����� ��&nbsp;��� ��&nbsp;����� ������������� ���� ������������. ���� ��� �&nbsp;����� ��&nbsp;����� �������� �������, ��&nbsp;�&nbsp;�����&nbsp;&mdash; ������������, ���� ��&nbsp;�������� �������. </p>
<p>�&nbsp;�&nbsp;������ ���, ����������� ����� ��������� ������, �&nbsp;������� ��&nbsp;����������. ��� ���� �����. ��� ���� ��������� ��������, ��� ���� ��� ��������. ���� ������� ������. �&nbsp;��� ����� ���������� ��&nbsp;�������. ��&nbsp;�����������. </p>
<p>����� ���� �������� ������ ��� ������. ����� ��&nbsp;������ ��� ����� �����, ��&nbsp;��� ��&nbsp;����� ����������� ��&nbsp;������ ������ �����. ��&nbsp;��&nbsp;��&nbsp;������ ��� ����, ����� ����� ����������� ������. </p>
<p>�&nbsp;��� �����������... ��� ���� ��&nbsp;������� �&nbsp;��&nbsp;����������, ��� ��� ���-�� ������, ���������������� �&nbsp;���, ����� ���������� ���� ������������. ��&nbsp;���� ���� ���� ��&nbsp;���������� �������?</p>
<p><strong>����� 6</strong></p>
<p>������� ������ ���������� �����������. ������ �������&nbsp;&mdash; ������. <br />
��&nbsp;������� �������� ����� ���������� ��������� ����.</p>
<p>����� ���������� ���������� ��������, ��&nbsp;������� �������� �����: &mdash;&nbsp;����� �&nbsp;�������. �&nbsp;�����, �������. ����� ���������� ����, ��� ������. <br />
<br />
��&nbsp;������� &laquo;����� ����&raquo;, ��&nbsp;����� ������, �&nbsp;�����, ��� ��&nbsp;�������. <br />
<em>���� ��&nbsp;���� ������&nbsp;&mdash; ������. ��� ������ ������ �&nbsp;�������. ��� ������ ���� ����� ������� ��&nbsp;��������� ������ ������.. <br />
�����&nbsp;&mdash; ��������� ���� ������.</em> <br />
<em>�&nbsp;������� ������ ������, ������ ��� ���&nbsp;�� ����, �&nbsp;���� ���� ��� ����� ��������� ������, ������ ��� ���� ������ �������� ��&nbsp;���� ����.</em> <br />
<em>���� ����� ����, ������ �������������� ������� ��&nbsp;������� ������� ���. ��� ������. ��&nbsp;������ ��� �������� �����, ����� ��� ����� ���������� ���� ������. </em> <br />
<em><br />
��&nbsp;����� ��&nbsp;�������, ��� ����� �������, ������ ������������� �&nbsp;�������, ������� �������� ����� �����. </em> <br />
<em><br />
</em><em>����� ������� ����� ��&nbsp;��������, ��&nbsp;��&nbsp;������� ������ ���������� ����� �&nbsp;�������� �������. ��� ����&nbsp;&mdash; ����� ����.</em> <br />
<em>����� ����� ���� ������� �����. ���� ������ ����� ���������.</em> <br />
<em>��� ��������� ����� �������� ����� ������.</em>&quot; </p>
����� ���� ���� ��&nbsp;�������� ��&nbsp;������ ��&nbsp;&laquo;���������&raquo;.
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
