<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ������� (������ �����)",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ������� (������ �����)</div>
<p><strong>����� 1</strong>
<p>��&nbsp;����� �&nbsp;����. �&nbsp;����� ���� ���������, �&nbsp;������ ����� ��&nbsp;���������. ����� ��&nbsp;����� ����� �������, �&nbsp;�&nbsp;����� ����� ������� �������� ��&nbsp;���� ��������. ��� ����� ��������, ������� �������� ���� ��������. ������� ��&nbsp;�����, ��&nbsp;������, ���� ��&nbsp;����� �&nbsp;��&nbsp;����������� �����. ��&nbsp;��� ������ �&nbsp;���, ��� ��� ����� ������� ��&nbsp;����. ��� ��&nbsp;��&nbsp;������ ��������. �&nbsp;��� ���, ����� �������������� ����, ��� ��� ��� ���� ��&nbsp;����� ����.<br />
<br />
������ ����� ���� ��������� �������, ������ ��������� �����. ��� �&nbsp;�������, ����� ��������� ���-�� ������������, ��&nbsp;������ ������� ���. ��&nbsp;��&nbsp;��� ��&nbsp;�������. ���� 28&nbsp;���, �&nbsp;��&nbsp;����� ������ ���� �&nbsp;������� ������ �����-����������. �&nbsp;������ ������� ���, ��&nbsp;������. <br />
<br />
��&nbsp;��� ��� ���� ������� �&nbsp;�������� �&nbsp;�����. ������, ��&nbsp;����� �������. ����, ��� ������, ��� �&nbsp;��� ����, ������ ������� ��� ��&nbsp;����. ������ ������, ��� ����� �&nbsp;����� ������&nbsp;&mdash; �������� ��&nbsp;������. </p>
<p>��&nbsp;��������, ��� �&nbsp;15&nbsp;��� ��&nbsp;��� ������� ��&nbsp;����� �������. ������� ��&nbsp;������� �&nbsp;���� ��&nbsp;�������� �������� ������� �&nbsp;����� �������. ������ ����� ���� �&nbsp;��������� �������, �&nbsp;��� �������� �&nbsp;�����. ������ ���� ������� ��&nbsp;�������� ������, ������� �&nbsp;����� ��&nbsp;�������. ��� ���� ������ �����������, ��&nbsp;����� ��������� ��&nbsp;��� ���� ������� �&nbsp;�������� �������� �����. �&nbsp;���� ��&nbsp;����� ��������� �����. ��&nbsp;������-�� ��&nbsp;����� ������ ����... ����? ��� �&nbsp;���� ����� ���������� ������� �&nbsp;��������� �����. <br />
<br />
�&nbsp;���� �&nbsp;���� �������� �������. ��, ��� ������������, �������� �������� �&nbsp;���� �����. ��� ��� ������. ��&nbsp;���� ������� ��&nbsp;������� ��������� ������, ����� ���� ������.<br />
&mdash;&nbsp;����.<br />
&mdash;&nbsp;������ ����. �&nbsp;�&nbsp;������, ����� ��������, ������ �&nbsp;������. &mdash;&nbsp;�������� ��������� ����� �������.<br />
&mdash;&nbsp;��� ����� ����, �&nbsp;��&nbsp;��������� ���, ��&nbsp;������. �����, ����� ����������. ��&nbsp;�������?<br />
&mdash;&nbsp;����� ������? &mdash;&nbsp;������ ��������.<br />
&mdash;&nbsp;��&nbsp;��� ������������� �����-�� ������� ����� ��&nbsp;���������, ��������. &mdash;&nbsp;��&nbsp;��������, ����� ���� ����� ������ �������������.<br />
&mdash;&nbsp;�&nbsp;��&nbsp;���, �������? ... ������, �&nbsp;�&nbsp;����� �����.<br />
&mdash;&nbsp;��� ��&nbsp;11&nbsp;���, ���� ����� ������ ��&nbsp;�������.<br />
&mdash;&nbsp;��&nbsp;�����. �&nbsp;��� �&nbsp;�����. ������ �&nbsp;����������.<br />
<br />
��&nbsp;������� ������� ��&nbsp;����. ����� ���������. ��&nbsp;������ �������� �&nbsp;����������� ��������� ��&nbsp;����. ��� ��&nbsp;��&nbsp;����� ����?</p>
<p><strong>����� 2</strong></p>
<p> &mdash;&nbsp;������ ����� ��������, ������ ��&nbsp;�����. ���� ������� ����. ������ ��&nbsp;������� ��&nbsp;����. &mdash;&nbsp;������� ����.<br />
&laquo;����� �������&raquo;&nbsp;&mdash; �������� ��&nbsp;��� ����. �&nbsp;��������� ����� ��&nbsp;�&nbsp;���������� ��������� ����������, ����� ������ ���� �&nbsp;����. �&nbsp;�&nbsp;������ ������ ��&nbsp;�������, ��� ��� �&nbsp;��������� ���. ��&nbsp;�&nbsp;������� ����� ����� ��������� ��&nbsp;������, �&nbsp;������� ������� �&nbsp;����������� ����, ������� ������ ������� ��&nbsp;�������. ����� ���� ������ ������ �������� ��������. ��&nbsp;��� ����� ����. ��������� ����, ������� ���������� ������, ����� ������, ��������� ��&nbsp;��� �������. ��&nbsp;����� �&nbsp;��������� �������&nbsp;&mdash; ������ �����, ����.<br />
&mdash;&nbsp;�&nbsp;����� ��&nbsp;������ �������? &mdash;&nbsp;������ ��.<br />
&mdash;&nbsp;�&nbsp;���� ����� ������� �&nbsp;���� �&nbsp;����� �����? &mdash;&nbsp;��������� ����, ���������� ����� ����. ��� ���� ����� �������� �&nbsp;��������� ����������� ����.<br />
&mdash;&nbsp;��� ���, �&nbsp;����?<br />
&mdash;&nbsp;�����, ��� ������ ��������? &mdash;&nbsp;���������� ������ �&nbsp;���� ��&nbsp;��������. ��&nbsp;��� �&nbsp;������ ������� ��&nbsp;����������. ������ ����� ����, �&nbsp;��&nbsp;��� ������, � <br />
������� ����� ��&nbsp;����������. ��&nbsp;���������� �&nbsp;�����, ����� ���������, �����&nbsp;�� ��. ��&nbsp;��� ����� ������ �����, ����� ������� ���� ��&nbsp;���� �&nbsp;�������:<br />
&mdash;&nbsp;��� ����� ��&nbsp;�����? ������� ��&nbsp;������ ����� ��&nbsp;����?!<br />
&mdash;&nbsp;��&nbsp;����, ���, �����. &mdash;&nbsp;���������� ��.<br />
���������� ���-�� ���������� ���������.<br />
&mdash;&nbsp;��� ��&nbsp;��� �������, ��&nbsp;��� �������... &mdash;&nbsp;��������&nbsp;��. ����� ��������� ���� �&nbsp;�����:<br />
&mdash;&nbsp;��&nbsp;������ ��&nbsp;��������, �������? ������ ��&nbsp;��������, ���&nbsp;�� ��&nbsp;���������, ������ �����������,&nbsp;&mdash; ������ ���� �&nbsp;�������-�� �������� ���� ����.</p>
<p><strong>����� 3</strong></p>
<p>����� ��&nbsp;��� ���������, ��&nbsp;���� ��&nbsp;�&nbsp;������� ���-�� ����� ��&nbsp;������� ������. ��� ��&nbsp;������� �������. �&nbsp;���� ���������� ����������� �����, ��� �&nbsp;����, �&nbsp;��&nbsp;����� ������������� ��&nbsp;����� ������. ��-�� ��������� ������� �&nbsp;��������, ���� ��������� ���� ����������� ��������������&nbsp;&mdash; ��� �������, �����, ������. </p>
<p>�&nbsp;��������� ��� ������� ��&nbsp;������, ��&nbsp;���� �&nbsp;������ �����. ���� ����� ������������ ����� ������, ������� ��������� ��&nbsp;����� �&nbsp;������� ���� �&nbsp;����: &laquo;����� ������, ���&nbsp;��, ��&nbsp;������ ������� ���� ��&nbsp;�������� �����!&raquo;. ��&nbsp;��������� �&nbsp;�������: &laquo;���� ��������, ��������, ��� ������ �&nbsp;������!&raquo;. &laquo;��&nbsp;���� ��&nbsp;������&raquo;&nbsp;&mdash; ��� �������� ����. </p>
<p>��&nbsp;����� ��&nbsp;��������, ��� ����� ������������, ��� ��&nbsp;������ ���������� �����. &laquo;����, ��� ���� �����. ������. �&nbsp;��&nbsp;���� �������!&raquo; ����� ������ ����������� ������ �&nbsp;��������� ����������. ��&nbsp;���� ��� ����. ����� �������. �&nbsp;������� �����, ��� ���� �������� �����, �&nbsp;������ �������. &laquo;��&nbsp;��&nbsp;���!&raquo;&nbsp;&mdash; ��&nbsp;������ ��������� ���� �&nbsp;��������. &laquo;����, ������!&raquo;&nbsp;&mdash; �&nbsp;����� ���� �������� ��&nbsp;���. </p>
<p>��&nbsp;��&nbsp;������ �����, ������� ��� �������� �&nbsp;���������. ��� ��&nbsp;����������. ����� ���� ���������� ���-�� ������� ����. &laquo;��&nbsp;������ ����� ������, �����? ����� ��&nbsp;�������?&raquo; ���������� ������ ��� 25&nbsp;����� ���� ��&nbsp;�����. ��&nbsp;���� ������ �������� ����, ��&nbsp;��&nbsp;����� ��&nbsp;����. ���� ������-�� ��������� ���� �����.</p>
<p><strong>����� 4</strong></p>
<p> &mdash;&nbsp;��� ���� �����? &mdash;&nbsp;������� ������, ��&nbsp;������ �������� ������ ����� �����. <br />
&mdash;&nbsp;������...<br />
&mdash;&nbsp;��&nbsp;���, ���� ��� ������?<br />
&mdash;&nbsp;���, �&nbsp;�������. <br />
&mdash;&nbsp;���. �����, ����� ������� ���� ����� ��� ������. ������ ��� ����� ����� ������ ���. <br />
�����, ������� ������� ���� ��&nbsp;�����, ����� ������ �����. ��� ������, ��� ��&nbsp;���������� ��&nbsp;����� ����, ���� ��&nbsp;�������� �������� �������, ����� ���-�� ��&nbsp;���. ��� ������ ���� �����-�� ��&nbsp;�����. ������ �������� �&nbsp;�����. ����, ��� ����. <br />
&mdash;&nbsp;���, ���,&nbsp;&mdash; ���������� ��-�� ����� ���������� �����. ��&nbsp;������������ ��������� ��&nbsp;�������. ��� ������ ��&nbsp;���� �&nbsp;���������. <br />
&mdash;&nbsp;��� ���� ��������, �����? &mdash;&nbsp;�������&nbsp;��. <br />
�&nbsp;���� ������ ����� ���������, �&nbsp;��&nbsp;������ ������� �������, ���������� ���� ��&nbsp;��������. <br />
&mdash;&nbsp;������, ���� ���? &mdash;&nbsp;������� ������. <br />
&mdash;&nbsp;����? ���&nbsp;��, ���&nbsp;��... �����, ���������&nbsp;�. �&nbsp;����� ���, ��&nbsp;�������&nbsp;&mdash; ��� ���,&nbsp;&mdash; ��� ������� ������ �&nbsp;���������� ���� ��� ����,&nbsp;&mdash; ��&nbsp;��&nbsp;���� ������� �����. ��&nbsp;������ �������� ������������� ��� ���������. <br />
&mdash;&nbsp;��&nbsp;���� ��� �������? &mdash;&nbsp;������� ������ ��&nbsp;���� ��� �&nbsp;����. <br />
��&nbsp;������. <br />
&mdash;&nbsp;�&nbsp;��&nbsp;60&nbsp;��� ����� ���� ����,&nbsp;&mdash; ������������ ��������. <br />
������ �������� ����� ������ ������ �&nbsp;���������� ������� �&nbsp;����� �&nbsp;�����. �&nbsp;���� ���� ������ ������������. ����� ��&nbsp;�������� �&nbsp;���� �����, ����� ������� ���������� ����, ������� ������ ��&nbsp;������� ��� ����� ���������. <br />
&mdash;&nbsp;���-�� ���������&nbsp;��, ����,&nbsp;&mdash; ������ ������. &mdash;&nbsp;�����, �&nbsp;����� ���� ��&nbsp;������?<br />
&mdash;&nbsp;�����... &mdash;&nbsp;���������� ���������&nbsp;��, ��&nbsp;����, ��� �&nbsp;������. <br />
������ ������������� �&nbsp;���� ��&nbsp;����, �&nbsp;��&nbsp;��� �&nbsp;������ ��������. ������&nbsp;�� ��������� ���� ����� �&nbsp;��� ��&nbsp;�����, ������ ������ ����������. ��&nbsp;��� ��� ��������� ���� ��&nbsp;���������. ��&nbsp;����� ��&nbsp;����� �&nbsp;������, �&nbsp;����&nbsp;��, ������ ��������� ���� ������� ������, ��� ��� ��&nbsp;����������. ������ ����� ��������� ���� ����� ���������. &laquo;��&nbsp;���, ������ ��&nbsp;�&nbsp;��� ������ ������� �����, ��� ��&nbsp;������ ��&nbsp;���������&raquo;,&nbsp;&mdash; ������ ������, ����� ��&nbsp;��������� ������. �&nbsp;��&nbsp;����� ��������� ���������. ���� ������ ��������� ��� ���.</p>
<p><strong>����� 5</strong></p>
<p>��� ���� ������ �������, ����� ���� ���� ��� ��� ������ �����. ��� ������ ��&nbsp;�&nbsp;������ ���������� �������� �&nbsp;�������� ������: ������ ����, ���������� &laquo;��&nbsp;�����&raquo;, ������� �������� &laquo;������������&raquo;. ����� ��&nbsp;��� ���������� �������, ��&nbsp;�������� �������� ������ �&nbsp;������ ������� �&nbsp;�������: &laquo;���� ����� ����!&raquo;. ��&nbsp;������� ���� ���&nbsp;�, ��&nbsp;��������� �������� �������, ������� ����� ��&nbsp;�������� ��&nbsp;�����. �������� ������������� �����, ����� ������� ���, ������� �������, �������� ����� ��&nbsp;���. �&nbsp;����� �&nbsp;������ ��������� ����, �������, ���-�� ������� �������� &laquo;�������&raquo;. �&nbsp;��&nbsp;����� ���������� ��&nbsp;����� ������ ����������� ����. ��&nbsp;����� �&nbsp;������ ����� ����� ����������������� �������, ��� �������� ��&nbsp;�����. ��&nbsp;���� �&nbsp;�������� ������� �������, ����� ���� ���-�� ���� �&nbsp;�����. ����� �&nbsp;�������� ��&nbsp;����&nbsp;&mdash; �&nbsp;������ ��&nbsp;������, ��� ��� ������� ����� �&nbsp;����������� ��������� ������, ������ ��������� ����� �&nbsp;��&nbsp;����� �����... �&nbsp;��&nbsp;������ ��������. �&nbsp;��������� ��� ���� �������, ������� ����������, ���� ���������� ��� ������� �������, ��&nbsp;������ ��&nbsp;������, ����� ��� ���� ����. �&nbsp;��� �&nbsp;�����, ��� ��&nbsp;���� ��&nbsp;�������� ������ �&nbsp;������, ��&nbsp;������� ��������, �&nbsp;�&nbsp;�������� ����� �����. ������ �&nbsp;������� ����. ����� �������� ���� ����� �����, �&nbsp;���������� �&nbsp;������� ��&nbsp;����� �&nbsp;�����. ����� ���� ����������, ������ ��&nbsp;������. ������ ���� �����, ���� ��&nbsp;������� ��������� ����������� ������. ��������� �&nbsp;���, �&nbsp;��� �������� ����, ����� ������������ ������ ��������� �����, ��&nbsp;&mdash; ��� �������� ���������� �����, ���&nbsp;40, ������ ������ �&nbsp;�������� ������� �&nbsp;������ �������, ������� �������� ��&nbsp;���� ����������, ����� ����������� ������ ����� (����� ����� ������ ���������� ��&nbsp;����� �������). &laquo;������ ���? ��� ���� ��&nbsp;�����?&raquo;&nbsp;&mdash; ������� ��&nbsp;����. &laquo;��, ��� ��� ����, ��� ��&nbsp;��� �������?&raquo;&nbsp;&mdash; ���������&nbsp;��, ��&nbsp;��������� ������. &laquo;��� ���� ��� ���, ��&nbsp;��������!&raquo;&nbsp;&mdash; ���������� ��&nbsp;�&nbsp;������ �����, �������� ��� ��&nbsp;�����. �&nbsp;��������� �&nbsp;������ ������ �����, ������������ ��&nbsp;��������: ��� ���� ���������� ����, ������, ������ �&nbsp;������ ������ �&nbsp;��� ����; ��� ��������, ������ ��������� �����, ��������� ���� ����� ������ ���� ������. �&nbsp;������ ��� ��&nbsp;��&nbsp;�������, ����� �������� �&nbsp;����������, ��������� �&nbsp;����, ��&nbsp;��&nbsp;�����, ����, ��&nbsp;������� ��&nbsp;������ ��� �����&nbsp;&mdash; ��������. ������ ��&nbsp;������� �&nbsp;��������� ��������� ������������ ������ �����, ����� ��&nbsp;��� ������� ��� ����: &laquo;��&nbsp;��� ���������? ����� ����� ���������! �� ���� ��-��������: ��&nbsp;����� �&nbsp;��� �����!&raquo;&nbsp;&mdash; ����������&nbsp;��. �����������, ��� ������ ������, �&nbsp;����� ������ ��&nbsp;���� ����������������, ���� ����, �&nbsp;��� ��&nbsp;������ �&nbsp;������� �����. ���� ������� ������ �&nbsp;������ �������� �&nbsp;��� ��������� �����, �&nbsp;������� ��� ��� �&nbsp;��&nbsp;�������� ������� �������. ��&nbsp;����� �&nbsp;�&nbsp;��� ������, �&nbsp;������, ������� �������� &laquo;�����&raquo; �&nbsp;�&nbsp;��� �����, ��� �&nbsp;��&nbsp;��� ��&nbsp;���������. �&nbsp;����� ������ �&nbsp;�� ������ ��&nbsp;���������� ������������ ���� �&nbsp;���������. ��&nbsp;�&nbsp;��� ��� ������ ������, ���&nbsp;40, ������� �&nbsp;�����, ���������� ���� ����������� �&nbsp;����������� ������� ���������.</p>
<h3>����������� ������������ (��&nbsp;��������)</h3>
<p><b><i>����� ��&nbsp;������������, ��� ������ �������������� ����, ������ ���� ����:</i></b></p>
<p>��&nbsp;��������, ��� �����, �&nbsp;�������, ������ ��&nbsp;�� ����� �������, ��� ����� �����. ��, �������, ��������, ��� ����� �������.</p>
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
