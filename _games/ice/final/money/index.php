<?
define("ROOT","../../../../");
require_once(ROOT . 'funcs.php');
show_header("˸� :: ����� ���� :: ���������� �����",ICE);
show_menu("inc/main.menu"); ?><td class="box">
<div class="boxheader"><a href="http://ice.bastilia.ru/">˸�</a> :: <a href="http://ice.bastilia.ru/final/">����� ����</a> :: ���������� ����� </div>

<p>��&nbsp;������� �&nbsp;������� <strong>50700</strong>&nbsp;������, ��������� <strong>54200</strong>&nbsp;������. ������� �������&nbsp;&mdash; <strong>3500</strong>&nbsp;������.</p>
<p>������ (50700&nbsp;�.) �������������� ��&nbsp;��������� �������:</p>
<ul>
<li><strong>1200</strong> ������&nbsp;&mdash; <strong>41</strong>&nbsp;����� (1&nbsp;��&nbsp;��� ��&nbsp;������);</li>
<li><strong>1500</strong> ������&nbsp;&mdash; <strong>1</strong>&nbsp;�����.</li>
</ul>
<p>������� ������� (3500&nbsp;�.) ��&nbsp;��� ������������. ������ ����� �&nbsp;���� ������ ���������� ������� �&nbsp;��������� ��-�� ����� ������ ���������� ��&nbsp;��� ����-�������: ���������� ����� ��&nbsp;������ �������� �&nbsp;���� ���������� ��� ��������� ������.</p>
<p>���� ����������� ��������� ����� �&nbsp;��������� ��������.</p>
<table width="95%" border="1" align="center" cellpadding="1" cellspacing="1">
<tr>
<td><div align="left"><strong>������ ��������</strong></div></td>
<td align="right"><div align="center"><strong>���-��</strong></div></td>
<td align="right"><div align="center"><strong>����</strong></div></td>
<td align="right"><div align="center"><strong>�����</strong></div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>������� (6663,03&nbsp;�.)</strong></div> <div align="center"></div><div align="center"></div><div align="center"></div></td>
</tr>
<tr>
<td><div align="left">������ ������ (��� �����), ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">36,8</div></td>
<td align="right"><div align="center">368</div></td>
</tr>
<tr>
<td><div align="left">������ ��� ������ ������������ (���������� ����� �����), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">142</div></td>
<td align="right"><div align="center">142</div></td>
</tr>
<tr>
<td><div align="left">��������� ����� 12V (��� �������), ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">48</div></td>
<td align="right"><div align="center">288</div></td>
</tr>
<tr>
<td><div align="left">���������� (������ ���������), ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">7,5</div></td>
<td align="right"><div align="center">45</div></td>
</tr>
<tr>
<td><div align="left">������ ���� 2&times;0,5 (��� �������), �</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">7,2</div></td>
<td align="right"><div align="center">288</div></td>
</tr>
<tr>
<td><div align="left">��������, ������� ������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">500</div></td>
<td align="right"><div align="center">500</div></td>
</tr>
<tr>
<td><div align="left">�������� (��� �����), ��</div></td>
<td align="right"><div align="center">50</div></td>
<td align="right"><div align="center">13</div></td>
<td align="right"><div align="center">650</div></td>
</tr>
<tr>
<td><div align="left">������� (��� �����), ��. ��&nbsp;8&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">70</div></td>
<td align="right"><div align="center">140</div></td>
</tr>
<tr>
<td><div align="left">���� ���������� ������� (��� ������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">138</div></td>
<td align="right"><div align="center">138</div></td>
</tr>
<tr>
<td><div align="left">����� ���������� (������� ��������), ��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">103</div></td>
<td align="right"><div align="center">515</div></td>
</tr>
<tr>
<td><div align="left">����� (����������� ��� ������, ����� �&nbsp;�������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">100</div></td>
</tr>
<tr>
<td><div align="left">���������� (������), ��.</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">11,2</div></td>
<td align="right"><div align="center">224</div></td>
</tr>
<tr>
<td><div align="left">��� (��� ��������), �����</div></td>
<td align="right"><div align="center">0,5</div></td>
<td align="right"><div align="center">73</div></td>
<td align="right"><div align="center">36,5</div></td>
</tr>
<tr>
<td><div align="left">������� (��� ����������� ����), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">24,9</div></td>
<td align="right"><div align="center">24,9</div></td>
</tr>
<tr>
<td><div align="left">����������� ��� �&nbsp;��������� (��� ��������), ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">30</div></td>
</tr>
<tr>
<td><div align="left">��� &laquo;������&raquo; (��� ��������), �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">51</div></td>
<td align="right"><div align="center">51</div></td>
</tr>
<tr>
<td><div align="left">������� ������� (��� ��������), �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">35</div></td>
<td align="right"><div align="center">35</div></td>
</tr>
<tr>
<td><div align="left">������ (��� ��������), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">27</div></td>
<td align="right"><div align="center">27</div></td>
</tr>
<tr>
<td><div align="left">���� (��� ��������), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">33</div></td>
<td align="right"><div align="center">33</div></td>
</tr>
<tr>
<td><div align="left">������� (��� ��������), ��.</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">30</div></td>
</tr>
<tr>
<td><div align="left">������� (��� ��������), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">23</div></td>
<td align="right"><div align="center">23</div></td>
</tr>
<tr>
<td><div align="left">�������� (��� ��������), ��.</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">7,5</div></td>
<td align="right"><div align="center">15</div></td>
</tr>
<tr>
<td><div align="left">����� ������������ (��� ��������), ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">18,59</div></td>
<td align="right"><div align="center">37,18</div></td>
</tr>
<tr>
<td><div align="left">����� &laquo;���-���&raquo; (��� ��������), ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">18,99</div></td>
<td align="right"><div align="center">56,97</div></td>
</tr>
<tr>
<td><div align="left">����������� 12&nbsp;������� (��� ���������), ��</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">53</div></td>
<td align="right"><div align="center">636</div></td>
</tr>
<tr>
<td><div align="left">����������� ������������� (��� ���������), ��</div></td>
<td align="right"><div align="center">7</div></td>
<td align="right"><div align="center">32,2</div></td>
<td align="right"><div align="center">225,4</div></td>
</tr>
<tr>
<td><div align="left">����������� &laquo;�����&raquo; (��� ���������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">113,5</div></td>
<td align="right"><div align="center">113,5</div></td>
</tr>
<tr>
<td><div align="left">����������� &laquo;������&raquo; (��� ���������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">128,8</div></td>
<td align="right"><div align="center">128,8</div></td>
</tr>
<tr>
<td><div align="left">����������� &laquo;������&raquo; (��� ���������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">128,8</div></td>
<td align="right"><div align="center">128,8</div></td>
</tr>
<tr>
<td><div align="left">������ ���������� ����� ������������ (���������), 4&nbsp;������</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">37,62</div></td>
<td align="right"><div align="center">150,48</div></td>
</tr>
<tr>
<td><div align="left">���������� ��� ��������� ��������, 2,4V. �������, ������, ���������, ��</div></td>
<td align="right"><div align="center">21</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">315</div></td>
</tr>
<tr>
<td><div align="left">���������� ��� ��������� ��������, 2,4V. �����, ��</div></td>
<td align="right"><div align="center">7</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">140</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� �������� CR2032 (��� �����������), 5&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">60</div></td>
<td align="right"><div align="center">120</div></td>
</tr>
<tr>
<td><div align="left">������������ �������� (����� ����������� �����)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">268,5</div></td>
<td align="right"><div align="center">268,5</div></td>
</tr>
<tr>
<td><div align="left">������ (��� ����������� ���������� �������), ��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">75</div></td>
</tr>
<tr>
<td><div align="left">��������� �&nbsp;�������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">145</div></td>
<td align="right"><div align="center">145</div></td>
</tr>
<tr>
<td><div align="left">�����, ���.</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">45</div></td>
<td align="right"><div align="center">90</div></td>
</tr>
<tr>
<td><div align="left">���������, �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">89</div></td>
<td align="right"><div align="center">89</div></td>
</tr>
<tr>
<td><div align="left">������� ������ (��� �������� ����������� �����), ��.</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">240</div></td>
</tr>
<tr>
<td colspan="4"><strong>����������� �&nbsp;������������ (4617,1&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">���� �������� 1020&times;620&times;220, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">570</div></td>
<td align="right"><div align="center">570</div></td>
</tr>
<tr>
  <td><div align="left">���� ���������� �&nbsp;������� (��� ��������� ������ ��&nbsp;�����), �����.</div></td>
  <td align="right"><div align="center">1</div></td>
  <td align="right"><div align="center">79</div></td>
  <td align="right"><div align="center">79</div></td>
</tr>
<tr>
<td><div align="left">�����-��������, ����� ���������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1068</div></td>
<td align="right"><div align="center">1068</div></td>
</tr>
<tr>
<td><div align="left">������ ��� ������ �������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">65,55</div></td>
<td align="right"><div align="center">131,1</div></td>
</tr>
<tr>
<td><div align="left">������ ��������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">284</div></td>
<td align="right"><div align="center">568</div></td>
</tr>
<tr>
<td><div align="left">������ �����������, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">289</div></td>
<td align="right"><div align="center">867</div></td>
</tr>
<tr>
<td><div align="left">���� ��� ���������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">460</div></td>
<td align="right"><div align="center">920</div></td>
</tr>
<tr>
<td><div align="left">����� �������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">414</div></td>
<td align="right"><div align="center">414</div></td>
</tr>
<tr>
<td colspan="4"><strong>������ (16438,9&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">������ ��-�����, ��</div></td>
<td align="right"><div align="center">25</div></td>
<td align="right"><div align="center">500</div></td>
<td align="right"><div align="center">12500</div></td>
</tr>
<tr>
<td><div align="left">������ ���/�����, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">50</div></td>
<td align="right"><div align="center">200</div></td>
</tr>
<tr>
<td><div align="left">������ �������/���������, ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">50</div></td>
<td align="right"><div align="center">300</div></td>
</tr>
<tr>
<td><div align="left">������ ������ ������� (�&nbsp;�.�. ������ �&nbsp;�����-����), ��</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">3000</div></td>
</tr>
<tr>
<td><div align="left">����������� �����, 1&nbsp;� (����� ��� �������), ��</div></td>
<td align="right"><div align="center">7</div></td>
<td align="right"><div align="center">62,7</div></td>
<td align="right"><div align="center">438,9</div></td>
</tr>
<tr>
<td colspan="4"><strong>���������� �&nbsp;������� ��������� (1809&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">�������&nbsp;�3 (������� ��&nbsp;��������), �� </div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">300</div></td>
</tr>
<tr>
  <td><div align="left">��������� ��� ������������� 111&times;154&nbsp;��, ��.</div></td>
  <td align="right"><div align="center">1</div></td>
  <td align="right"><div align="center">393</div></td>
  <td align="right"><div align="center">393</div></td>
</tr>
<tr>
<td><div align="left">�������� ���������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">119</div></td>
<td align="right"><div align="center">119</div></td>
</tr>
<tr>
<td><div align="left">�������� �����, �������, ������, ��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">12,4</div></td>
<td align="right"><div align="center">62</div></td>
</tr>
<tr>
<td><div align="left">�������� ������, �������, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">16</div></td>
<td align="right"><div align="center">64</div></td>
</tr>
<tr>
<td><div align="left">������, ��</div></td>
<td align="right"><div align="center">60</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">660</div></td>
</tr>
<tr>
<td><div align="left">���������� (������� ������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">211</div></td>
<td align="right"><div align="center">211</div></td>
</tr>
<tr>
<td colspan="4"><strong>�������������� �&nbsp;��������� (4683,17&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">������� (��� 3,2&times;1220&times;2750), ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">180,5</div></td>
<td align="right"><div align="center">722</div></td>
</tr>
<tr>
<td><div align="left">�������� �����������, ���</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">10,5</div></td>
<td align="right"><div align="center">31,5</div></td>
</tr>
<tr>
<td><div align="left">��� ������������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">9,5</div></td>
<td align="right"><div align="center">9,5</div></td>
</tr>
<tr>
<td><div align="left">������, �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">40</div></td>
</tr>
<tr>
<td><div align="left">������ �������, ���.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">28,5</div></td>
<td align="right"><div align="center">28,5</div></td>
</tr>
<tr>
<td><div align="left">������ �����������, ����� 1,5&times;10&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">110</div></td>
<td align="right"><div align="center">220</div></td>
</tr>
<tr>
<td><div align="left">��������� Duracell&nbsp;AA, ��</div></td>
<td align="right"><div align="center">96</div></td>
<td align="right"><div align="center">16,37</div></td>
<td align="right"><div align="center">1571,52</div></td>
</tr>
<tr>
<td><div align="left">������ (�������� ��� ��� �����), 3&times;1,5&nbsp;� </div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">677</div></td>
<td align="right"><div align="center">677</div></td>
</tr>
<tr>
<td><div align="left">������&nbsp;1,5&nbsp;�, ��</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">15,2</div></td>
<td align="right"><div align="center">304</div></td>
</tr>
<tr>
<td><div align="left">����������� �������� (5&nbsp;���)</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">46,55</div></td>
<td align="right"><div align="center">93,1</div></td>
</tr>
<tr>
<td><div align="left">����� �������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">58,9</div></td>
<td align="right"><div align="center">58,9</div></td>
</tr>
<tr>
<td><div align="left">����� ��� ������ 120&nbsp;�, 10&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">109,3</div></td>
<td align="right"><div align="center">109,25</div></td>
</tr>
<tr>
<td><div align="left">����� ��� ������ 60&nbsp;�, ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">39,9</div></td>
<td align="right"><div align="center">39,9</div></td>
</tr>
<tr>
<td><div align="left">������� ��&nbsp;���������� 60&times;40&times;30, ��</div></td>
<td align="right"><div align="center">14</div></td>
<td align="right"><div align="center">38</div></td>
<td align="right"><div align="center">532</div></td>
</tr>
<tr>
<td><div align="left">�������� ��� ���������, ���.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">236</div></td>
<td align="right"><div align="center">236</div></td>
</tr>
<tr>
<td><div align="left">������ ������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">10</div></td>
</tr>
<tr>
<td colspan="4"><strong>����������� �������� (7858,15&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">�������� ��� �������, ���</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">33,25</div></td>
<td align="right"><div align="center">66,5</div></td>
</tr>
<tr>
<td><div align="left">����� �����������������, �</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">160</div></td>
<td align="right"><div align="center">800</div></td>
</tr>
<tr>
<td><div align="left">����� ����������, �</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">160</div></td>
<td align="right"><div align="center">800</div></td>
</tr>
<tr>
<td><div align="left">����� ������� <nobr>2-�����������</nobr> &laquo;������&raquo;, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1300</div></td>
<td align="right"><div align="center">1300</div></td>
</tr>
<tr>
<td><div align="left">����� ��� �����, �</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">37</div></td>
<td align="right"><div align="center">74</div></td>
</tr>
<tr>
<td><div align="left">������ ������� ����������, 5�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">600</div></td>
<td align="right"><div align="center">600</div></td>
</tr>
<tr>
<td><div align="left">�������� ���� <nobr>1-1,2,</nobr> ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">125</div></td>
<td align="right"><div align="center">125</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� �������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">60</div></td>
<td align="right"><div align="center">60</div></td>
</tr>
<tr>
<td><div align="left">���� ����������� �����������, 1,5&nbsp;�</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">27</div></td>
<td align="right"><div align="center">135</div></td>
</tr>
<tr>
<td><div align="left">�����, ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">9,49</div></td>
<td align="right"><div align="center">56,94</div></td>
</tr>
<tr>
<td><div align="left">�����, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">15,19</div></td>
<td align="right"><div align="center">45,57</div></td>
</tr>
<tr>
<td><div align="left">�����, 1500&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">125</div></td>
<td align="right"><div align="center">249,98</div></td>
</tr>
<tr>
<td><div align="left">������, 350&nbsp;�</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">19,94</div></td>
<td align="right"><div align="center">79,76</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� �������, ��</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">72</div></td>
<td align="right"><div align="center">432</div></td>
</tr>
<tr>
<td><div align="left">�������� ������, ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">44,99</div></td>
<td align="right"><div align="center">449,9</div></td>
</tr>
<tr>
<td><div align="left">���� 3�1, 50&nbsp;���</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">140</div></td>
<td align="right"><div align="center">139,99</div></td>
</tr>
<tr>
<td><div align="left">���� �����������, �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">67,29</div></td>
<td align="right"><div align="center">67,29</div></td>
</tr>
<tr>
<td><div align="left">����� �/�, ��</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">5,79</div></td>
<td align="right"><div align="center">173,7</div></td>
</tr>
<tr>
<td><div align="left">�������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">61,99</div></td>
<td align="right"><div align="center">61,99</div></td>
</tr>
<tr>
<td><div align="left">��������, 900&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">22,99</div></td>
<td align="right"><div align="center">45,98</div></td>
</tr>
<tr>
<td><div align="left">������ ��������, �����</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">37,51</div></td>
<td align="right"><div align="center">187,55</div></td>
</tr>
<tr>
<td><div align="left">����������� �����, 100&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">54,99</div></td>
<td align="right"><div align="center">109,98</div></td>
</tr>
<tr>
<td><div align="left">����������� �������, 100&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">46,99</div></td>
<td align="right"><div align="center">93,98</div></td>
</tr>
<tr>
<td><div align="left">����������� �������, 50&nbsp;��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">81,69</div></td>
<td align="right"><div align="center">245,07</div></td>
</tr>
<tr>
<td><div align="left">����������� �����, 25&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">37,39</div></td>
<td align="right"><div align="center">37,39</div></td>
</tr>
<tr>
<td><div align="left">����� ����������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">32,99</div></td>
<td align="right"><div align="center">32,99</div></td>
</tr>
<tr>
<td><div align="left">������� �������, 400&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">25,39</div></td>
<td align="right"><div align="center">50,78</div></td>
</tr>
<tr>
<td><div align="left">������� ������/��������, 250&nbsp;�</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">17,25</div></td>
<td align="right"><div align="center">103,5</div></td>
</tr>
<tr>
<td><div align="left">�������, 400&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">26,39</div></td>
<td align="right"><div align="center">52,78</div></td>
</tr>
<tr>
<td><div align="left">���� ������������ �/�, 450&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">91,39</div></td>
<td align="right"><div align="center">182,78</div></td>
</tr>
<tr>
<td><div align="left">���, 1500&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">51,49</div></td>
<td align="right"><div align="center">102,98</div></td>
</tr>
<tr>
<td><div align="left">�����, 500&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">23,99</div></td>
<td align="right"><div align="center">47,98</div></td>
</tr>
<tr>
<td><div align="left">��� Viola ���������, 400&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">82,59</div></td>
<td align="right"><div align="center">165,18</div></td>
</tr>
<tr>
<td><div align="left">��� �&nbsp;�������, 400&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">120,7</div></td>
<td align="right"><div align="center">362,1</div></td>
</tr>
<tr>
<td><div align="left">����, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">17,39</div></td>
<td align="right"><div align="center">69,56</div></td>
</tr>
<tr>
<td><div align="left">������ �������, �����</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">47,79</div></td>
<td align="right"><div align="center">95,58</div></td>
</tr>
<tr>
<td><div align="left">��� ������� &laquo;�����&raquo;, 25&nbsp;���.</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">34,99</div></td>
<td align="right"><div align="center">69,98</div></td>
</tr>
<tr>
<td><div align="left">��� ������ &laquo;��������� ����&raquo;, 100&nbsp;���.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">84,39</div></td>
<td align="right"><div align="center">84,39</div></td>
</tr>
<tr>
<td colspan="4"><strong>��������� (8000&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">�������� ���������, ���� �&nbsp;�������</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">3500</div></td>
<td align="right"><div align="center">7000</div></td>
</tr>
<tr>
<td><div align="left">���� ���������� (��� ��������� ������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1000</div></td>
<td align="right"><div align="center">1000</div></td>
</tr>
<tr>
<td colspan="4"><strong>������ (4127,5&nbsp;�.)</strong></td>
</tr>
<tr>
<td><div align="left">������� ����� ��������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">200</div></td>
</tr>
<tr>
<td><div align="left">������, �</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">23,95</div></td>
<td align="right"><div align="center">479</div></td>
</tr>
<tr>
<td><div align="left">��������� ������������ ��������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">1661</div></td>
<td align="right"><div align="center">3322</div></td>
</tr>
<tr>
<td><div align="left">�������� (��� �������), ��.</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">12</div></td>
</tr>
<tr>
<td><div align="left">����� �������������� (��� �������), ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">40</div></td>
</tr>
<tr>
<td><div align="left">���� ���������� (��� �������), ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">15,3</div></td>
<td align="right"><div align="center">30,6</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� (��� �������), ���.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">12,9</div></td>
<td align="right"><div align="center">12,9</div></td>
</tr>
<tr>
  <td><div align="left">��� (��� �������), ���.</div></td>
  <td align="right"><div align="center">1</div></td>
  <td align="right"><div align="center">31</div></td>
  <td align="right"><div align="center">31</div></td>
</tr>
<tr>
  <td><div align="right"><strong>�����:</strong></div></td>
  <td align="right">&nbsp;</td>
  <td align="right">&nbsp;</td>
  <td align="right"><div align="center"><strong>54196</strong></div></td>
</tr>
</table>
</td>
<? right_block('ice'); ?>
	     </tr>
<? show_footer(); ?>

