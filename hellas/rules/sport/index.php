<?php
define ('ROOT', '../../../');
require_once ROOT . 'funcs.php';
show_header ('���� ��������� :: ������� � ��������� :: ����� � ������', HELLAS);
show_menu ('inc/main.menu'); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/hellas/">���� ���������</a> :: <a href="http://bastilia.ru/hellas/rules/">������� � ���������</a> :: ����� � ������ </div>
            <p>������ �������� ��������, ��&nbsp;������, ���������� ��� ���������� &laquo;����������� ������&raquo;. &laquo;����&raquo; �&nbsp;�������� �&nbsp;���������������� �������� ����������. ����������� ����� ��������� ��&nbsp;���, ��&nbsp;�������� ���������� �&nbsp;����������� ���� ���������� ����������.</p>
            <p>������� �&nbsp;7 ��� ��� �������� ����������� �������� <em>��������</em>, ��� ���������� ����� ���������� ���������. ��� ��������� �������� ����� �����������, �&nbsp;���� �������� ������� ������ ��&nbsp;������ �����. �&nbsp;�������� ������������� �&nbsp;������� �&nbsp;��&nbsp;��������: ��&nbsp;���� ��� �������� �������� ������ (����������, �������) ��������� �&nbsp;�������� ������������ ����� ����� �����.</p>
            <p>�&nbsp;������, ��� �&nbsp;�&nbsp;����������� ��������� �������, �&nbsp;������������ ��������� �&nbsp;�������� ����������: <strong>���</strong> (��&nbsp;������� �&nbsp;��&nbsp;�������� ���������), <strong>������ �&nbsp;�����</strong>, <strong>������� �����</strong>, <strong>������� �����</strong>, <strong>�������� ���</strong> (��� ����������� ��&nbsp;������� ��� �&nbsp;������� ����������), <strong>������</strong> (����������� ���������� ����������� <nobr>�����-�������</nobr> ������), ���� ������� ��������� <em><strong>����������</strong></em> (���������� ��� ��� ������, �&nbsp;������� ���� ��������� ��, ����� ������ �&nbsp;����������� ����) �������� ��������������� ������� ��� �������� ���������. �� ��� ���������� ��������� &laquo;����������� ���� ������&raquo;. ������ ��� �&nbsp;����������� ���� ���������� ���������� ��������.</p>
            <p>������ ������� ���������� ���� ����� ����������� ��� ����.</p>
            <p>������ ���� ������ ���� ������� �&nbsp;<nobr>�����-����</nobr> ����������� ������� ��� ������������ ��������. �&nbsp;������� �&nbsp;����� ����������� ������ <strong>���������������� �������� ��������� ������� �������� ����</strong> (��&nbsp;������ ���������� ���������� (�&nbsp;��� �����, ��&nbsp;����������� ����) ������� ��&nbsp;����������� ���� �&nbsp;�������� ��������, ��� ����, ����� ��������� ��������� ������� ������� �&nbsp;����������, ������ ��������� ����������). ������ �������� ��� ������ ��� ����������� ��&nbsp;���� ����� ����������, ����� ������� ������������������ ���� ����������� ��������� ������. �������� ����� ���������� �&nbsp;������ ����������� ��� ��� <strong>���������</strong>, ���������� �&nbsp;���� ����� ���������: ������ �&nbsp;����� �&nbsp;����� (�&nbsp;�������������), ������� ����� (����� ��. 5 ��), ������� ������� ��� ����� (�&nbsp;������� �������������), ��� ��&nbsp;�������� ��������� (��. 60&nbsp;�), ������. ����� ����� ����������� �&nbsp;����������, ���������� ���� �������� ������ ��� ������� �&nbsp;��� �����.</p>
            <p>������ ��&nbsp;�������������� �&nbsp;�&nbsp;������ ������������� �&nbsp;������ ������� ����, �������� ����������� �������� ���������&nbsp;�������:<br />
            1.	��� ������ ��������� ��&nbsp;����&nbsp;�����������.<br />
            2.	���������� �������� ��&nbsp;����������� �&nbsp;�������������� ��������. ������, �������� ��������� ����, ������ ������������� ��������� ����������� ��������. <br />
            3.	���������� �������� �&nbsp;���������&nbsp;������������������:<br />
            &mdash;&nbsp;<strong>������ �&nbsp;�����</strong> �&nbsp;����� �&nbsp;����� ������� �&nbsp;����� ��&nbsp;5&nbsp;��.<br />
            &mdash;&nbsp;<strong>������� �����</strong> ������ 2&nbsp;� �&nbsp;�����.<br />
            &mdash;&nbsp;<strong>������</strong> ��&nbsp;60&nbsp;�.<br />
            &mdash;&nbsp;<strong>�������� ���</strong>&nbsp;&mdash; ���������� �&nbsp;������ ���������, ����� ��������� ������ ������, ����� �&nbsp;���, ������ �&nbsp;���� ����� ���������. ��� ��� ��&nbsp;���� ���������� ��������������� ������ �&nbsp;�����. ��� ��������� �&nbsp;�����������. ��&nbsp;��������� �������� ������� ��� ����� ����������� ��&nbsp;���&nbsp;�� ��������, ��&nbsp;��� �������� �&nbsp;��� ����� ���������� ������, ��&nbsp;����� ������ ��&nbsp;�������. ��� ���� ����� ���������, ��&nbsp;������� ������ ������������� ���� ���������&nbsp;���.<br />
            &mdash;&nbsp;<strong>������</strong>&nbsp;&mdash; ��&nbsp;�������� ����������� <nobr>�����-�������</nobr> ������, ��&nbsp;����� ��������� ����� �������� ����� ��� ��������� 10 ������&nbsp;��&nbsp;��������. <br />
            5. ����� ������ ��� ���������� ����������� ����� ����������� ����, ��&nbsp;��&nbsp;���������, ��� ��� ������, ��������, ������������� ����������� �������, ����������� �����������&nbsp;�&nbsp;����.<br />
            6. ������������ ������� �&nbsp;������ ��������� ����&nbsp;����������.<br />
            7. �������� �������������� ���� ������������, �������� ��� ��&nbsp;360&nbsp;� ��� ��� ��&nbsp;����� ��&nbsp;��������� 60&nbsp;� ��� 360&nbsp;�.</p></td>
<?php show_menu_ex("inc/hellas.menu");?>
	     </tr>
<?php show_footer(); ?>

