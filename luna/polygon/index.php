<?
define("ROOT","../../");
require_once("../../funcs.php");
show_header("�������� �� ���� :: �������",LUNA);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/luna/">�������� �� ����</a> :: ������� </div>
            <h4>���������� ���������� </h4>
<p>������� ���� ��������� �&nbsp;40&nbsp;������� ������ ��&nbsp;������� �������� (�&nbsp;��������, �&nbsp;���� �������, �&nbsp;���� ����� ���� ��&nbsp;������ � ���������� �����������). ������ ������� ���������� ����������: </p>
<table border="2" cellpadding="2" cellspacing="2">

<tr>
<td><div align="center"><strong>����������� ������</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>����� ��������</strong></div></td>
</tr>
<tr>
<td><div align="center">4:37</div></td>
<td><div align="center">4:48</div></td>
<td><div align="center">6:30</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">6:30</div></td>
<td><div align="center">6:42</div></td>
<td><div align="center">8:14</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">8:04</div></td>
<td><div align="center">8:16</div></td>
<td><div align="center">10:04</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">8:56</div></td>
<td><div align="center">9:08</div></td>
<td><div align="center">10:54</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">10:21</div></td>
<td><div align="center">10:33</div></td>
<td><div align="center">12:21</div></td>
<td><div align="center">�� ��������</div></td>
</tr>
<tr>
<td><div align="center">12:04</div></td>
<td><div align="center">12:16</div></td>
<td><div align="center">14:03</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">15:37</div></td>
<td><div align="center">15:49</div></td>
<td><div align="center">17:35</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">17:14</div></td>
<td><div align="center">17:26</div></td>
<td><div align="center">19:12</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">18:44</div></td>
<td><div align="center">18:56</div></td>
<td><div align="center">20:42</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">19:35</div></td>
<td><div align="center">19:47</div></td>
<td><div align="center">21:36</div></td>
<td><div align="center">���������</div></td>
</tr>
<tr>
<td><div align="center">22:42</div></td>
<td><div align="center">22:53</div></td>
<td><div align="center">0:35</div></td>
<td><div align="center">���������</div></td>
</tr>
</table>
<p>�������� ��������, ��� ���������� ����� ����. ������� ��������� ���������� ���� ����������&nbsp;&#8212; ���� ��������� ��&nbsp;����������, ��������� �������� ����� 1,5-2&nbsp;����. </p>
<h4>�������</h4>
<p>&laquo;�������� ��&nbsp;����&raquo; ������� ��&nbsp;�������� ����� ������������ �����&nbsp;&#8212; ���&nbsp;��, ��� 5&nbsp;��� ����� ��� ��� <a href="http://bastilia.ru/sigil/">&laquo;�����&raquo;</a>. ���� ������������ ����� ��������� �&nbsp;����� ������ ��&nbsp;��������.</p>
<p><a href="leipyasuo.jpg"><img src="leipyasuo-mini.jpg" width="515" height="692" border="0" /></a></p>
<p>��&nbsp;������ �������� ���� ��&nbsp;�����&nbsp;�� �����, ��&nbsp;�&nbsp;��� ���� ������. ��&nbsp;������� ����������� ����� 472&nbsp;������� &mdash; ��� 1&nbsp;��,&nbsp;�.�. ��&nbsp;��������������� ������� ��&nbsp;�������� &mdash; ���-�� 2,5&nbsp;���������.</p>
<p>&laquo;�������&raquo; (�����) ������� ������� <strong>�&nbsp;����</strong> (�.�. ������� ���-�� �&nbsp;�����<strong> 2</strong>) ������� �������, &laquo;�����&raquo; (�������������) &mdash; ����� ��&nbsp;�������, ������ ��� ���� ����� �������. ����������, ����������, ��� ����� ����� �������� ������� ������, ������� ������� ��������� ��������.</p>
<h4>��� ���������� ������</h4>
<p>����� �&nbsp;���������� ��&nbsp;���������, ��������� ��&nbsp;������ ������� �������� ������ �&nbsp;���������� ��&nbsp;�������� �&nbsp;�������� �������� ������������ ������ <strong>(1)</strong>. ������ ����� 100&nbsp;�������� ������ ��
������. ��&nbsp;��� ����� ���� �&nbsp;�������� ������. ������ �������� ���. ����� ����� ��&nbsp;��������, ����� ���� ������ �����, ������ ��&nbsp;���������� ��&nbsp;���������. ��&nbsp;�&nbsp;��������� ������� ������ ������, ��� ������ ��� �����, �&nbsp;��� ������������, ������� ��&nbsp;�������� ����� ��������� �����.</p>
<p>� ���� ����� ��� ������� �����, ������� ����������� ����� ��� ���������� �������� &laquo;���� ������&raquo; <strong>(2)</strong>. ������, �� ���������. ����� ����� ������� ������ �&nbsp;���������� ������� <strong>(3)</strong>&nbsp;&#8212; ���� ��&nbsp;����. </p>
<p>����� 700&nbsp;������ ��&nbsp;������� ������������ ����� ��&nbsp;�������� <strong>(4)</strong>. ������ ������� ������ �&nbsp;�������� ������ �&nbsp;������������� �������. ������� ����� �����. ��&nbsp;�������� �������� <strong>(5)</strong>&nbsp;&#8212; ���� �����.</p>
<p>��� ����� 900&nbsp;������ ��� ���� ��� ���� ��&nbsp;��������, �&nbsp;��������� ����������� <strong>(6)</strong>. ������ ������� ������ ������������ �&nbsp;�������� �&nbsp;�������� �&nbsp;������. ������ ������ ��������������� ���, ��� �������� ��&nbsp;��� ����� ������ ��&nbsp;����� ������� �����. �&nbsp;��� ������ ���, ������� ��&nbsp;�������� �����. ����� ����&nbsp;��.</p>
<p>��� ��������� ������ ��������� ���� (��&nbsp;����� ���� ��� ������) �&nbsp;����� ��&nbsp;��������� �������� <strong>(7)</strong>. ������� &laquo;�����&raquo; ������ ������ ��&nbsp;&laquo;����&raquo;. ������, ������ ��&nbsp;������������ �������.</p>
<p>������ ������ ���������� ����� ������ �������� �&nbsp;����������� ������. ����� ��&nbsp;������ (��&nbsp;��������� �������������) ��������� ������� �������&nbsp;&#8212; ������ ����� ������� <strong>(�)</strong>.</p>
<p>� ����� ��������� ������ ������ ������ ���-�� 30-40&nbsp;�����. <strong>��� ������ ������ ��&nbsp;������� &#151; ����� �������� ��������! ��� ��� ��������������, �&nbsp;����� ������� ������� ������, ������ �&nbsp;�.�.</strong></p>
<h4>��� ���������� ��&nbsp;���������� </h4>
<p>������� �&nbsp;������ &laquo;�����&nbsp;&#8212; ������&raquo; ��������� �������� ��&nbsp;114-� ���������, ������� ����������� ������� ��&nbsp;������������� ��������. ��� ����� ����� �������� �&nbsp;������ ������ ��&nbsp;��������� &laquo;��������&raquo;. ����� ��������� �������� 3,5&nbsp;��&nbsp;��&nbsp;��������� ������, ���������� ����� ������ �����������. ��� ������ ������� ��������� �������� ������ (��&nbsp;��� ������ ���� ������ 200), ��������� ������� <strong>(0)</strong>, ����� ������� ��&nbsp;�������. ����� �������� ���������� �&nbsp;������� �������� �������� ������������ ������ <strong>(1)</strong>&nbsp;�&nbsp;������ ������� ��&nbsp;�������� ������ ��������.</p>
<h4>GPS-����������</h4>
<p>���� �&nbsp;��� ���� GPS-��������� �&nbsp;��� ����� ��������, ������ ����� ���� �������� ����� ��������. ��� ��&nbsp;����������:</p>
<p><strong>0.</strong> N60�33.365' E29�09.099' (������� �� �������)<br />
  <strong>1.</strong> N60�33.161' E29�09.055' (����������� ������)<br />
  <strong>2.</strong> N60�33.059' E29�09.135' (���� ������)<br />
  <strong>3.</strong> N60�32.928' E29�09.288' (��������)<br />
  <strong>4.</strong> N60�32.574' E29�09.556' (������������ �����)<br />
  <strong>5.</strong> N60�32.499' E29�09.570' (��������)<br />
  <strong>6.</strong> N60�32.016' E29�09.725' (�����������)<br />
  <strong>7.</strong> N60�31.930' E29�09.720' (��������� ��������)<br />
  <strong>�.</strong> N60�31.805' E29�09.902' (�������)</p>
<h4>����� ��&nbsp;������ ����</h4>
<p>����������, ��� ���� ���������� �&nbsp;������� �������. ������� ����� ��&nbsp;�������� ��� ��&nbsp;�������� (26&nbsp;���). ������� ���� �&nbsp;��� ���� ������� (�&nbsp;�����������) ��������� ��&nbsp;������� �&nbsp;������ ���� ��� �����&nbsp;&#8212; ���������� �������, ����� ����.</p>
<h4>���������� �������� ���������� </h4>
<table border="2" cellpadding="2" cellspacing="2">

<tr>
<td><strong>��������</strong></td>
<td><strong>��������</strong></td>
<td><strong>����������� ������</strong></td>
<td><strong>����� ��������</strong></td>
</tr>
<tr>
<td>5:13</td>
<td>6:58</td>
<td>7:10</td>
<td>���������</td>
</tr>
<tr>
<td>6:53</td>
<td>8:43</td>
<td>8:55</td>
<td>���������</td>
</tr>
<tr>
<td>8:59</td>
<td>10:50</td>
<td>11:02</td>
<td>���������</td>
</tr>
<tr>
<td>10:01</td>
<td>11:49</td>
<td>12:01</td>
<td>���������</td>
</tr>
<tr>
<td>11:21</td>
<td>12:49</td>
<td>13:00</td>
<td>���������</td>
</tr>
<tr>
<td>13:45</td>
<td>15:37</td>
<td>15:49</td>
<td>�� ��������</td>
</tr>
<tr>
<td>16:05</td>
<td>17:54</td>
<td>18:05</td>
<td>���������</td>
</tr>
<tr>
<td>18:11</td>
<td>20:04</td>
<td>20:16</td>
<td>���������</td>
</tr>
<tr>
<td>19:38</td>
<td>21:28</td>
<td>21:40</td>
<td>���������</td>
</tr>
<tr>
<td>21:33</td>
<td>23:20</td>
<td>23:32</td>
<td>���������</td>
</tr>
<tr>
<td>22:15</td>
<td>23:48</td>
<td>0:00</td>
<td>���������</td>
</tr>
</table>
<h4>���� ����������� ����� �&nbsp;����������! </h4>
<p>����������� copy-paste ������������ ��&nbsp;�������. ���������� ������� �� �������������. ��&nbsp;������ <a href="neznaika-dobiralovo.doc">������� doc-����</a> (735&nbsp;��)&nbsp;�&nbsp;������, ��������� ������ �&nbsp;����������� ����������, ���� ����� �����������.</p>            </td>
<td width="150" class="side">


<? show_menu("inc/luna.menu");?>
	     </tr>
<? show_footer(); ?>

