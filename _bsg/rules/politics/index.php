<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header("BSG :: ������� :: �������� � ������", BSG);
show_menu("inc/main.menu"); ?>

<td class="box">

<div class="boxheader"><a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> :: <a href="http://bsg.bastilia.ru/rules/">�������</a> :: �������� � ������</div>
<div>
<h2>�������� � ������</h2>
<h4>������ �&nbsp;���������</h4>
<img src="../../images/baltar.jpg" class="bsgpic" style="float:right"/>
<p>�&nbsp;�������� ����� ������������ ������������� ������������ ����������� �&nbsp;���������� �������. ���� �&nbsp;������� <b>������</b> ������� ��&nbsp;���������� ���������, ��&nbsp;���� ��&nbsp;����� ������&nbsp;&mdash; ��&nbsp;����� �������, �������������� ������� ����� ��������� ����� ���������� (��� ��&nbsp;����� ��� ����� ������� ������ ���������: ������ ��� ���������).
<br />�������, ����������� ������������ �������, ����� ���������� ������ ��&nbsp;����� ����� (��������, ���������� ������������ ������� ��� ������ ����������� ���), �&nbsp;����� �������� ���������� ��� ������������ ������������ ����� ��������� (��������, ���� ���������� ��� ����, ��� ������� ���������� ������).

<p>������ ����� ����� ������������ ������� �������� ������-�� �������. ��� �������� �&nbsp;������������ ��������� ������� �&nbsp;��������� �������� �&nbsp;������������. ����� ������� ������ ������ ������� �������� ��&nbsp;��� �������; ���������� �������� ������� �������� ��������� ������������.

<p>������ <b>��������� �������</b> ���������� ������ ������ ����. ���� ��������� ������������ ������ ��&nbsp;������ 40&nbsp;�����, ������� ���������� �&nbsp;�����������. �������� ������� ����� ��������� ����� ������� ����������. ������ ��&nbsp;�������� ��������� �������. ��������� ����� �������� ��������������� ��������� ������� ���, ��������, ��������� ������������.

<p>���������� ��������, ������� ����� ���� �������� �&nbsp;<b>��������</b> ���������, ��&nbsp;���������� �����, ����� ������������ ���������. 

<p><b>���������</b> ��&nbsp;��������� �&nbsp;�����������, ��&nbsp;����� �������� ���� ��&nbsp;������� �������. ������������� ����� ���� ��������� ��������� ������������� ��&nbsp;���������� ������ ��������� ����������. ���� ����� ��������� ����� ��������� ������������ �������&nbsp;&mdash; ���������� ������ ������ ����������. ��������� ������� �������, ��� ���������� ����� ��������� ����������, ��� ������������ ������,&nbsp;&mdash; ��������� ��� ����� ������������� �&nbsp;������� �&nbsp;�������.

<p>����� ��������� ��� ��������� ������� ����� ���������� ����� �������.

<p>�&nbsp;��� ������, ���� ��������� ��&nbsp;����� ��������� ���� ����������� (��������� ������� ���������, �����, ���� �&nbsp;�.&nbsp;�.), ��&nbsp;���������� ������� ��� ����������� ����������� <b>����-�����������</b>.

<p>����-���������� �&nbsp;��������� ������� ��������� ������. ��&nbsp;������ ���� ��������� ����-���������� ��������, ��������� �������&nbsp;&mdash; ������ �����.

<h4>������������ �������</h4>

<p>������ ��������� ������� ����� �������, ��������� ��&nbsp;��������� ��&nbsp;������� ������� ������ ��&nbsp;�������.

<p>�&nbsp;������� ������������� �&nbsp;������� ���������� ������� ��������������� ��������� ����� ������� ��� �������. ���� ��������� ������ 50%, ��&nbsp;�������� ��������, ��&nbsp;������� ������������ ������ ��� �������, ����� ������� ������� ��������. ��&nbsp;������� �&nbsp;������� �������� ������� �������, ������� ���������� ��������� ���� ������� ������������ ��&nbsp;��� �������.

<p>������������ ��������� �������, ��������� ���������� ��&nbsp;������������� ������ �&nbsp;�&nbsp;�������� ����� ������� ���������� ���������, ���������� ����������� ��� ��&nbsp;������������ ����� �&nbsp;������ ������.

<img src="../../images/journalists.jpg" class="bsgpic" style="float:right"/>
<h4>������</h4>

<p>������ �������� ������������ �������. ������ ������� ������������ �&nbsp;���� �������� ��������, ����� ������������ ������ ����� �����. ������ ��&nbsp;������� ������������ �&nbsp;�������� ��������.

<p>������ ���������� �������� �&nbsp;������ ������ ��� ������������. ��������, ���� �&nbsp;������ &laquo;New Reviews&raquo; �����������, ��� &laquo;������� ������ ��&nbsp;������� ��������� ������� ������� �&nbsp;���������� ���� ��������� �������&raquo;, ��&nbsp;������������ �������� ������� ����� ��������� ��������, ������������ ������, ������ (��� ����������� ��&nbsp;����, ��� ��&nbsp;��������� �&nbsp;����������������).
<br />���� ������� �������� ������������, �&nbsp;�&nbsp;������ ��&nbsp;�������, ��� ��� �����������, ���������, ��� ��� ������������ ��� ����� �������.
<br />�������, ������� ����� ��&nbsp;����������� �&nbsp;������, ��&nbsp;�������� ������ ���� ������������.</div>
 </td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

