<?
define("ROOT","../../../../");
require_once ROOT . 'funcs.php';
show_header("BSG :: ������� :: ����� � ������������� :: ����� �����", BSG);
show_menu("inc/main.menu"); ?>

<td class="box">

<div class="boxheader">
<a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> 
:: <a href="http://bsg.bastilia.ru/rules/">�������</a> 
:: <a href="http://bsg.bastilia.ru/rules/anchor/">����� � �������������</a>
:: �����</div>
<div>
<h2>����� �����</h2>
<p>�������� ������� ����� � ������� ��������� �����, � ���� � ����������. ������� ��� ������ ����� ������� ��������������� �� ��������� �����:</p>
<ol>
<li>����� �������� ������ ��������� ����� ������. ����� ������ �������� ��������-���������-��������� ��� �������� ���������� ���, ��� ��� ������. �������, ������� ���� �����, �� ���������� ������������� �� ��������� (� ����� � � ����� �����), � ��������� �� ������ ��������� ��� �����-�� ������ �����. ��������, ��� �������� ����������, ��� ��� �������.</li>
<li>����� �������� ����� �����, ���� � �������� ���� ������ ��������� ��� ����� ������������� �����. �������, ������� �����, ��������� � � ��� �� �� ���� ���������? ������ ��������? ������ ��������? ������ �������� ����? ���� ������������ ������ ������ ��������� ������ �� ������� �� ���, ����� ��� ���������� �� ����, �� ���� ������ � ���������. ����� �� ����� ����� ������, ��� ����� ������.</li>
<li>����� �� ����� ������ ����������. ��� ������� � ���� ����������. �������, ������� �����, ������� ����������� ���� ��������, � ������� �� ����� ���������. ���� ����� �� ���������� � ���������� ������ �����. ������ ���������� ���-�� �����: �� ����� ������ ���������. ����� � ������� ���, �����, ����� � �� ������� ���-��, � ��� �� �� ���� ������������. ����� �� ������ ����, �����, ����� �� ���� ������� ��� ������ �� �������.</li>

<li>�������� ���������� ������ �����: ���� ����, ��� ���������, ��� ����� (������ ����� � �������, ���� ������ � �������� � ���������). ����� ��������� ��� ������� �����, ��� ��� ������. �� ������ ����� �� ������ �� ��������.</li>
<li>����� � ������ ��������� ����������� ���� ������ ���������. ��������, ���� ������ ���� ������ �������� ������. �.�. �������, �������, ����� ��� ��������� (� ���-���� ��� ���������), �� ������ ����� � ����� ������� ���, � �� ���. ��� ��� �������� ����� ��� ����, ���� �� ���-������ ��� �� ���������. ��� �����, ������� �������� �� ���������, ������� ����������, ��� ���, ������� ������� ��� �� ������ � ��� ����������� ���� ������.</li>
</ol>
<p>�����:</p>
<ul>
  <li>
    ����� ������ ������ ��������� �������� �� ������
  </li>
  <li>
    ����� ������ ��������� �� ��������� ���-��
  </li>

  <li>
    ��� ������ ����� ������� ��������� �������� ��� ���������� �����
  </li>
  <li>
    ����� ������� �� ��������� �����, ��� �� �����������
  </li>
  <li>
    ����� � ������ ���� ������ ���������, � �� ���� �������
  </li>
</ul>
<p>P.S. ���� ��� ����� ����������, ��� ���� ��� ��������� ����� � ������, ����� ������ �������. ���������� � ������������ �� ���� ���� ������ ����: ������� ������ ����� ����� � ���� �����...�</p>
<h4>������� ������</h4>
<p><i>������������ ������ ��� ������ ��� ����� ��������!</i></p>
<ul>
  <li>
    ����
  </li>
  <li>
    ����������
  </li>

  <li>
    ��������
  </li>
  <li>
    ����
  </li>
  <li>
    ������������ �����
  </li>
  <li>

    ��������� ������������
  </li>
  <li>
    �����
  </li>
  <li>
    ���� �����
  </li>
  <li>��������� ����������
  </li>
  <li>
    ������ ����������
  </li>

  <li>
    ������ ������������
  </li>
  <li>��������������
  </li>
  <li>
    ��������
  </li>
  <li>
    ������� ������
  </li>
  <li>

    ����� � ������������
  </li>
  <li>������
  </li>
  <li>
    �����
  </li>
  <li>
    ���� ������
  </li>
  <li>
    �����������
  </li>

  <li>
    ������������� �� ����� �������
  </li>
  <li>��������
  </li>
  <li>
    ����������
  </li>
  <li>
    ����, �� ������� �� ��������
  </li>
  <li>

    ��������������
  </li>
  <li>
    �������
  </li>
  <li>
    �������
  </li>
  <li>
    ������
  </li>

  <li>
    ����
  </li>
  <li>
    ������
  </li>
  <li>
    ������ ����������
  </li>
</ul>

</div>
 </td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

