<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header("BSG :: ������� :: �������", BSG);
show_menu("inc/main.menu"); ?>
<style type="text/css">
<!--
.�����1 {font-weight: bold}
-->
</style>


<td class="box">

<div class="boxheader">
<a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> 
:: <a href="http://bsg.bastilia.ru/orginfo/">�������</a> 
:: ������� </div>
    
<div>
  
 <p>
<table border="1" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
<tr>
<td><h2 align="center"><a href="bsg-dobiralovo.doc">������� ������ ��� ������</a></h2></td>
</tr>
</table>
<p class="�����1"><span>���� ������� ��&nbsp;</span><a href="http://maps.yandex.ru/?text=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F%2C%20%D0%9B%D0%B5%D0%BD%D0%B8%D0%BD%D0%B3%D1%80%D0%B0%D0%B4%D1%81%D0%BA%D0%B0%D1%8F%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C%2C%20%D0%9F%D1%80%D0%B8%D0%BE%D0%B7%D0%B5%D1%80%D1%81%D0%BA%D0%B8%D0%B9%20%D1%80%D0%B0%D0%B9%D0%BE%D0%BD%2C%20%D0%BB%D0%B0%D0%B3%D0%B5%D1%80%D1%8C%20%D0%9C%D0%B5%D1%87%D1%82%D0%B0&amp;sll=29.905006%2C60.583091&amp;sspn=0.065883%2C0.032412">���� ������ &laquo;�����&raquo;</a> (&laquo;�����-����&raquo;)&nbsp;&mdash; ���������� ��&nbsp;������� ����������� (����������� �����).</p>
<p><strong>��������� ��&nbsp;���� ����� ��&nbsp;20:00&nbsp;�������.</strong></p>
<p><strong>���� ���������� ������ ����� ������������� ��&nbsp;<nobr>1-��</nobr> ����� ������� &#8470;&nbsp;7.</strong></p>
<h3>��� ����� ����� �������</h3>
<p><table width="97%" align="center" border="1" cellpadding="1" cellspacing="1">
<tbody>
<tr>
<td rowspan="6"><div align="left">��&nbsp;������������ ����������</div></td>
<td><div align="left">�&nbsp;������� �������</div></td>
<td><div align="left">�������� ��� ���������� ��&nbsp;�����</div></td>
</tr>
<tr>
<td rowspan="2"><div align="left"><strong>�&nbsp;������� �����-����</strong></div></td>
<td><div align="left"><strong>������� ��&nbsp;��������� (8:40, 11:20) + 1&nbsp;�� ������ </strong></div></td>
</tr>
<tr>
<td><div align="left">������� ��&nbsp;������� (9:00, 11:00, 13:00, 15:00) + 3&nbsp;�� ������</div></td>
</tr>
<tr>
<td rowspan="2"><div align="left"><strong>�&nbsp;������� �������</strong></div></td>
<td><div align="left"><strong>���������� ��&nbsp;������� (15:18) + ������� + 1&nbsp;�� ������ </strong></div></td>
</tr>
<tr>
<td><div align="left">���������� ��&nbsp;�������/���������� 16:22, 16:31, 17:30 (��������) + �����</div></td>
</tr>
<tr>
<td><div align="left">�&nbsp;������� ������� (������� �&nbsp;������ ����!)</div></td>
<td><div align="left">���������� ��&nbsp;�������&nbsp;17:35 + ������� + 1&nbsp;�� ������</div></td>
</tr>
<tr>
<td rowspan="2"><div align="left">��&nbsp;����������</div></td>
<td><div align="left"><strong>�&nbsp;������� ����� ��� ������</strong></div></td>
<td><div align="left"><strong>����� ����������� �����, �������� ��&nbsp;��������/��������/����������� </strong></div></td>
</tr>
<tr>
<td><div align="left">�&nbsp;������� ������� (����� ���� ������!)</div></td>
<td><div align="left">����� &laquo;�����������&raquo;, �������� ��&nbsp;������������, ����� ��&nbsp;����������/�����������</div></td>
</tr>
</tbody>
</table>

<a name="map" id="map"></a><p><img src="map.jpg" width="774" height="380" /><p>
����������� �������� ����.
<h3>��&nbsp;������������ ����������
</h3>
<ol>
<li><a href="#1">��&nbsp;���������� ��&nbsp;�������</a>, �&nbsp;������:
<ol type="a">
<li><a href="#1a">��&nbsp;�������� ��&nbsp;�������� ��&nbsp;&laquo;�����&raquo; (�&nbsp;����� 1&nbsp;�� ������ ��&nbsp;����)</a></li>
<li><a href="#1b">��&nbsp;����� ��&nbsp;����� ���� (0&nbsp;�� ������)</a></li>
</ol>
</li>
<li><a href="#2">��&nbsp;�������� ��&nbsp;��������� ��&nbsp;�������� ��&nbsp;&laquo;�����&raquo; (�&nbsp;����� 1&nbsp;�� ������ ��&nbsp;����)</a></li>
<li><a href="#3">��&nbsp;�������� ��&nbsp;������� ��&nbsp;������������ (�&nbsp;����� 3&nbsp;�� ������ ��&nbsp;����)</a></li>
</ol>
<h4><a name="1" id="1"></a>1. ��&nbsp;���������� ��&nbsp;�������, �&nbsp;����� ��&nbsp;�������� ��� �����</h4>
<p><strong>�&nbsp;������� ������� ������������ ����������� ����� ��&nbsp;����������.</strong></p>
<p>���������� ���������� ��&nbsp;3&nbsp;�������� (<a href="http://tutu.ru/spb/rasp.php?st1=46405&amp;st2=47605">������ ��� �������� ����</a>) �&nbsp;�������� �&nbsp;��������:</p>
<a name="rasp" id="rasp"></a><table width="97%" border="1" align="center" cellpadding="1" cellspacing="1">
<tbody>
<tr>
<td><strong>����. ������</strong></td>
<td><strong>���������</strong></td>
<td><strong>�������</strong></td>
<td><strong>����� ��������</strong></td>
<td><strong>������� ����������</strong></td>
<td><strong>��������� ������� </strong></td>
</tr>
<tr>
<td>06:10 </td>
<td>06:40 </td>
<td>07:57 </td>
<td>���������</td>
<td>�������</td>
<td><strong>8:00 (&#8470;&nbsp;646) </strong></td>
</tr>
<tr>
<td>06:52 </td>
<td>07:22 </td>
<td>08:39 </td>
<td>���������</td>
<td>�������</td>
<td>10:10 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td>07:37 </td>
<td>08:10 </td>
<td>09:31 </td>
<td>���������</td>
<td>���������</td>
<td>10:10 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td></td>
<td>08:40 </td>
<td>09:59 </td>
<td>��&nbsp;�������</td>
<td>�������</td>
<td>10:10 (&#8470;&nbsp;897)*</td>
</tr>
<tr>
<td>08:28 </td>
<td>08:59 </td>
<td>10:17 </td>
<td>���������</td>
<td>���������</td>
<td>10:40 (&#8470;&nbsp;645)</td>
</tr>
<tr>
<td></td>
<td>09:15 </td>
<td>10:34 </td>
<td>���������</td>
<td>�������</td>
<td><strong>10:40 (&#8470;&nbsp;645)</strong> </td>
</tr>
<tr>
<td>10:02 </td>
<td>10:32 </td>
<td>11:51 </td>
<td>���������</td>
<td>�������</td>
<td><strong>12:00 (&#8470;&nbsp;646) </strong></td>
</tr>
<tr>
<td>11:07 </td>
<td>11:37 </td>
<td>12:56 </td>
<td>���������</td>
<td>�������</td>
<td>14:10 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td></td>
<td>14:15 </td>
<td>15:33 </td>
<td>���������</td>
<td>�������</td>
<td>16:10 (&#8470;&nbsp;645)</td>
</tr>
<tr>
<td>14:15 </td>
<td>14:46 </td>
<td>16:05 </td>
<td>���������</td>
<td>���������</td>
<td>16:10 (&#8470;&nbsp;645)</td>
</tr>
<tr>
<td><strong>15:18 </strong></td>
<td><strong>15:49 </strong></td>
<td><strong>17:07 </strong></td>
<td><strong>��&nbsp;�������</strong></td>
<td><strong>������� </strong></td>
<td><strong>17:10 (&#8470;&nbsp;646) </strong></td>
</tr>
<tr>
<td>16:22 </td>
<td>16:52 </td>
<td>18:06 </td>
<td>���������</td>
<td>���������</td>
<td>����� ��� 
19:25 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td>16:31 </td>
<td>17:01 </td>
<td>18:19 </td>
<td>���������</td>
<td>�������</td>
<td>����� ��� 
19:25 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td>17:30 </td>
<td>17:51 </td>
<td>18:44 </td>
<td>��������� </td>
<td>��������� (��������)</td>
<td>����� ��� 
19:25 (&#8470;&nbsp;646)</td>
</tr>
<tr>
<td>17:35 </td>
<td>18:06 </td>
<td>19:22 </td>
<td>���������</td>
<td>�������</td>
<td><strong>19:25 (&#8470;&nbsp;646) </strong></td>
</tr>
<tr>
<td>18:35 </td>
<td>19:05 </td>
<td>20:22 </td>
<td>���������</td>
<td>���������</td>
<td>�����</td>
</tr>
<tr>
<td>19:25 </td>
<td>19:56 </td>
<td>21:14 </td>
<td>���������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td>19:55 </td>
<td>20:28 </td>
<td>21:46 </td>
<td>��&nbsp;��������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td>20:58 </td>
<td>21:19 </td>
<td>22:25 </td>
<td>��&nbsp;��������</td>
<td>���������</td>
<td>�����</td>
</tr>
<tr>
<td></td>
<td>22:00 </td>
<td>23:17 </td>
<td>���������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td>22:13 </td>
<td>22:43 </td>
<td>23:59 </td>
<td>����� ������</td>
<td>�������</td>
<td>�����</td>
</tr>
<tr>
<td>23:36 </td>
<td>0:05</td>
<td>01:19 </td>
<td>���������</td>
<td>�������</td>
<td>�����</td>
</tr>
</tbody>
</table>
<p>* ���� ������� ����� �&nbsp;��������, �.�. ��&nbsp;���� ��&nbsp;�����-����������.</p>
<p><em>���� ��������� ��&nbsp;�������, ���������� ������ (���� ��&nbsp;������, ��� ��&nbsp;�������� �������� ����������), ��&nbsp;�������� ����� <nobr>40-60</nobr> ����� ����� ���� ������ ��&nbsp;�������� &#8470;&nbsp;897 (��&nbsp;���������). ��� ���������� ����� ��&nbsp;���������. ��� ������� ��&nbsp;�����, ���� ��&nbsp;������ �����.</em></p>
<p>����������� ��� ������� �&nbsp;����� �������� ����.</p>
<p><strong><a name="1a" id="1a"></a>�. ��&nbsp;�������� ��&nbsp;�������� ��&nbsp;&laquo;�����&raquo;</strong></p>
<p>���������� ��������� ��������� ����� �/�-������� �������. ��� �������� ����� ��������, ������ ��&nbsp;������������ (����������&nbsp;<a href="#rasp">��. ����</a>).</p>
<p>��������� �������� ���������� ��� <strong>�&nbsp;�������� ��&nbsp;&laquo;�����&raquo;</strong>&nbsp;&mdash; ��� �������� ��&nbsp;2&nbsp;�� ��&nbsp;������������. ���������� ��� ������&nbsp;&mdash; ��� ����� ��������� &laquo;�����&raquo; (��. ����� &laquo;1&raquo; <a href="#map">��&nbsp;����� ����</a>).</p>
<p>����� ����� 1&nbsp;�� ��&nbsp;��������� ��&nbsp;����� ����. ��� ��������������� ����� ������ ����� �������� �&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo; (��. ����� &laquo;2&raquo; <a href="#map">��&nbsp;�����</a>). ��� ����� &laquo;����� ����&raquo;.</p>
<p><strong><a name="1b" id="1b"></a>b. ��&nbsp;����� ��&nbsp;����</strong></p>
<p>��������������� ��������� �������� <nobr class="phone">(<strong>+7(921)368-28-70</strong>,</nobr> ������), �������, ��� ��� ����� �&nbsp;&laquo;�����&raquo;, �&nbsp;�������� ����� �������� ����������. ����&nbsp;&mdash; <strong>500&nbsp;�.</strong> ��������������������&nbsp;&mdash; 4&nbsp;��������. ���� ��� ��������� ������, �������� ��&nbsp;���� ��������&nbsp;&mdash; ��&nbsp;������� ��������. �&nbsp;���� ��� ����� ������ ����� (20&nbsp;�&nbsp;������)&nbsp;&mdash; ������� ��������, ��&nbsp;������� ������� <nobr>(100-120 �.</nobr> �&nbsp;��������).</p>
<p>������ ������� ��&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo;. ��� ����� &laquo;����� ����&raquo;.</p>
<h4><a name="2" id="2"></a>2. ��&nbsp;�������� ��&nbsp;���������</h4>
<p><strong>������������� ��&nbsp;������������� ����� �&nbsp;������� �������&nbsp;&mdash; ����������� ����� ������! </strong></p>
<p>
��������� ��&nbsp;��.�. &laquo;���������&raquo; �&nbsp;���� ��&nbsp;���������� &laquo;��������&raquo;. ��� ����� ������� &#8470;&nbsp;897 (��&nbsp;�������������). ����������: <strong>8:40, 11:20, 16:20, 18:50</strong> (�������� ����� ��&nbsp;�������� <nobr class="phone">635-81-58).</nobr> ����� ����� ��<strong>&nbsp;������������</strong> (<strong>140&nbsp;�.</strong>), ����� �&nbsp;����&nbsp;&mdash; <strong>2&nbsp;����</strong> (��� �������&nbsp;&mdash; ������).
<p>
��������� �������� ���������� ��� <strong>�&nbsp;�������� ��&nbsp;&laquo;�����&raquo;</strong>&nbsp;&mdash; ��� �������� ��&nbsp;2&nbsp;�� ��&nbsp;������������. ���������� ��� ������&nbsp;&mdash; ��� ����� ��������� &laquo;�����&raquo; (��. ����� &laquo;1&raquo; <a href="#map">��&nbsp;����� ����</a>). 
<p>
����� ����� 1&nbsp;�� ��&nbsp;��������� ��&nbsp;����� ����. ��� ��������������� ����� ������ ����� �������� �&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo; (��. ����� &laquo;2&raquo; <a href="#map">��&nbsp;�����</a>). ��� ����� &laquo;����� ����&raquo;.
<h4><a name="3" id="3"></a>3. ��&nbsp;�������� ��&nbsp;�������</h4>
<p><strong>������������� ��&nbsp;������������� ����� �&nbsp;������� �������&nbsp;&mdash; ����������� ����� ������! </strong></p>
<p>
��������� ��&nbsp;��.�. &laquo;������&raquo;. (����������� ������� ���� ������ ��&nbsp;��.�. &laquo;�������� �����������&raquo;, ���� ���� ��������� ��&nbsp;<nobr>13-��������</nobr> �������� �������� ��&nbsp;&laquo;�������&raquo;&nbsp;&mdash; ���������� ��� �����, ����� ��&nbsp;�������� ��&nbsp;�������).<p>
<a href="http://maps.yandex.ru/-/CNqQIhd">���������� ���������</a> ��������� ����� �������� ������ ��&nbsp;����� &laquo;������&raquo;. ��� ����� <strong>������� &#8470;&nbsp;678</strong> (��&nbsp;�����������).
<p>
���������� ��������: <strong>9:00, 11:00, 13:00, 15:00, 17:00, 19:00, 21:00</strong>. ������ ������ ������� �&nbsp;<strong>7:15</strong>&nbsp;&mdash; ����� �������� ��� �������, ��������� ����������: <nobr class="phone">+7(921)344-10-38</nobr>
<p>
���� ������&nbsp;&mdash; <strong>160&nbsp;�.</strong> ����� �&nbsp;����&nbsp;&mdash; <strong>2&nbsp;����</strong> (��� �������&nbsp;&mdash; ������). ����������� ��������� �������, ����� ������ ������� �����.
<p>
������� &#8470;&nbsp;678 ����� ��������� ���� ��.�. &laquo;������&raquo;. ����� ������ ��&nbsp;����� ������������� �&nbsp;����������� �����, ��&nbsp;�������� �����&nbsp;&mdash; <a href="http://maps.yandex.ru/-/CNqQVhR">��� ����� ���������</a>. ������� ��������� ��&nbsp;���-�� ����� <nobr>10-15</nobr> ����� ����� ������ �&nbsp;�������.<p><em>
�������� �&nbsp;������� ��&nbsp;����������� ������ ���� �&nbsp;��� ���� �������, ������� ���� ��&nbsp;������� �&nbsp;����� ��� �����&nbsp;&mdash; ����� ���� ���� ����� ��� ������ ������! </em>
<p>
����� ������� �������� �&nbsp;<a href="http://maps.yandex.ru/-/CNqUY1D">�����������</a> (��. ����� &laquo;0&raquo; <a href="#map">��&nbsp;����� ����</a>) ����� ��&nbsp;������ �&nbsp;������� �������. �������� ����� 2&nbsp;�� ��&nbsp;������� ��������� &laquo;�����&raquo; (��. ����� &laquo;1&raquo; <a href="#map">��&nbsp;�����</a>). ����� ��� 1&nbsp;�� ��&nbsp;��������� ��&nbsp;����� ����. ��� ��������������� ����� ������ ����� �������� �&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo; (��. ����� &laquo;2&raquo; <a href="#map">��&nbsp;�����</a>). ��� ����� &laquo;����� ����&raquo;.
<h3>��&nbsp;����������</h3>
<h4>1. ��&nbsp;������������ �����</h4>
<p><strong>������������� ����� �&nbsp;������� ����� ��� ������. ����� ������� ����� ������ ��������� ��������! </strong></p>
<p>
������� ��&nbsp;������������ ����� ��&nbsp;������� �&nbsp;������������� ������ ��&nbsp;��������, �������� �&nbsp;�����������. �������� ����� 20&nbsp;�� ����� �������� ����� ��������� &laquo;�����&raquo; (��. ����� &laquo;1&raquo; <a href="#map">��&nbsp;����� ����</a>). <p>
������������ ������� �&nbsp;���������� 1&nbsp;�� ��&nbsp;��������� ��&nbsp;����� ����. ��� ��������������� ����� ������ ����� �������� �&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo; (��. ����� &laquo;2&raquo; <a href="#map">��&nbsp;�����</a>). ��� ����� &laquo;����� ����&raquo;.

<h4>2. ��&nbsp;������ &laquo;�����������&raquo;</h4>
<p><strong>�&nbsp;������� ������� &laquo;�����������&raquo; ����� ��������, ��� ����������� �����, ��&nbsp;������ ��� ����� �����. ��������� �������. </strong></p>
<p>
��&nbsp;&laquo;�����������&raquo; ��� ����� ��������� ������� ��&nbsp;������������. �����&nbsp;&mdash; �&nbsp;������� ���������� �&nbsp;������������. ����� ������������ ���������� �������� 2&nbsp;�� �&nbsp;������� �������, ���� ��&nbsp;������� ��������� &laquo;�����&raquo; (��. ����� &laquo;1&raquo; <a href="#map">��&nbsp;����� ����</a>).
<p>
����� ��� 1&nbsp;�� ��&nbsp;��������� ��&nbsp;����� ����. ��� ��������������� ����� ������ ����� �������� �&nbsp;��������� &laquo;����� ���&raquo; �&nbsp;&laquo;����� ����&raquo; (��. ����� &laquo;2&raquo; <a href="#map">��&nbsp;�����</a>). ��� ����� &laquo;����� ����&raquo;.
<h3>�������� ������</h3>

<h4>������� &#8470;&nbsp;678 ��&nbsp;������������ ��&nbsp;������� </h4>
<p>
����������: 6:10, 7:10, 10:00, 11:40, 13:40, 15:40, 17:40, 19:40 
<h4>
������� &#8470;&nbsp;897 ��&nbsp;�������� ��&nbsp;&laquo;�����&raquo; ��&nbsp;��������� </h4>
<p>
��&nbsp;����� ������ ����� ������ ����� �������� ��&nbsp;��������������: 7:40, 12:50, 15:20. ��� �&nbsp;����� ������ ��� ����� &laquo;�����&raquo;&nbsp;&mdash; ���������� ��&nbsp;�����.
<h4>
���������� ��&nbsp;������� </h4>
<p>
���������� ��&nbsp;5&nbsp;��������:<table  width="97%" align="center" border="1" cellpadding="1" cellspacing="1">
<tbody>
<tr>
<td><strong>�������</strong></td>
<td><strong>���������</strong></td>
<td><strong>����. ������</strong></td>
<td><strong>����� ��������</strong></td>
<td><strong>������� ����������� </strong></td>
</tr>
<tr>
<td>04:46 </td>
<td>06:06 </td>
<td>06:36 </td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>05:28 </td>
<td>06:42 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>06:00 </td>
<td>07:14 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>06:20 </td>
<td>07:35 </td>
<td>08:04 </td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td>07:44 </td>
<td>08:42 </td>
<td>09:06 </td>
<td>��������� (����������)</td>
<td>���������</td>
</tr>
<tr>
<td>07:50 </td>
<td>09:06 </td>
<td>09:35 </td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td>09:00 </td>
<td>10:16 </td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>09:27 </td>
<td>10:43 </td>
<td>11:11 </td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td>11:00 </td>
<td>12:17 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>12:46 </td>
<td>14:06 </td>
<td>14:37 </td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td>13:20 </td>
<td>14:39 </td>
<td>15:09 </td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>13:53 </td>
<td>15:10 </td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>14:23 </td>
<td>15:40 </td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>15:01 </td>
<td>16:17 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>15:33 </td>
<td>16:26 </td>
<td>16:50 </td>
<td>��&nbsp;�������� (����������)</td>
<td>���������</td>
</tr>
<tr>
<td>16:01 </td>
<td>17:18 </td>
<td>17:48 </td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>17:06 </td>
<td>18:23 </td>
<td>18:52 </td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td>17:30 </td>
<td>18:47 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>17:53 </td>
<td>19:13 </td>
<td>19:44 </td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td>18:20 </td>
<td>19:37 </td>
<td></td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>18:35 </td>
<td>19:54 </td>
<td>20:24 </td>
<td>��&nbsp;��������</td>
<td>���������</td>
</tr>
<tr>
<td>19:01 </td>
<td>20:18 </td>
<td>20:48 </td>
<td>��&nbsp;��������</td>
<td>�������</td>
</tr>
<tr>
<td>19:16 </td>
<td>20:33 </td>
<td>21:03 </td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>19:44 </td>
<td>21:01 </td>
<td></td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>20:04 </td>
<td>21:23 </td>
<td>21:54 </td>
<td>���������</td>
<td>���������</td>
</tr>
<tr>
<td>20:45 </td>
<td>22:04 </td>
<td>22:39 </td>
<td>���������</td>
<td>�������</td>
</tr>
<tr>
<td>22:01 </td>
<td>23:18 </td>
<td>23:47 </td>
<td>���������</td>
<td>���������</td>
</tr>
</tbody>
</table>
  <br />
</div></td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>