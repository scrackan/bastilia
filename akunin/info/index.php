<?
define("ROOT","../../");
require_once("../../funcs.php");
show_header("&laquo;��������� ������&raquo;",AKUNIN);
show_menu("inc/main.menu"); ?>

    <td class="box">
            <div class="boxheader"><a href="http://bastilia.ru/akunin/">��������� ������</a> :: ����������� ����������</div>
            <p>� ������ ������� �������������� ��� �� ����������, ������� �� ��������� � �������� ����, �� ������ ������� ������� ���� �� �� � ������ �������: �����, �������, �������, ���������, � �.�. </p>
            <h4>����� ���������� </h4>
            <p>���� ��������� <strong>10-13 �������</strong>. ����� ���� - ����� �������� (10 �������). ��� ������, ��� � ������� ������� ����� �������������. ����������, ����������, ��� ����������� ���� ��� ������������ ����� ��������. ������ ����: <strong>80-100 �������</strong>. ������������ ������������ �������: �������� <strong>�����</strong> (������������ ������ �������� ����� ��������), �������� <strong>�������</strong> (���� ������ ���� ����� ������ �����������) � �������� <strong>��������</strong> (�.�. ��������� ��������� ����� ��������-���� �������). ������� ���������� � <strong>���������</strong> (������� ���� ���� �� �����-����������).</p>
            <h4>������</h4>
            <p>�������� ������ ������� �� ������ ��� ����� ��������. ����, �� 31 ���� (�����������) ��� ����� ���������� <strong>300 ������</strong>, � � 1 ������� ������������� �� <strong>400 ������</strong>. �� �������� �� (��������, ��� ������� ����� ���, ��� �������, � 9 �������) ��� ������� �������� ��� <strong>500 ������</strong>. ������ �����: �������� ����� �������, � ����� ���� ��� �������. �������� ����� �������� ������  ����-���� �� ���������� �������������� ���������� ������ (��. ����) ��� ��������� ������� ��������� �������� � �������. </p>
            <p>�� ��� ������ ���������� ��������� ������? �� ��� � ������� ��� &quot;���������&quot; (���� �����������, ��� �������, ���), �� ����� ��� ������������ &quot;���������&quot;, �� ������������ �������� (�����, ������� � �.�.), �� ��������� ��������� ��� ������������� (������� � ����� ��� ���������, ����� ��� ������������� ��������, ������, � ��.), �� ������������ ��������������� ��������� (������, ������), �� ������������ ������� �������, �� ������� ������� ��������� ��������, � ����� �� �������� ����� ����� ����� �� ��������. �� ������ ���� ����� ����������� ���������� �����. </p>
            <h4>��������� ��������</h4>
            <p>��� ������������� �������� ����� ����������� ��� ����������������� ���� <strong>��������� ����������� ���������</strong> (��. � <a href="http://bastilia.ru/sigil/rules/town-planning.shtml">�������� �� ��������� ������</a>). Ÿ ������� ����: ������� ������� �������, ������ �������� ����� ���� ��� (��� � ��������� �������� � ����� ��� �������-�������, ����� ������ ��� ���, � �.�.), � ������������ ������ (�.�. ����� ����� � ������������� ���������) � ����� ������� �������� ����.</p>
            <p>��, ������� ��������� ����� �����. �� ��� ������ �� �������� <strong>�������������</strong> ����. ��� ������, ��� ��� <em>�����</em> ����� ��� ����������� ����� <em>����</em> �����. � ����������� ��� �������� ���������� ������, ������� ��������� � ������� ���������. �������, ��� ��������� ������ ���� �� ������ ��������, ��� ������ ���� �������� �� ������� ��� ������������ ���� (���� ��, �������, �� ������������ ����� �� ��������� ������) - ����� ��� ���������� �����������.</p>
            <h4>���������� ������ </h4>
            <p>�� ������ ���������� �� ����������� ����� �������� ���������� ������, ������� �� ����� ����� �������� �����������, ��������������, � �.�. ��� ����, ����������� � �������� ���� &quot;���������� ������&quot;, � ����� �� ������� ����������� � ��������� ��������:</p>
            <p><table border="1" align="center" cellpadding="1" cellspacing="1">
              <tr>
                <th scope="col"><div align="center">������</div></th>
                <th scope="col"><div align="center">���������</div></th>
                <th scope="col"><div align="center">�� ��� ��������</div></th>
                <th scope="col"><div align="center">��������</div></th>
                <th scope="col"><div align="center">E-mail</div></th>
                <th scope="col"><div align="center">ICQ, �� </div></th>
              </tr>
              <tr>
                <td><div align="left"><strong>���</strong> (������ �����) </div></td>
                <td><div align="left">��������</div></td>
                <td><div align="left">����� ����������� ������� </div></td>
                <td><div align="left">+79217776855<br />
                713-62-91</div></td>
                <td><div align="left"><a href="mailto:leo@bastilia.ru">leo@bastilia.ru</a></div></td>
                <td><div align="left">
                    <a href="http://www.livejournal.com/userinfo.bml?user=leotsarev"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="leotsarev" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/leotsarev/">leotsarev</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>�����</strong> (������� �����) </div></td>
                <td><div align="left">�������</div></td>
                <td><div align="left">�������, ����, ������� </div></td>
                <td><div align="left">+79062669367<br />
                428-13-05</div></td>
                <td><div align="left">
                  <div align="left"><a href="mailto:cladbische@bastilia.ru">cladbische<br />
                  @bastilia.ru</a></div>
                </div></td>
                <td><div align="left"><br />
                <a href="http://www.livejournal.com/userinfo.bml?user=mtr"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="mtr" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/mtr/">mtr</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>����� ��</strong> (����� ������) </div></td>
                <td><div align="left">���������</div></td>
                <td><div align="left">�����, ������� </div></td>
                <td><div align="left">+79213970023<br />
                  712-54-88</div></td>
                <td><div align="left"><a href="mailto:antoxa_z@bastilia.ru">antoxa_z<br />
                @bastilia.ru</a></div></td>
                <td><div align="left"><IMG height=18 alt="ICQ status" src="http://web.icq.com/whitepages/online?icq=286823019&amp;img=5" width=18><B><A href="http://wwp.icq.com/286823019">286823019</A></B><br />
                <a href="http://www.livejournal.com/userinfo.bml?user=antoxa_z"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="antoxa_z" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/antoxa_z/">antoxa_z</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>�������</strong> (����� �������) </div></td>
                <td><div align="left">������-<br />
                �������</div></td>
                <td><div align="left">����������� � ��������������� ������� </div></td>
                <td><div align="left">+79112782269<br />
                706-59-73</div></td>
                <td><div align="left"><a href="mailto:admin@bastilia.ru">admin@bastilia.ru</a></div></td>
                <td><div align="left"><IMG height=18 alt="ICQ status" src="http://web.icq.com/whitepages/online?icq=109612345&amp;img=5" width=18><B><A href="http://wwp.icq.com/109612345">109612345</A></B><br />
                <a href="http://www.livejournal.com/userinfo.bml?user=scrackan"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="scrackan" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/scrackan/">scrackan</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>������</strong> (����� �����) </div></td>
                <td><div align="left">Special Guest Star</div></td>
                <td><div align="left">�������, ��������, ���� </div></td>
                <td><div align="left">+79213815915</div></td>
                <td><div align="left"><a href="mailto:raimon@nm.ru">raimon@nm.ru</a></div></td>
                <td><div align="left"><IMG height=18 alt="ICQ status" src="http://web.icq.com/whitepages/online?icq=171879075&amp;img=5" width=18><B><A href="http://wwp.icq.com/171879075">171879075</A></B><br />
                <a href="http://www.livejournal.com/userinfo.bml?user=raimon_"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="raimon_" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/raimon_/">raimon_</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>��������� �������� </strong></div></td>
                <td><div align="left">Special Guest Star</div></td>
                <td><div align="left">������� �� ���������, ����������� </div></td>
                <td><div align="left">
                  <div align="left">
                    <div align="left">+79119453065<br />
                    538-69-48</div>
                  </div>
                </div></td>
                <td><div align="left">
                  <div align="left"><a href="mailto:yatsurenko@rambler.ru">yatsurenko<br />
                  @rambler.ru</a></div>
                </div></td>
                <td><div align="left"><IMG height=18 alt="ICQ status" src="http://web.icq.com/whitepages/online?icq=221220891&amp;img=5" width=18><B><A href="http://wwp.icq.com/221220891">221220891</A></B><br />
                <a href="http://www.livejournal.com/userinfo.bml?user=yatsurenko"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="yatsurenko" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/yatsurenko/">yatsurenko</a></div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>������</strong> (���� ��������) </div></td>
                <td><div align="left">Special Guest Star</div></td>
                <td><div align="left">����������, �����������, ���� ������� � ������ </div></td>
                <td><div align="left">&nbsp;</div></td>
                <td><div align="left">&nbsp;</div></td>
                <td><div align="left">&nbsp;</div></td>
              </tr>
              <tr>
                <td><div align="left"><strong>�����</strong> (���� ���������) </div></td>
                <td><div align="left">Special Guest Star</div></td>
                <td><div align="left">�����������, ���� ������� � ������ </div></td>
                <td><div align="left">&nbsp;</div></td>
                <td><div align="left"><a href="mailto:eowin@yandex.ru">eowin@yandex.ru</a></div></td>
                <td><div align="left"><a href="http://www.livejournal.com/userinfo.bml?user=e_vin"><img height="17" border="0" src="http://www.livejournal.com/img/userinfo.gif" alt="e_vin" align="absmiddle" width="17"></a><a style="FONT-WEIGHT: 800" href="http://www.livejournal.com/users/e_vin/">e_vin</a></div></td>
              </tr>
            </table></p>
            <p>���� ������������, � ���� ���������� - ����� ������ �� ���� ���� <a href="mailto:akunin@bastilia.ru">akunin@bastilia.ru</a> (��� ������ ��� ������� ��������� �������). ����� �� ��������� ��� ��-���������� <a href="http://www.livejournal.com/community/bastilia/">bastilia</a>.</p>
            <h4>������� </h4>
            <p>��������� ���������� � �������� ������ ������������ � ���� <a href="../polygon/index.php">��������� ������</a>. </p>
</td>
    <td width="150" class="side">
<? show_menu("inc/akunin.menu");?>
    </tr>
<? show_footer(); ?>