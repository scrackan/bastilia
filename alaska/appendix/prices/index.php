<?
define("ROOT","../../../");
require_once("../../../funcs.php");
show_header("��������: ������� ��������� :: ���������� :: ��������� ���� �� ����",ALASKA);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/alaska/">��������: ������� ���������</a> :: <a href="http://bastilia.ru/alaska/appendix/">����������</a> :: ��������� ���� �� ���� </div>
                        
     <p>��� ��� ���� ������� �&nbsp;<a href="../../rules/">��������</a> (���������&nbsp;&mdash; �&nbsp;������ ��� ���������), ������ ����� ������� ��&nbsp;������ ���� ������������ ����� �����. ��&nbsp;������ ���������� �������� ��� ��������� ������� �&nbsp;��������� ���� ����� ������� ��������� ��&nbsp;3&nbsp;���������: ������ (����� 20&nbsp;$), ������� (����� 40&nbsp;$) �&nbsp;������� (����� 200&nbsp;$). ���������� ������� ������� ��� ��&nbsp;��������&nbsp;&mdash; �&nbsp;�������� ����������� ����������� �����. ���� ������������ ������� ��������������� ��&nbsp;���� ��,&nbsp;����������, ����� ��&nbsp;������.</p>
<p>1&nbsp;������ ������������ 1,5&nbsp;������� ������.</p>
<p>���� ������������ ��������������� ���� ��&nbsp;������� ������ �&nbsp;������. � ������ �� ��������� �������������� ����� � <a href="../law/">������� � ����������</a>. </p>
<p><strong>��� �&nbsp;������� �&nbsp;��������:</strong></p>
<ul>
<li>����� ����� ��� ������� &mdash; 1&nbsp;$</li>
<li>����� ����� &mdash; 2&nbsp;$</li>
<li>������ ��� &mdash; 1&nbsp;$</li>
<li>��������� &mdash; 1&nbsp;$</li>
<li>������ ����������� &mdash; 1&nbsp;$</li>
<li>������� ������� ��� &mdash; 2&nbsp;$ </li>
</ul>
<p><strong>������������ ������ ��������� ��������:</strong></p>
<ul>
<li>������ �&nbsp;�������� (�&nbsp;�������� ��������) &mdash; 3&nbsp;$ </li>
<li>������� ��&nbsp;����������� ������� &mdash; 5&nbsp;$ </li>
<li>������-��������� &mdash; 1&nbsp;$ ��&nbsp;��������� 1&nbsp;��&nbsp;����� (����� ��� ����� ����������� ��&nbsp;����) ��&nbsp;1&nbsp;��,&nbsp;�.�. 3&nbsp;$/�� ��&nbsp;���� ��&nbsp;������� ��&nbsp;�������� &laquo;������-�����&raquo; (������ ��� ������ ���� ��&nbsp;�������) </li>
<li>����� ��&nbsp;���-��������� &mdash; 30&nbsp;$ </li>
</ul>
<p><strong>������� ������:</strong></p>
<ul>
<li>������� �������� �������� &mdash; 10&nbsp;$ </li>
<li>����� (����� �������) &mdash; 3-4&nbsp;$ </li>
<li>����� ������� &mdash; 3-5&nbsp;$ </li>
<li>����� �����/������� &mdash; 3&nbsp;$ </li>
<li>������ ���� (����� �������) &mdash; 5&nbsp;$ </li>
<li>������ ��������� &mdash; 5&nbsp;$</li>
</ul>
<p><strong>������ �&nbsp;����������:</strong></p>
<ul>
<li> ����� &mdash; 20&nbsp;$ </li>
<li>��������� &mdash; 15&nbsp;$ </li>
<li>������� &mdash; 1&nbsp;$ ��&nbsp;2&nbsp;������� ��� 2-3&nbsp;$ ��&nbsp;������ ��������</li>
</ul>
<p><strong>������������� ����������:</strong></p>
<ul>
<li>������ &mdash; 5&nbsp;$ </li>
<li>����� &mdash; 7&nbsp;$ </li>
</ul>
<p><strong>������:</strong></p>
<ul>
<li>����� ��������� &mdash; ��&nbsp;3&nbsp;$ </li>
<li>���� ���������� &mdash; ��&nbsp;5&nbsp;$ </li>
<li>������������� ������� &mdash; ��&nbsp;100&nbsp;$ </li>
</ul>



</td>
	    <td width="150" class="side">

<? show_menu("inc/alaska.menu");?>
	     </tr>
<? show_footer(); ?></td>
