<?php

if (!defined('ROOT'))
	die ('Error!');

define('SQL_HOST','mysql.1.leotsar.cz8.ru');
define('SQL_USER','dbu_leotsar_1');
define('SQL_PHPBB','db_leotsar_3');
define('SQL_SITE','db_leotsar_1');
define('SQL_PASSWORD','3iNYxdDViuj');
define('SQL_PHPBB_USERS','phpbb_users');

$handle = FALSE;

function connect()
{
	global $handle;
	if ($handle)
		return $handle;
	$handle = @mysql_connect(SQL_HOST, SQL_USER, SQL_PASSWORD);
	mysql_query("SET NAMES cp1251");
	if (!$handle)
	{
 		die('Could not connect to the database');
	}
	return $handle;
}

function do_query ($sql)
{
	return mysql_query ($sql, connect());
}

function do_query_or_die ($sql)
{
	$result = do_query($sql);
	if (!$result)
	{
	  die(mysql_error());
	}
	return $result;
}

function fetchrow ($result)
{
	return mysql_fetch_array($result);
}

function free_result ($result)
{
	return mysql_free_result ($result);
}

function quote_smart($value)
{
    // ���� magic_quotes_gpc �������� - ���������� stripslashes
    if (get_magic_quotes_gpc()) {
        $value = stripslashes($value);
    }
    // ���� ���������� - �����, �� ������������ � �� �����
    // ���� ��� - �� ������� � ���������, � ����������
    if (!is_numeric($value)) {
        $value = "'" . mysql_real_escape_string($value, connect()) . "'";
    }
    return $value;
}

function get_scalar ($sql)
{
	$result = do_query_or_die ($sql);
	$row = fetchrow ($result);
	free_result ($result);
	return $row;
}

function get_array ($sql)
{
	$result = do_query_or_die ($sql);
	$row = fetchrow ($result);
	$array = array();

	while ($row)  {
		$array[] = $row;
		$row = fetchrow ($result);
	}

	free_result ($result);
	return $array;
}

function last_insert_id()
{
	return mysql_insert_id();
}
