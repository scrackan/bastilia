<?
define('ROOT', '../../');
header('Content-type:application/rss+xml; charset=utf-8');
require_once ROOT . 'funcs.php';
require_once ROOT . 'appcode/news.php';

$news = find_last_news (DEFAULT_COUNT);

echo("<?xml version='1.0' encoding='utf-8' ?>\n");
echo("<rss version=\"2.0\"> <channel><title> МТГ «Бастилия»</title><link>http://bastilia.ru/</link>\n");
echo("<description>Bastilia.Ru - полевые игры в СПб</description>\n");
echo("<generator>Новостной движок от Л. Царева (Лео) - специально для Bastilia.Ru</generator>\n");
echo('<lastBuildDate>' . date('D, d M Y H:i:s O',strtotime($news[0]['news_date'])) . "</lastBuildDate>\n");
echo("<language>ru-ru</language>\n<copyright> (c) МТГ «Бастилия» </copyright>\n");
echo("<webMaster> webmaster@bastilia.ru </webMaster>\n");

function convert($string)
{
  $string = iconv ("Windows-1251", "UTF-8", $string);
  return htmlspecialchars($string);
}

foreach ($news as $item) {


	echo "<item>\n";
	echo " <title>" . convert($item['news_header']) . "</title>\n";

	echo '<description>' . convert($item['news_text']);
	echo '&lt;br /&gt; ';
	$ljuser = $item['news_author'];
	$link = get_lj_path ($ljuser, false);
	echo "&lt;a href=\"$link/profile\"&gt; ";
	echo '&lt;img src="http://bastilia.ru/images/userinfo.gif" width="17" height="17" style="border: 0;" alt="LJ" /&gt;';
	echo '&lt;/a&gt;';

	echo "&lt;a href=\"$link/\"&gt; &lt;b&gt;$ljuser&lt;/b&gt;&lt;/a&gt; </description>\n";

	echo ' <guid isPermaLink="true"> http://bastilia.ru/index.php#' . date('YmjHis', strtotime($item['news_date'])) . "</guid>\n";
	echo " <pubDate>". date('D, d M Y H:i:s O', strtotime($item['news_date'])) . "</pubDate>\n";

	echo " <author>mailto:bastilia@bastilia.ru</author>\n";
	echo "</item>\n";
}
echo("</channel></rss>");
?>
