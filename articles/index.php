<? 
define("ROOT","../");
require_once("../funcs.php");
show_header("��������",BEFREE);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader">��������</div>
<p>����, ��������� �����, ��&nbsp;����� �&nbsp;����� �������� 
����� &laquo;������������ ��������&raquo;&nbsp;&mdash; �&nbsp;��������. ����� ���� ����� �����, 
������� ����������� �������� �&nbsp;��������� �����, �����, &laquo;��������� 
�������&raquo;, �����, �&nbsp;����� �������&nbsp;&mdash; ����� ������ ����� ��������� 
�&nbsp;��������. ��&nbsp;�������� �������� ���������� �������� ���������� 
�&nbsp;����� ����������� ������� �&nbsp;������� ������������&nbsp;&mdash; ������� �����. 
�������&nbsp;��, ��&nbsp;������ ������������ �&nbsp;����, ������� ��&nbsp;������ &laquo;�����&raquo; 
�&nbsp;����� ������� ���� ��������, ������, ������������ ��&nbsp;�������, 
��&nbsp;�������� ����������� ���������������� ������������ ��&nbsp;������ 
������ ��������������� �&nbsp;������ ������� ������� ������������.</p>
<h4>����������� ������� �&nbsp;���������</h4>
<p><img src="../images/article.gif" width="18" height="16" /><em>�������. </em><a href="mojito/">������ ��� ��������, ��� �� �&nbsp;������� ����.</a> &mdash;&nbsp;������ �&nbsp;&laquo;������� ������&nbsp;&mdash; 2010&raquo;. �������������� �������, ���������� ����������, ��������� �&nbsp;���������� ��������������. �������� �������� ����������� �������� ����������� �&nbsp;������� �������. </p>
<p><img src="../images/article.gif" width="18" height="16" /><em>������� �&nbsp;���.</em> <a href="winter_games/">������ ����: ��� ������ �������.</a> &mdash;&nbsp;������ �&nbsp;�������-2010. ������ ��&nbsp;����� ������� ������ ����� �&nbsp;����. �&nbsp;�������������. </p>
<p><img src="../images/article.gif" width="18" height="16" /><em>���. </em><a href="writing-rules/">��������� ������: ������� ����.</a> &mdash;&nbsp;������, ����������� �&nbsp;����������� ������� �&nbsp;�������-2010. ��� ������ �������. �������� ���������� �&nbsp;��� ��������, �&nbsp;��� ��������. </p>
<h4>������ 2004&nbsp;�. </h4>
<p><img src="../images/article.gif" width="18" height="16"><em>����� 
�&nbsp;������. </em><a href="http://bastilia.ru/articles/oleni/">��������� ������� �������� 
������ �&nbsp;�������� ������ �&nbsp;����� ����������� ���������.</a> &mdash;&nbsp;������, 
������� ����������� ��� ��&nbsp;������ �������������. �����, ������ �����, 
������ �&nbsp;�������������� (��� �&nbsp;�� ��&nbsp;����� �����), ��������&nbsp;�. 
�. &laquo;��-����������&raquo; �&nbsp;����� ������� �&nbsp;�������������� ��������. 
�������, ������ ����������, ��� ��&nbsp;&laquo;����������������&raquo; 
�&nbsp;&laquo;���������� �����������&raquo; ���������� ����� �������� �������������� 
�����.</p>

<p><img src="../images/article.gif" width="18" height="16"><em>�����.</em> 
<a href="http://bastilia.ru/articles/madness">����� �������.</a> &mdash;&nbsp;��������-����������� 
�������������������, ���������� ���������� ��� ���� &laquo;����� ���������. 
������ ��� ������&raquo;, ��&nbsp;��� �&nbsp;��&nbsp;���������� �������� �������.</p>
<p><img src="../images/article.gif" width="18" height="16"><em>���.</em> 
<a href="http://bastilia.ru/articles/courts">Complete Idiot&rsquo;s Guide to&nbsp;Creating Courts 
on&nbsp;the LARP games</a>, ��� ��� ��� �&nbsp;������� ����������� ���� �� 
�����.</p>
</td>
<?php right_block(''); show_footer(); ?>

