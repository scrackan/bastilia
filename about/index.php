<?php
define("ROOT","../");
require_once("../funcs.php");
show_header("��������",BEFREE);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader">��������</div>
<p>����, ���-���! ���� ��&nbsp;������, ����� �&nbsp;�����! ���������� �����? ������� ����������,
��&nbsp;������&nbsp;��? �&nbsp;��������� ����������. ��&nbsp;��&nbsp;������ ��&nbsp;���, ��� �������.
���� ������ �&nbsp;������ ������� ����� ��&nbsp;����� ����. ��&nbsp;��������! ��������,
���. ���� ����������, �������&nbsp;��, ���� ��&nbsp;�����? �&nbsp;��������, ���
�������! ������ ��� ����� �����, ��? �&nbsp;���� ���&nbsp;&mdash; ����� ����� ������������...</p>
<p>��� �������: <a href="index.shtml">�������� ��&nbsp;2004&nbsp;���</a>, ����� ��� ���&nbsp;��, �&nbsp;�������� ������ �����������..</p>
<table class="faces" cellpadding="7" cellspacing="2">
	<tr><td colspan="2"><h3>�������� ������ �������������� ������ &laquo;��������&raquo;</h3></td></tr>
	<tr><td class="avatar"><img src="photo/scrackan.jpg" /></td>
		<td><h4>������� (����� �������)</h4>
			<p>����� ������������� ���� �������������� ������.
				������������, ������. ������� ��&nbsp;���������� �������, ��� ������ �&nbsp;�����. ��� �������� �&nbsp;������������
				������������ ������� ������ ������������.</p>
				<ul>
					<li>Bastilia.Ru �&nbsp;������, �������������</li>
					<li>&laquo;������������ ������&raquo;, ������</li>
					<li>&laquo;������������ ����&raquo;, ������</li>
					<li>&laquo;Planescape: Sigil&raquo;, ������� ������ �&nbsp;������-������������ ��&nbsp;������ ����</li>
					<li>&laquo;���������� �����&raquo;, ������-������������ ��&nbsp;�������</li>
					<li>&laquo;��������� ������&raquo;, ������-������������� �&nbsp;������ ��&nbsp;�������������</li>
					<li>&laquo;���� ���������&raquo;, ������-������������� �&nbsp;������ ��&nbsp;���������</li>
					<li>&laquo;��������&raquo;, ������ �&nbsp;����� ����������������� ������</li>
					<li>&laquo;��������: ������� ���������&raquo;, ������� ������</li>
					<li>&laquo;�������� ��&nbsp;����&raquo;, ������-����������� �&nbsp;������-�������������</li>
					<li>&laquo;XVI: ��� �&nbsp;����������&raquo;, ������ ��&nbsp;��������</li>
					<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������-������������� </li>
					<li>&laquo;���&raquo;, ������� ������</li>
					<li>&laquo;�����: ����� �����&raquo;, ������-�������������, ������ ��&nbsp;��������� �&nbsp;������� &laquo;����-������&raquo;</li>
					<li>&laquo;����� ���������: �����������&raquo;, ������-�������������</li>
					<li>&laquo;�����������&raquo;, ������</li>

<li>&laquo;�������: ��&nbsp;��&nbsp;�������&raquo;, ������</li>
<li>&laquo;���� �����&raquo;, ������-�������������</li>
<li>&laquo;����&raquo;, ������� ������</li>
				</ul>
				<p><strong>�������</strong>:
<ul>
					<li>&laquo;������� ����-2005&raquo; ��&nbsp;&laquo;���������� �����&raquo;</li>
<li>&laquo;������� ����-2007&raquo; ��&nbsp;&laquo;���� ���������&raquo;</li>
<li>���� �������&mdash;2010 ��&nbsp;&laquo;XVI: ��� �&nbsp;����������&raquo;</li>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������</strong>: <?php lj_user('scrackan'); ?></p>
			<p><strong>����������� �����</strong>: <a href="mailto:admin@bastilia.ru">admin@bastilia.ru</a></p>
				<p><strong>Jabber</strong>: boris.zemskov@gmail.com</p> 		</td></tr>
			<tr><td class="avatar"><img src="photo/mtr.jpg" ></td>
				<td><h4>����� (������� �����)</h4>
			<p>C���� ����������� ���� �������������� ������, ����� ������ �������������� ����. ������������ �������� ��������� �&nbsp;������� ��������. ������� ���� ���� ����������� �������, �&nbsp;������, ��� ��� ������ �&nbsp;�����������. ����� ��������� ������ ������.</p>
			<ul>
				<li>&laquo;������������ ������&raquo;, ������</li>
				<li>&laquo;������������ ����&raquo;, ������</li>
				<li>&laquo;Planescape: Sigil&raquo;, ������-������������ ��&nbsp;������ �������</li>
				<li>&laquo;���������� �����&raquo;, ������� ������, ����� ���������</li>
				<li>&laquo;��������� ������&raquo;, ��������</li>
				<li>&laquo;���� ���������&raquo;, ������ ��&nbsp;������ �&nbsp;����� ������ ��&nbsp;����������</li>
				<li>&laquo;��������&raquo;, ������</li>
				<li>&laquo;������&raquo;, ������� ������</li>
				<li>&laquo;�������� ��&nbsp;����&raquo;, ����������</li>
				<li>&laquo;XVI: ��� �&nbsp;����������&raquo;, ���������� �����������</li>
				<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, �������� ��&nbsp;��������</li>
				<li>&laquo;���-��� �&nbsp;������ ���-����&raquo;, ������� ������</li>
				<li>&laquo;����� ��������: �����������&raquo;, ������ ������������ �����������</li>
				<li>&laquo;�����������&raquo;, ������� ������</li>
<li>&laquo;����&raquo;, ������</li>
		 </ul>
			<p><strong>�������</strong>:
<ul>
					<li>&laquo;������� ����-2005&raquo; ��&nbsp;&laquo;���������� �����&raquo;</li>
			<li>&laquo;������� ����-2007&raquo; ��&nbsp;&laquo;���� ���������&raquo;</li>
<li>������ �������-2012 &laquo;������ �������������� �����&raquo; ��&nbsp;&laquo;����������� ������ ������� �&nbsp;��������� �������� �&nbsp;����� �������&raquo;</li>
			
</ul>
			<p><strong>����� ������</strong>: <?php lj_user('mtr'); ?></p>
			<p><strong>����������� �����/Jabber</strong>: <a href="mailto:mtr@bastilia.ru">mtr@bastilia.ru</a></p> 			</tr>
			<tr><td class="avatar"><img src="photo/leo.jpg" /></td>
				<td>
<h4>��� (������ �����)</h4>
			<p>����� ������� ���� �������������� ������. ����� ������������ �&nbsp;��� ������������ ��-������, ��&nbsp;����� ������� ���� ��&nbsp;�������� ����. ������������. �&nbsp;�������: ��&nbsp;����� �������� ��� &laquo;������� ���&raquo;&nbsp;&mdash; ��&nbsp;&laquo;����� ������� ���&raquo;.</p>
			<ul>
				<li>Bastilia.Ru �&nbsp;������, ��-�������������</li>
				<li>&laquo;Planescape: Sigil&raquo;, ���������� �&nbsp;����� ����������������</li>
				<li>&laquo;���������� �����&raquo;, ������-������������ ��&nbsp;���-���������</li>
				<li>&laquo;��������� ������&raquo;, ��������</li>
				<li>&laquo;���� ���������&raquo;, ������-����������� ��&nbsp;������, ������ ��&nbsp;���������</li>
				<li>&laquo;��������&raquo;, ������</li>
				<li>&laquo;��������: ������� ���������&raquo;, ������</li>
				<li>&laquo;�������� ��&nbsp;����&raquo;, ������ ��&nbsp;������� �&nbsp;�������</li>
				<li>&laquo;XVI: ��� �&nbsp;����������&raquo;, �������� ������������ ������� &laquo;����� ����&raquo;</li>
			 <li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������� ������ </li>
			 <li>&laquo;���&raquo;, ������</li>
			 <li>&laquo;���-��� �&nbsp;������ ���-����&raquo;, &laquo;������ ��&nbsp;���������&raquo;</li>
			 <li>&laquo;�����: ����� �����&raquo;, ������� ������</li>
			 <li>&laquo;����� ��������: �����������&raquo;, ������ ��&nbsp;������� �&nbsp;��������, �����������</li>
			 <li>&laquo;�������� �����&raquo;, ������</li>
<li>&laquo;���� �����&raquo;, ������� ������</li>
			</ul>
			<p><strong>�������</strong>:
		 <ul>
					<li>&laquo;������� ����-2005&raquo; ��&nbsp;&laquo;���������� �����&raquo;</li>
			<li>&laquo;������� ����-2007&raquo; ��&nbsp;&laquo;���� ���������&raquo;</li>
			<li>&laquo;������� ����-2009&raquo; ��&nbsp;&laquo;������� �����&raquo;</li>
			<li>���� �������&mdash;2010 ��&nbsp;&laquo;XVI: ��� �&nbsp;����������&raquo;</li>
			<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
			<li>&laquo;������� ��� 2012&raquo;, ��&nbsp;���������� �������� �&nbsp;������ �&nbsp;���� &laquo;��������� �����&raquo;</li>
			</ul>
				<p><strong>����� ������</strong>: <?php lj_user('leotsarev'); ?></p>
			<p><strong>����������� �����/Jabber</strong>: <a href="mailto:leo@bastilia.ru">leo@bastilia.ru</a></p> 				</td>
			</tr>

			<tr><td class="avatar"><img src="photo/wer.jpg" /></td>
				<td>
		 <h4>��� (������ ���������)</h4>
			<p>����� ����� ���� ��� &laquo;��������&raquo;. �������� ����� ���������� �����������.</p>
			<ul>
				<li>&laquo;Planescape: Sigil&raquo;, ������-������������ ��&nbsp;�����</li>
				<li>&laquo;���������� �����&raquo;, �������������: ������-������������ ��&nbsp;�������; �����: �������� �������-������������� ��&nbsp;���-���� �&nbsp;����������</li>
				<li>&laquo;Quo Vadis?&raquo;, ������</li>
				<li>&laquo;��������� ������&raquo;, ����������</li>
				<li>&laquo;���� ���������&raquo;, ������ ��&nbsp;������</li>
				<li>&laquo;��������&raquo;, ������</li>
				<li>&laquo;��������: ������� ���������&raquo;, ������</li>
				<li>&laquo;�������� ��&nbsp;����&raquo;, ������ ��&nbsp;�������� ������ �&nbsp;���������� ��� ������</li>
				<li>&laquo;XVI: ��� �&nbsp;����������&raquo;, ����������� ������� &laquo;����� ����&raquo;</li>
			 <li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;������, �������� ������� ��&nbsp;������� &laquo;Galactica&raquo; </li>
			 <li>&laquo;���&raquo;, ������ ��&nbsp;������� &laquo;������� ���&raquo;</li>
			 <li>&laquo;�����: ����� �����&raquo;, ������ ��&nbsp;�����������</li>
			 <li>&laquo;����� ��������: �����������&raquo;, ������� ������</li>
			 <li>&laquo;�����������&raquo;, ����������</li>
<li>&laquo;�������: ��&nbsp;��&nbsp;�������&raquo;, ������</li>
			 
			</ul>
			<p><strong>�������</strong>:
		 <ul>
					<li>&laquo;������� ����-2005&raquo; ��&nbsp;&laquo;���������� �����&raquo;</li>
			<li>&laquo;������� ����-2007&raquo; ��&nbsp;&laquo;���� ���������&raquo;</li>
			<li>���� �������&mdash;2010 ��&nbsp;&laquo;XVI: ��� �&nbsp;����������&raquo;</li>
			<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
		 </ul>
				<p><strong>����� ������:</strong> <?php lj_user('wer'); ?></p>
			<p><strong>����������� �����/Jabber:</strong> <a href="mailto:wer@bastilia.ru">wer@bastilia.ru</a></p> 				</td>
			</tr>
			
<tr>
<td class="avatar"><img src="photo/ksi.jpg" width="100" height="100" /></td>
<td><p><strong>�������� (���� ��������)</strong>
</p>
<p>������ �&nbsp;���� �������-��������� (���� ���������), ���� ������� �&nbsp;���� ������-���������. �������� �������� �&nbsp;������ ��&nbsp;���������. ������ ������ ����� ��&nbsp;����, ����� ���������� ����������� ����������� ���. ������������ �����������, ���������� ������, �������� ����. </p>
<ul>
<li>&laquo;��������&raquo;, ������ ������������ ����������� (�������)</li>
<li>&laquo;�������� ��&nbsp;����&raquo;, ���������� �����</li>
<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;��������</li>
<li>&laquo;�����: ����� �����&raquo;, ������ ��&nbsp;������� &laquo;������&raquo;</li>
<li>&laquo;����&raquo;, ������ ��&nbsp;������</li>
</ul>
<p><strong>�������</strong>: </p>
<ul>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������:</strong>
<?php lj_user('ksiontes'); ?>
</p>
<p><strong>����������� �����</strong>: <a href="mailto:ksi@bastilia.ru">ksi@bastilia.ru</a></p>
		<p><strong>ICQ</strong>: 241389124</p>
		<p><strong>Jabber</strong>: ksiontes@gmail.com</p>
</td>
</tr>


<tr>
<td class="avatar"><img src="photo/atana.jpg" width="100" height="100" /></td>
<td><p><strong>����� (���������� ������)</strong>
</p>
<p>����� ����� �&nbsp;������ ������� ����. ��������� ���� ����� �&nbsp;����� �&nbsp;��������� �&nbsp;���������. ������������� ������� ���� ��&nbsp;������� ����������� ��������� ��������.</p>
<ul>
<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;������</li>
<li>&laquo;�����: ����� �����&raquo;, ����������� �������� ������ �&nbsp;������ ��&nbsp;�������� &laquo;�����&raquo; �&nbsp;&laquo;���-��&raquo;</li>
<li>&laquo;���� �����&raquo;, ���. �������� �������</li>
</ul>
<p><strong>�������</strong>: </p>
<ul>
<li>&laquo;������� ����-2009&raquo; ��&nbsp;&laquo;������� �����&raquo;</li>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������:</strong>
<?php lj_user('aleks'); ?>
</p>
<p><strong>����������� �����</strong>: <a href="mailto:atana@bastilia.ru">atana@bastilia.ru</a></p>
		<p><strong>ICQ</strong>: 299207403</p>
		<p><strong>Jabber</strong>: aleksvlasova@ya.ru</p>
</td>
</tr>

<tr>
<td class="avatar"><img src="photo/clair.jpg" width="100" height="100" /></td>
<td><p><strong>���� (����� �������)</strong>
</p>
<p>����� ��������� ��� ������� ����, ������ ������ �������� ������ �&nbsp;��������� ��&nbsp;&laquo;BSG&raquo;, ���������� ����� ��&nbsp;����� ������� �&nbsp;����� ��������� ��� �������� �������������� �������. ��������������. ������������� ������� ������� ���� ���.</p>
<ul>
<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;������� &laquo;Colonial One&raquo;</li>
<li>&laquo;�����: ����� �����&raquo;, ������ ��&nbsp;�������� &laquo;�����&raquo; �&nbsp;&laquo;������ �����&raquo;</li>
<li>&laquo;����� ��������: �����������&raquo;, ������ ��&nbsp;������� �����</li>
<li>&laquo;���� �����&raquo;, ������-������������</li>
</ul>
<p><strong>�������</strong>: </p>
<ul>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������:</strong>
<?php lj_user('le-ange-clair'); ?>
</p>
<p><strong>����������� �����/Jabber</strong>: <a href="mailto:clair@bastilia.ru">clair@bastilia.ru</a></p>
		<p><strong>ICQ</strong>: 272732532 </p>

</td>
</tr>

			<tr><td class="avatar"><img src="/about/photo/alicia.jpg" /></td>
				<td><p><strong>������ (����� ����������)</strong></p>
				<p>������� �&nbsp;���������������, ����� ������������� ��&nbsp;������������ �&nbsp;������������ ������ �����. ������������ ��������� �&nbsp;����������� ��&nbsp;�������� �&nbsp;��� �&nbsp;�������� ������� ��&nbsp;���� ��������. ������� �����������&nbsp;&mdash; ����� ������� ��������� ������ ���, ��� ������������ ��� ���������. ���������� ������&nbsp;&mdash; &laquo;�������&raquo;.</p>
			<ul>
										<li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;�������� �&nbsp;�������</li>
											<li>&laquo;�����: ����� �����&raquo;, ����������</li>
											<li>&laquo;����� ��������: �����������&raquo;, ���. �����������</li>
<li>&laquo;���� �����&raquo;, ������-������������</li>
</ul>
			<p><strong>�������</strong>: </p>
<ul>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������:</strong> <?php lj_user('_mcgoogles_'); ?></p>
			<p><strong>����������� �����/Jabber</strong>: <a href="mailto:alicia@bastilia.ru">alicia@bastilia.ru</a></p>
			</td></tr>



<tr><td class="avatar"><img src="/about/photo/borisych.jpg" /></td>
<td><p><strong>������� (������� �����)</strong></p>
<p>��������� �������. ���������������� ������� ��������. �&nbsp;2005&nbsp;�. ����� ��&nbsp;������ ����������� ����. �&nbsp;2012&nbsp;�. �������-�� ����� ��������� &laquo;����������&raquo; ��������� �&nbsp;���������.</p>
<ul>
<li>&laquo;���� ���������&raquo;, ������ ������������ ����������� (������)</li>
<li>&laquo;��������&raquo;, ������ ������������ ����������� (���������, �����)</li>
<li>&laquo;�����: ����� �����&raquo;, ������ ������������ �����������</li>
<li>&laquo;�������: ��&nbsp;��&nbsp;�������&raquo;, ������� ������</li>
<li>&laquo;����&raquo;, ���, ���������</li>
</ul>
<p><strong>����� ������:</strong> <?php lj_user('altsarev'); ?></p>
<p><strong>����������� �����/Jabber</strong>: <a href="mailto:papa@bastilia.ru">papa@bastilia.ru</a></p>
</td></tr>

			
			<tr><td class="avatar"><img src="/about/photo/ranma.jpg" /></td>
				<td><p><b>����� (������� �������)</b>
				<p>�&nbsp;������� 2010&nbsp;�. ����� ���� ��������� ��� &laquo;BSG&raquo; �&nbsp;������ ������� ������� ����, ����� �������������� ��&nbsp;����. ������������ ��&nbsp;����� ������������ ������ ����� �����������, ��&nbsp;�&nbsp;�� �������&nbsp;&mdash; �&nbsp;���� ������������� �&nbsp;��������, ������������� ��&nbsp;��������������� ������ �������� ��� ���.</p>
			<ul><li>&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;, ������ ��&nbsp;�������</li>
<li>&laquo;����� ��������: �����������&raquo;, ������ ��&nbsp;�������</li>
<li>&laquo;�������� �����&raquo;, ������ ��&nbsp;�������</li>
<li>&laquo;�������: ��&nbsp;��&nbsp;�������&raquo;, ������</li>
<li>&laquo;���� �����&raquo;, ������ ��&nbsp;�������</li>
</ul>
			<p><strong>�������</strong>: </p>
<ul>
<li>&laquo;����� ���!&raquo; �&nbsp;&laquo;������ ������&raquo; (�������-2010) ��&nbsp;&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</li>
</ul>
<p><strong>����� ������:</strong> <?php lj_user('saotome-kun'); ?></p>
			<p><strong>����������� �����/Jabber</strong>: <a href="mailto:ranma@bastilia.ru">ranma@bastilia.ru</a></p>
			</td></tr>


<tr><td class="avatar"><img src="/about/photo/freexee.jpg" /></td>
				<td><p><b>������ (��������� �������)</b>
				<p>������� ������������� �&nbsp;�����������, �������� ��� ������ �����������. ��&nbsp;��� ��� ��&nbsp;�����������. �������� ��&nbsp;������ ����������� �����&nbsp;&mdash; ��� �������, ��� �&nbsp;���������. �����������, ��&nbsp;���������. ������-������ ������� ����.</p>
			<ul>
<li>&laquo;�����: ����� �����&raquo;, ����������</li>
<li>&laquo;�����������&raquo;, ����������</li>
<li>&laquo;����&raquo;, ������ ��&nbsp;������</li>
</ul>
			<p><strong>����� ������:</strong> <?php lj_user('freexee'); ?></p>
			<p><strong>����������� �����/Jabber</strong>: <a href="mailto:freexee@bastilia.ru">freexee@bastilia.ru</a></p>
			</td></tr>

<tr><td class="avatar"><img src="/about/photo/kubela.jpg" /></td>
				<td><p><b>������ (��������� ������)</b>
				<p>����� � #�������� ����� ����� �����, ���� ������ ���������� ������ �������. ������ ��&nbsp;������ ����� 13-� ����������, ���� ������� ���������� ������� ����� ������, ��� ��� ��������� ������ ������.</p>
			<ul>
<li>&laquo;���� �����&raquo;, ������</li>
</ul>
			<p><strong>����� ������:</strong> <?php lj_user('kubela'); ?></p>
			<p><strong>����������� �����</strong>: <a href="mailto:kubela@bastilia.ru">kubela@bastilia.ru</a></p>
				<p><strong>Jabber</strong>: pr.garda@gmail.com</p>
			</td></tr>
			
			<tr><td class="avatar"><img src="/about/photo/enno.jpg" /></td>
				<td><p><b>���� (���� ��������)</b>
				<p>����� ����� �� ���� ����� ��� ����, �� ��� ���� ����� ��������� ������� ������ (� ��� ���� �������� �����).</p>
			<ul>
<li>&laquo;���� �����&raquo;, ���������� � �������� �� ��������</li>
</ul>
			<p><strong>����� ������:</strong> <?php lj_user('enno-elfinit'); ?></p>
			<p><strong>����������� �����</strong>: <a href="mailto:enno@bastilia.ru">enno@bastilia.ru</a></p>
			</td></tr>



</table>
</td>
<?php show_menu_ex('inc/about.menu');
echo '</tr>';
show_footer(); ?>


